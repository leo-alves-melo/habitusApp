//
//  Colors.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 13/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

let correctColor = UIColor(red: 9.0/255.0, green: 195.0/255.0, blue: 71.0/255.0, alpha: 1)
let incorrectColor = UIColor(red: 255.0/255.0, green: 84.0/255.0, blue: 74.0/255.0, alpha: 1)
let confirmButtonColor = UIColor(red: 8.0/255, green: 198.0/255.0, blue: 144.0/255.0, alpha: 1)
let redoButtonColor = UIColor(red: 53.0/255, green: 143.0/255.0, blue: 255.0/255.0, alpha: 1)
let progressBarBackgroundColor = UIColor(red: 216.0/255.0, green: 213.0/255.0, blue: 213.0/255.0, alpha: 1)
