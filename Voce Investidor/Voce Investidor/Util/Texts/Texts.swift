//
//  Texts.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 24/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

let advantagesTextDescription1 =
"Aqui você descobre quais investimentos são seguros, sem prejuizos e ideais para seu objetivo."
let advantagesTextDescription2 =
"Com apenas 5 minutos e em qualquer lugar você aprende sobre investimentos de forma fácil, rápida e dinâmica."
let advantagesTextDescription3 =
"Saiba como obter retorno financeiro a partir do seu dinheiro, como planejar aquela viagem, a casa dos sonhos entre outros objetivos."
let advantagesTextDescription4 =
"É praticando que se aprende, por isso proporcionamos práticas para que você possa se tornar um investidor de sucesso."

let advantagesTextDescription =
    [advantagesTextDescription1, advantagesTextDescription2, advantagesTextDescription3, advantagesTextDescription4]

let advantagesTextTitle1 =
"100% Seguro"
let advantagesTextTitle2 =
"Aprenda em 5 minutos!"
let advantagesTextTitle3 =
"Conquiste suas metas!"
let advantagesTextTitle4 =
"Aprenda na prática!"

let advantagesTextTitle =
    [advantagesTextTitle1, advantagesTextTitle2, advantagesTextTitle3, advantagesTextTitle4]
