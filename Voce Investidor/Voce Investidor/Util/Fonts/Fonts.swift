//
//  Fonts.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 20/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.

import UIKit

let navigationFont = UIFont(name: "Gilroy-ExtraBold", size: 20)
let navigationLargeFont = UIFont(name: "Gilroy-ExtraBold", size: 34)

let advantegesTextFontLight = UIFont.systemFont(ofSize: 20.0, weight: .medium)
let advantegesTextFontHeavy = UIFont(name: "Gilroy-ExtraBold", size: 30)!

let advantegesTextFontLightiPad = UIFont.systemFont(ofSize: 40.0, weight: .medium)
let advantegesTextFontHeavyiPad = UIFont(name: "Gilroy-ExtraBold", size: 60)!

let cardsFont = UIFont.systemFont(ofSize: 17, weight: .regular)
