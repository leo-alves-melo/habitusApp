// Markup
//
// Copyright (c) 2017 Guille Gonzalez
// See LICENSE file for license
//
//swiftlint:disable all

import Foundation

public enum MarkupNode {
	case text(String)
	case strong([MarkupNode])
	case emphasis([MarkupNode])
	case delete([MarkupNode])
    case underline([MarkupNode])
    case colorize([MarkupNode])
}

extension MarkupNode {
	init?(delimiter: UnicodeScalar, children: [MarkupNode]) {
		switch delimiter {
		case "*":
			self = .strong(children)
        case "#":
			self = .emphasis(children)
		case "~":
			self = .delete(children)
        case "_":
            self = .underline(children)
        case "@":
            self = .colorize(children)
        default:
			return nil
		}
	}
}
