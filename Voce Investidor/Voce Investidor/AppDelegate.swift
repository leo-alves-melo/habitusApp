//
//  AppDelegate.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 15/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var defaults = UserDefaults.standard
    
    /// Set to true when developing, and set to false when realising
    static let debug = false
    
    private func configureNavigationBar() {
        let navigation = UINavigationBar.appearance()
        
        navigation.titleTextAttributes = [NSAttributedStringKey.foregroundColor:
            UIColor.white, NSAttributedStringKey.font: navigationFont!]
        
        if #available(iOS 11, *) {
            navigation.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor:
                UIColor.white, NSAttributedStringKey.font: navigationLargeFont!]
        }
    }
    
    private func chooseFirstViewController() -> UIViewController? {
        
        var storyboardName = ""
        if defaults.bool(forKey: "notFirstTime") {
            storyboardName = "Main"
        } else {
            storyboardName = "Advantages"
        }

        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        
        return viewController
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if !AppDelegate.debug {
            FirebaseApp.configure()
        }
        
        
        self.configureNavigationBar()

        self.window?.rootViewController = self.chooseFirstViewController()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}
