//
//  Crisis.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 10/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Model of a Crisis Card
struct Crisis {
    var title: String?
    var content: Content?
    var identification: String?
    
    init(data: [String: Any]) {
        if let identification = data["ID"] as? String {
            self.identification = identification
        }
        if let title = data["title"] as? String {
            self.title = title
        }
        if let contentData = data["content"] as? [String: Any] {
            if let content = self.parseContent(contentData: contentData) {
                self.content = content
            }
        }
    }
    
    /// Parse the data of the content to a Content
    ///
    /// - Parameter contentData: The json
    /// - Returns: The Content of the json
    private func parseContent(contentData: [String: Any]) -> Content? {
        
        var content: Content?
        
        if let type = contentData["type"] as? String {
            
            if let contentEnum = ContentEnum(rawValue: type) {
                switch contentEnum {
                case .informative:
                    content = Informative(data: contentData)
                case .crisisSingleAnswer:
                    content = CrisisSingleAnswer(data: contentData)
                case .singleAnswer:
                    content = SingleAnswer(data: contentData)
                case .graph:
                    content = Graph(data: contentData)
                }
            }
        }
        
        return content
    }
}
