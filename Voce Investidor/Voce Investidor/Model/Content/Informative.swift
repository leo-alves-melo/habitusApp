//
//  ContentCardModel.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Model of an informative card
struct Informative: Content {
    var text: String?
    var imagePath: String?
    var title: String?
    
    init(data: [String: Any]) {
        if let imagePath = data["imagePath"] as? String {
            self.imagePath = imagePath
        }
        if let text = data["text"] as? String {
            self.text = text
        }
        if let title = data["title"] as? String {
            self.title = title
        }
    }
}
