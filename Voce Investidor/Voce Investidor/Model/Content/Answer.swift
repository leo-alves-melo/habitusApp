//
//  Answer.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// Model of a Answer Card
struct Answer: Content {
    var response: Bool?
    var explanation: String?
}
