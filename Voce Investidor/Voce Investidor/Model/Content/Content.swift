//
//  CardModel.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum ContentEnum: String {
    case informative = "informative"
    case singleAnswer = "questionSingleAnswer"
    case crisisSingleAnswer = "crisisSingleAnswer"
    case graph = "graph"
}

/// Definition of every card
protocol Content {}
