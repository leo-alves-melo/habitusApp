//
//  SingleAnswer.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Model of a Single Answer Card
struct SingleAnswer: Question {
    var question: String?
    var options: [Option]?
    
    init(data: [String: Any]) {
        if let question = data["question"] as? String {
            self.question = question
        }
        if let optionsData = data["options"] as? [[String: Any]] {
            var optionsList: [Option] = []
            for optionData in optionsData {
                optionsList.append(Option(data: optionData))
            }
            self.options = optionsList
        }
    }
}
