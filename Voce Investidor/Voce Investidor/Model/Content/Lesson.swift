//
//  Topic.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 04/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Model of the Lesson
struct Lesson {
    var contentList: [Content]?
    var title: String?
    var description: String?
    var level: Level?
    var duration: Int?
    var percentageCompleted: Int?
    var lessonNumber: Int?
    var completionEffectPhrase: String?
    var imagePath: String?
    var imagePathCell: String?
    var identification: String?
    var lastSeenIndex: Int?
    var totalNumberOfLessons: Int?
    
    init(data: [String: Any]) {
        if let title = data["title"] as? String {
            self.title = title
        }
        if let description = data["description"] as? String {
            self.description = description
        }
        if let totalNumberOfLessons = data["totalNumberOfLessons"] as? Int {
            self.totalNumberOfLessons = totalNumberOfLessons
        }
        if let levelID = data["levelID"] as? String {
            var level = Level()
            level.identification = levelID
            self.level = level
        }
        if let duration = data["duration"] as? Int {
            self.duration = duration
        }
        if let lessonNumber = data["lessonNumber"] as? Int {
            self.lessonNumber = lessonNumber
        }
        if let completionEffectPhrase = data["completionEffectPhrase"] as? String {
            self.completionEffectPhrase = completionEffectPhrase
        }
        if let imagePath = data["imagePath"] as? String {
            self.imagePath = imagePath
        }
        if let imagePathCell = data["imagePathCell"] as? String {
            self.imagePathCell = imagePathCell
        }
        if let identification = data["ID"] as? String {
            self.identification = identification
        }
        if let contentListData = data["contentList"] as? [[String: Any]] {
            var contentList: [Content] = []
            for contentData in contentListData {
                if let content = self.parseContent(contentData: contentData) {
                    contentList.append(content)
                }
            }
            self.contentList = contentList
        }
    }
    
    /// Parse the data of the content to a Content
    ///
    /// - Parameter contentData: The json
    /// - Returns: The Content of the json
    private func parseContent(contentData: [String: Any]) -> Content? {
        
        var content: Content?
        
        if let type = contentData["type"] as? String {
            
            if let contentEnum = ContentEnum(rawValue: type) {
                switch contentEnum {
                case .informative:
                    content = Informative(data: contentData)
                case .singleAnswer:
                    content = SingleAnswer(data: contentData)
                case .crisisSingleAnswer:
                    content = CrisisSingleAnswer(data: contentData)
                case .graph:
                    content = Graph(data: contentData)
                }
            }
        }
        
        return content
    }
}
