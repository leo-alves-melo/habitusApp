//
//  MultipleChoices.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Model of a Multiple Choices Card
struct MultipleChoices: Question {
    var question: String?
    var options: [Option]?
}
