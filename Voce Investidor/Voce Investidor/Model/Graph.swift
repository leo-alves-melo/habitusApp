//
//  Chart.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 25/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Graph: Content {
    
    var minimunAmmountInvested: Float?
    var maximunAmmountInvested: Float?
    var minimunTime: Float?
    var maximunTime: Float?
    
    init(data: [String: Any]) {
        if let minimunAmmountInvested = data["minimunAmmountInvested"] as? Double {
            self.minimunAmmountInvested = Float(minimunAmmountInvested)
        }
        if let maximunAmmountInvested = data["maximunAmmountInvested"] as? Double {
            self.maximunAmmountInvested = Float(maximunAmmountInvested)
        }
        if let minimunTime = data["minimunTime"] as? Double {
            self.minimunTime = Float(minimunTime)
        }
        if let maximunTime = data["maximunTime"] as? Double {
            self.maximunTime = Float(maximunTime)
        }
        
    }
}
