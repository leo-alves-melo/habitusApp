//
//  Level.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 19/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Level {
    var title: String?
    var subtitle: String?
    var lessonList: [Lesson]?
    var completude: Int?
    var identification: String?
    var levelNumber: Int?
    var lessonCount: Int?
    
    init() {}
    
    init(data: [String: Any]) {
        if let title = data["title"] as? String {
            self.title = title
        }
        if let subtitle = data["subtitle"] as? String {
            self.subtitle = subtitle
        }
        if let identification = data["identification"] as? String {
            self.identification = identification
        }
        if let levelNumber = data["levelNumber"] as? Int {
            self.levelNumber = levelNumber
        }
        if let lessonCount = data["lesson_count"] as? Int {
            self.lessonCount = lessonCount
        }
    }
}
