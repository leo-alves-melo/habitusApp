//
//  Broker.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 26/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Available Broker
struct Broker {
    /// The name of the Broker
    var name: String?
    /// The URL image of the Broker
    var imageURL: String?
    /// The internal ID of the broker
    var enumValue: BrokerEnum?
    
    init() {}
    
    init(name: String, imagePath: String, enumValue: BrokerEnum) {
        self.name = name
        self.imageURL = imagePath
        self.enumValue = enumValue
    }
    
    init(data: [String: Any]) {
        if let name = data["name"] as? String {
            self.name = name
        }
        if let imageURL = data["imageURL"] as? String {
            self.imageURL = imageURL
        }
        if let enumValue = data["enumValue"] as? BrokerEnum {
            self.enumValue = enumValue
        }
    }
}
