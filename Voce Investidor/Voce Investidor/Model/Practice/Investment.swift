//
//  Investment.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 26/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// The User investments
struct Investment {
    
    var identification: String?
    var goal: String?
    /// The name of the Investment
    var name: String?
    /// The date that the investment was made
    var date: Date?
    /// The Broker that the investment was made
    var broker: BrokerAccount?
    /// Initial amnount
    var initialAmount: Double?
    /// Any note that user wants to add
    var note: String?
    /// The title of Tesouro Direto
    var investmentPaperID: String?
    /// The current amount earned in the investment
    var currentAmount: Double?
    
    var fraction: Double?
    
    init() {}
    
}
