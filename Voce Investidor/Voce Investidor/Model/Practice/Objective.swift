//
//  Objective.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Objective {
    
    var name: String?
    var imagePath: String?
}
