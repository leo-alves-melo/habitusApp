//
//  Goal.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 26/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Enumerates the possible types of a Goal. It could be for emergency or for other propouses
///
/// - emergency: If the goal is for an emergency
/// - other: For others kinds of goal
enum GoalType: String {
    case emergency
    case other
    
    /// Initializes the GoalType, if can't initialize, use the default value .other
    ///
    /// - Parameter value: The value to be initialized the GoalType
    init(value: String) {
        if let goalType = GoalType(rawValue: value) {
            self = goalType
        } else {
            self = .other
        }
    }
}

/// The goal of the user
struct Goal {
    
    var type: GoalType?
    /// The name of the Goal
    var name: String?
    var identification: String?
    var objectiveName: String?
    var objectiveImagePath: String?
    /// The final value
    var finalValue: Double?
    /// The deadline of the goal
    var deadLine: Date?
    /// The investments made for this goal
    var investments: [Investment]?
    
    /// The current value earned in the goal
    var currentValue: Double? {
        guard let investments = investments else { return 0.0 }
        var sum = 0.0
        for investment in investments {
            sum += investment.currentAmount ?? 0.0
        }
        return sum
    }
    
    init() {}
    
}
