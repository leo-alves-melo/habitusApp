//
//  BrokerAccount.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 26/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Enumerates the possible Brokers
///
/// - easynvest: Easynvest broker
/// - xpinvestimentos: XP Investimentos broker
/// - rico: Rico broker
/// - other: other brokers
enum BrokerEnum: String {
    case easynvest
    case xpinvestimentos
    case rico
    case other
    
    /// Initializes the BrokerEnum, if can't initialize, use the default value .other
    ///
    /// - Parameter value: The value to be initialized the BrokerEnum
    init(value: String?) {
        
        if let value = value {
            if let broker = BrokerEnum(rawValue: value) {
                self = broker
            } else {
                self = .other
            }
        } else {
            self = .other
        }
        
    }
}

/// Specific Broker where user do the investments
struct BrokerAccount {
    /// The broker account identification
    var identification: String?
    /// The name of the Broker
    var name: String?
    /// The path image of the Broker
    var imagePath: String?
    
    var miniImagePath: String?
    /// The date where user openned an account at the Broker
    var openDate: Date?
    
    var broker: BrokerEnum
    
    /// Initializes given a broker enumeration
    ///
    /// - Parameter brokerAccount: The broker enumeration
    init(brokerAccount: BrokerEnum) {
        switch brokerAccount {
        case .easynvest:
            self.name = "Easynvest"
            self.imagePath = "brokerEasynvest"
            self.miniImagePath = "brokerMiniEasynvest"
        case .rico:
            self.name = "Rico"
            self.imagePath = "rico"
            self.miniImagePath = "brokerMiniRico"
        case .xpinvestimentos:
            self.name = "XP Investimentos"
            self.imagePath = "xp-investimentos-logo.jpg"
            self.miniImagePath = "brokerMiniXp"
        default:
            self.imagePath = "otherBroker"
            self.miniImagePath = "brokerMiniPlaceholder"
        }
        
        self.broker = brokerAccount
    }
}
