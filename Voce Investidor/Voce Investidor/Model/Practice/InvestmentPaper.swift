//
//  Title.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 26/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Title in Tesouro Direto
struct InvestmentPaper {
    
    var identifier: String?
    /// The name of the paper
    var name: String?
    /// The minumum amount to do the paper
    var minimumAmount: Double?
    /// The due date
    var due: Date?
    /// How the title is calculated
    var calculationString: String?
    
    init() {}
    
    init(data: [String: Any]) {
        if let name = data["name"] as? String {
            self.name = name
        }
        if let minimumAmount = data["minimumAmount"] as? Double {
            self.minimumAmount = minimumAmount
        }
        if let due = data["due"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dueDate = dateFormatter.date(from: due)
            self.due = dueDate
        }
        if let calculationString = data["calculationString"] as? String {
            self.calculationString = calculationString
        }
    }
}
