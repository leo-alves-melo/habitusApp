//
//  UserStatic.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 09/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class UserStatic {
    static var name: String?
    static var investment: [String]?
    static var userAge: Int?
    static var userControl: String?
    static var userDebts: String?
    static var userEmail: String?
    static var userGoal: String?
    static var userLastAnswer: String?
    static var userMonthlyMoney: String?
    static var userSalaryCompromised: String?
    static var userHasCar: String?
    static var userHasHealthPlan: String?
    static var userPayRent: String?
    static var userAutomotivePayment: String?
    static var userEletronicPayment: String?
    static var userCoursePayment: String?
    static var userChangeCar: String?
    static var userBuyBuilding: String?
    static var userRetirement: String?
    static var userChildren: String?
    static var userAccept: String?

    static var userObject: User?
}
