//
//  Message.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 03/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum MessageType: Int {
    case system = 0
    case user = 1
    
    func asString() -> String {
        switch self {
        case .system:
            return "systemMessage"
        case .user:
            return "userMessage"
        default:
            return "systemMessage"
        }
    }
}

class Message {
    var identification: Int?
    var text: String = ""
    var type: MessageType = .system
    var displayed: Bool = false
    var next: [Int] = []
    var userInput: UserInput?
    var storyToShow: String?
    var returnOnSuccess: Int?
    var returnOnFailed: Int?
    
    
    init(_ text: String, _ type: MessageType = .system) {
        self.text = text
        self.type = type
    }
    
    init(data: [String: Any]) {
        if let identification = data["ID"] as? Int {
            self.identification = identification
        }
        if let userInputData = data["userInput"] as? [String: Any] {

            self.userInput = UserInput(data: userInputData)
        }
        if let text = data["text"] as? String {
            self.text = text
        }
        if let type = data["type"] as? Int {
            if let mType = MessageType(rawValue: type) {
                self.type = mType
            } else {
                print("ERROR: Invalid message type in JSON")
            }
        }
        if let next = data["next"] as? [Int] {
            self.next = next
        }
        if let storyToShow = data["storyToShow"] as? String {
            self.storyToShow = storyToShow
        }
        if let returnOnSuccess = data["returnOnSuccess"] as? Int {
            self.returnOnSuccess = returnOnSuccess
        }
        if let returnOnFailed = data["returnOnFailed"] as? Int {
            self.returnOnFailed = returnOnFailed
        }
    }
}
