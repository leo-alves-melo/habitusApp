//
//  Option.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 12/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Modelo da opção de escolha de uma alternative de uma questão
struct Option {
    var alternative: String?
    var isCorrect: Bool?
    var explanation: String?
    
    init(data: [String: Any]) {
        if let alternative = data["alternative"] as? String {
            self.alternative = alternative
        }
        if let isCorrect = data["isCorrect"] as? Bool {
            self.isCorrect = isCorrect
        }
        if let explanation = data["explanation"] as? String {
            self.explanation = explanation
        }
    }
}
