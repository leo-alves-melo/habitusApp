//
//  UserInput.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 05/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum UserInputType: String {
    case keyboardName
    case keyboardNumber
    case datePicker
    case buttonsSingleOption
    case keyboardEmail
}

enum UserInputAttributeTo: String {
    case userName
    case userAge
    case userInvestment
    case userDebts
    case userControl
    case userEmail
    case userLastAnswer
    case userMonthlyMoney
    case userSalaryCompromised
    case userHasCar
    case userHasHealthPlan
    case userPayRent
    case userAutomotivePayment
    case userEletronicPayment
    case userCoursePayment
    case userChangeCar
    case userBuyBuilding
    case userRetirement
    case userChildren
    case userAccept
}

struct UserInput {
    var type: UserInputType?
    var attributeTo: UserInputAttributeTo?
    var optionsData: [String]?
    
    init(data: [String: Any]) {
        if let type = data["type"] as? String {
            self.type = UserInputType(rawValue: type)
        }
        if let attributeTo = data["attributeTo"] as? String {
            self.attributeTo = UserInputAttributeTo(rawValue: attributeTo)
        }
        if let optionsData = data["optionsData"] as? [String] {
            self.optionsData = optionsData
        }
    }
}
