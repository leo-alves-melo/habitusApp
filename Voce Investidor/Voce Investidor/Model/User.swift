//
//  File.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 08/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

// Do not change order!
enum UserData: String, CaseIterable {
    case name = "%USERNAME"
    case investment = "%USERINVESTMENT"
    case userAge = "%USERAGE"
    case userControl = "%USERCONTROL"
    case userDebts = "%USERDEBTS"
    case userEmail = "%USEREMAIL"
    case userLastAnswer = "%USERLASTANSWER"
    case userMonthlyMoney = "%USERMONTHLYMONEY"
    case userSalaryCompromised = "%USERSALARYCOMPROMISED"
    case userHasCar = "%USERHASCAR"
    case userHasHealthPlan = "%USERHASHEALTHPLAN"
    case userPayRent = "%USERPAYRENT"
    case userAutomotivePayment = "%USERAUTOMOTIVEPAYMENT"
    case userEletronicPayment = "%USERELETRONICPAYMENT"
    case userCoursePayment = "%USERCOURSEPAYMENT"
    case userChangeCar = "%USERCHANGECAR"
    case userBuyBuilding = "%USERBUYBUILDING"
    case userRetirement = "%USERRETIREMENT"
    case userChildren = "%USERCHILDREN"
    case userAccept = "%USERACCEPT"
    
}

enum FinancialControl: String {
    case best
    case minimum
    case nothing
}

enum InvestmentsType: String {
    case poupanca
    case funds
    case privatePension
    case others
    case nothing
}

enum BillsPayment: String {
    case overdue
    case okButHard
    case allOk
}

enum FixedExpenses: String {
    case lessThan10
    case lessThan50
    case greaterThan50
    case unknow
}

enum CrisisResponse: String {
    case friends
    case loan
    case dontPay
    case savings
}

enum PeriodShort: String {
    case lessThanOneYear
    case lessThanTwoYears
    case dont
}

enum PeriodMedium: String {
    case lessThanOneYear
    case lessThanFiveYears
    case dont
}

enum PeriodLong: String {
    case lessThanFiveYears
    case lessThanFifteenYears
    case moreThanFifteenYears
    case dont
}

enum CrisisHowToGet: String {
    case invest
    case financing
}

enum CrisisRetire: String {
    case govern
    case working
    case privatePension
    case savings

}

/// User model
class User {
    
    /// If the user is updated from the database
    var updated = false
    
    /// Name
    var name: String = ""

    /// E-mail
    var email: String = ""
    
    /// HashPasswors
    var hashPassword: String?

    /// Birthdate
    var birthDate: Date?

    /// Answer how the user control its finances
    var financialControl: FinancialControl?

    /// Answer to the question about he has investiments
    var hasPreviousInvestments: InvestmentsType?

    /// Answer to the question about payment bill habit
    var howPayBills: BillsPayment?

    /// Answer to the wuestion about the payment. Store in max option. If he choose up to 1000, store 1000. If he choose up to 3000, store 3000.
    var receipts: Int?

    /// Answer to the fixed expenses
    var fixedExpensesMonthly: FixedExpenses?

    /// Answer if the user has a car
    var hasCar: Bool?

    /// Answer if the user has helth insurance
    var hasHealthInsurance: Bool?

    /// Answer to the crisis car break question - level 1
    var crisisCarBreak: CrisisResponse?

    /// Answer to the crisis health insurance question - level 1
    var crisisHealthInsurance: CrisisResponse?

    /// Answer to the question if user have to pay house rent
    var payHouseRent: Bool?

    /// Answer to the question if user have to pay car financing
    var payCarFinancing: Bool?

    /// Answer to the question if user have to pay equipament
    var payEquipament: Bool?

    /// Answer to the question if user have to pay university
    var payUniversity: Bool?

    /// Answer to the crisis house rent question - level 2
    var crisisHouseRent: CrisisResponse?

    /// Answer to the crisis car financing question - level 2
    var crisisCarFinancing: CrisisResponse?

    /// Answer to the crisis car break question - level 2
    var crisisEquipment: CrisisResponse?

    /// Answer to the crisis university question - level 2
    var crisisUniversity: CrisisResponse?

    /// Answer to the question if user want to buy a car
    var wantToBuyCar: PeriodShort?

    /// Answer to the question if user want to buy a house
    var wantToBuyHouse: PeriodMedium?

    /// Answer to the question if user want to retire
    var wantToRetire: PeriodLong?

    /// Answer to the crisis how to buy a car question - level 3
    var crisisBuyCar: CrisisHowToGet?

    /// Answer to the crisis how to buy a house question - level 3
    var crisisBuyHouse: CrisisHowToGet?

    /// Answer to the crisis how to retire question - level 3
    var crisisRetire: CrisisRetire?
    
    var goals: [Goal]?
    
    var brokers: [BrokerAccount]?
    
    static let instance = User()
    
    private init() {
        
    }
}
