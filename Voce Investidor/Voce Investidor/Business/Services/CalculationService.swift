//
//  CalculationService.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Centralize the earnings calculations.
/// Calculate the earnings from each investment, progress in each goal, and
/// compare values with what the user would have earned had they used Poupança instead.
class CalculationService {
    
    /// Calculate how much the user has earned from an investment so far.
    func earnings(from investment: Investment) -> Double {
        // TODO: Actual calculations.
        guard let initial = investment.initialAmount, let current = investment.currentAmount else { return -1.0 }
        return current
    }
    
    /// Calculate how much the user has earned in a goal so far.
    func earnings(in goal: Goal) -> Double {
        // TODO: Actual calculations.
        guard let investments = goal.investments else { return -1.0 }
        var totalEarnings: Double = 0.0
        for investment in investments {
            totalEarnings += self.earnings(from: investment)
        }
        return totalEarnings
    }
}
