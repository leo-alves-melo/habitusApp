//
//  DatabaseService.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 19/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class DatabaseService {
    
    func getData(from path: String, completion: ([String: Any]?, Error?) -> Void) {
        
        let dataString = self.getStringToFile(path: path)
        
        let data = dataString.data(using: .utf8)!
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                as? [String: Any] {
                completion(json, nil)
                
            }
        } catch let error as NSError {
            completion(nil, error)
        }
    }
    
    private func getStringToFile(path: String) -> String {
        var text = ""
        
        if let path = Bundle.main.path(forResource: path, ofType: "json") {
            do {
                text = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                text = text.replacingOccurrences(of: "\n", with: " ")
                text = text.replacingOccurrences(of: "\t", with: " ")
            } catch {
                
            }
        } else {
            return text
        }
        
        return text
    }
}
