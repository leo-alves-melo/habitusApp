//
//  LevelService.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 19/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class LevelService {
    
    let path = "level"
    
    func allLevels(completion: ([Level]?, Error?) -> Void) {
        
        let databaseService = DatabaseService()
        
        databaseService.getData(from: self.path) { (json, error) in
            
            guard let json = json, error == nil else {
                print("bad json")
                completion(nil, nil)
                return
            }
            
            var levels: [Level] = []
            
            for (_, value) in json {
                if let value = value as? [String: Any] {
                    
                    let level = Level(data: value)
                    
                    levels.append(level)
                    
                }
            }

            let sorted = levels.sorted { one, two in
                guard let levelNumber1 = one.levelNumber, let levelNumber2 = two.levelNumber else {
                    return false
                }
                return levelNumber1 < levelNumber2
            }
            completion(sorted, nil)
        }
    }
    
    func getLevelCompletude(for level: Level, completion: (Int?, Error?) -> Void) {
        if let identification = level.identification {
            let completude = UserDefaults.standard.integer(forKey: identification)
            completion(completude, nil)
        } else {
            completion(nil, nil)
        }
    }
    
    func decrementLevelCompletude(for level: Level, completion: ((Error?) -> Void)?) {
        if let identification = level.identification {
            self.getLevelCompletude(for: level) { (oldCompletude, error) in
                if let completude = oldCompletude {
                    UserDefaults.standard.set(completude - 1, forKey: identification)
                } else {
                    UserDefaults.standard.set(1, forKey: identification)
                }
                completion?(error)
            }
            
        }
    }
    
    func incrementLevelCompletude(for level: Level, completion: ((Error?) -> Void)?) {
        if let identification = level.identification {
            self.getLevelCompletude(for: level) { (oldCompletude, error) in
                if let completude = oldCompletude {
                    UserDefaults.standard.set(completude + 1, forKey: identification)
                } else {
                    UserDefaults.standard.set(1, forKey: identification)
                }
                completion?(error)
            }
            
        }
    }
}
