//
//  UserService.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 10/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class UserService {

    private let dao = UserDAO()

    /// Create or update the user
    ///
    /// - Parameters:
    ///   - user: User to save
    ///   - completion: Result callback
    func update(_ user: User, completion: @escaping (_ user: User?, _ error : Error?) -> Void) {
        dao.update(user: user, completion: completion)
    }
    
    /// Create or update the user
    ///
    /// - Parameters:
    ///   - user: User to save
    ///   - completion: Result callback
    func create(_ user: User, completion: @escaping (_ user: User?, _ error : SignUpErrors?) -> Void) {
        dao.create(user: user, completion: completion)
    }

    /// Check if the e-mail exists
    ///
    /// - Parameters:
    ///   - email: E-mail to check
    ///   - completion: result callback
    func existsEmail(email: String, completion: @escaping (_ result: Bool) -> Void) {

        dao.fetchUserByEmail(email: email, { (user, error) in
            if let _ = error {
                completion(true)
                return
            }

            completion(false)

        })
    }

    /// Do the login for user
    ///
    /// HOW TO USE
    ///        let userService = UserService()
    ///        var user = User()
    ///
    ///        user.name = "edgar"
    ///        user.email = "aeee@eeee.com.br"
    ///        user.hasPreviousInvestments = InvestmentsType.funds
    ///
    ///        userService.save(user) { (userSaved, error) in
    ///
    ///            if let error = error {
    ///                print(error)
    ///                return
    ///            }
    ///
    ///            print(userSaved!)
    ///
    ///            userService.login(user: userSaved!, completion: { (saved) in
    ///                if saved {
    ///                    print("GLORIA A DEUX")
    ///                } else {
    ///                    print("OHMEUDEUX")
    ///                }
    ///            })
    ///
    ///
    ///        }
    ///
    /// - Parameters:
    ///   - user: User to save
    ///   - completion: result callback
    func login(user: User, completion:  ((_ result: Bool) -> Void)?) -> Void {

        dao.saveLogin(user: user) { (user, error) in
            if let _ = error {
                if let completion = completion {
                    completion(false)
                }
                return
            }
            if let completion = completion {
                completion(true)
            }
        }
    }

    /// Return the logged user
    /// HOW TO USE
    ///        let userService = UserService()
    ///
    ///        userService.getLogged { (user, error) in
    ///            if let error = error {
    ///                print(error)
    ///                return
    ///            }
    ///
    ///            print(user!)
    ///
    ///        }
    ///
    /// - Parameter completion: Result callback
    func getLogged(completion: @escaping (_ user: User?, _ error: AuthenticationError?) -> Void) -> Void {
        
        if User.instance.updated == false {
            dao.getLogged(completion: completion)
        } else {
            completion(User.instance, nil)
        }
        
    }

    //TODO: todo
    func getLogin(email: String, hashPassword: String, completion: @escaping (User?, AuthenticationError?)-> Void) {
        
        dao.getLogin(email: email, hashPassword: hashPassword, completion: completion)
    }
    
    func exit() {
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        User.instance.updated = false
        User.instance.name = ""
        User.instance.email = ""
        User.instance.birthDate = nil
        User.instance.crisisBuyCar = nil
        User.instance.crisisBuyHouse = nil
        User.instance.crisisCarBreak = nil
        User.instance.crisisCarFinancing = nil
        User.instance.crisisEquipment = nil
        User.instance.crisisHealthInsurance = nil
        User.instance.crisisHouseRent = nil
        User.instance.crisisRetire = nil
        User.instance.crisisUniversity = nil
        User.instance.financialControl = nil
        User.instance.fixedExpensesMonthly = nil
        User.instance.hasCar = nil
        User.instance.hasHealthInsurance = nil
        User.instance.hashPassword = nil
        User.instance.hasPreviousInvestments = nil
        User.instance.howPayBills = nil
        User.instance.payCarFinancing = nil
        User.instance.payEquipament = nil
        User.instance.payUniversity = nil
        User.instance.receipts = nil
        User.instance.wantToBuyCar = nil
        User.instance.wantToBuyHouse = nil
        User.instance.wantToRetire = nil
        
        UserStatic.userObject = nil
    }
    
    func haveRegisteredHashPassword(email: String, completion: @escaping (Bool?, Error?) -> Void) {
        
        dao.fetchUserByEmail(email: email, { (user, error) in
            
            if let _ = error {
                completion(nil, error)
                return
            }
            
            if let _ = user?.hashPassword{
                completion(true, nil)
                return
            }else{
                completion(false, nil)
                return
            }
        })
    }
    
}
