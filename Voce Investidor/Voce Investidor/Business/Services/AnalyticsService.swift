//
//  AnalyticsService.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 20/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Firebase

enum EventType: String {
    case timeToCompleteLesson = "TimeToCompleteLesson"
    case timeToCompleteCrisis = "TimeToCompleteCrisis"
    case timeToSkipCrisis = "TimeToSkipCrisis"
    case timeInACard = "TimeInACard"
    case indexStopped = "IndexStopped"
}

class AnalyticsService {
    
    private func send(eventName: String, json: [String: Any]) {
        
        if !AppDelegate.debug {
            let eventNameWithoutBraces = eventName.replacingOccurrences(of: "-", with: "_")
            Analytics.logEvent(eventNameWithoutBraces, parameters: json)
        }
    }
    
    func send(time: Int, for lesson: Lesson) {
        var json = [String: Any]()
        
        json["timeInSeconds"] = time
        if let identification = lesson.identification {
            self.send(eventName: EventType.timeToCompleteLesson.rawValue + "_" + identification, json: json)
        }
    }
    
    func send(time: Int, for crisis: Crisis) {
        var json = [String: Any]()
        
        json["timeInSeconds"] = time
        if let identification = crisis.identification {
            self.send(eventName: EventType.timeToCompleteCrisis.rawValue + "_" + identification, json: json)
        }
    }
    
    func send(time: Int, forSkipped crisis: Crisis) {
        var json = [String: Any]()
        
        json["timeInSeconds"] = time
        if let identification = crisis.identification {
            self.send(eventName: EventType.timeToSkipCrisis.rawValue + "_" + identification, json: json)
        }
    }
    
    func send(indexStopped: Int, for lesson: Lesson) {
        var json = [String: Any]()
        
        json["indexStopped"] = indexStopped
        if let identification = lesson.identification {
            self.send(eventName: EventType.indexStopped.rawValue + "_" + identification, json: json)
        }
    }
}
