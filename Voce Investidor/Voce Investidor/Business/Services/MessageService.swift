//
//  MessageService.swift
//  Voce Investidor
//
//  Created by Gustavo de Mello Crivelli on 04/10/2018.
//  Copyright © 2018 Gustavo de Mello Crivelli. All rights reserved.
//

import Foundation
import UIKit

class MessageService {
    
    let path = "messages"
    
    /// Return all messages to the completion
    ///
    /// - Parameters:
    ///   - diagID: The ID of the current diagnostic session
    ///   - completion: The function to be called when levels are got
    func allMessages(for diagID: String, completion: ([Message]?, Error?) -> Void) {
        
        let databaseService = DatabaseService()
        
        databaseService.getData(from: self.path) { (json, error) in
            guard let json = json, error == nil else {
                print("bad json")
                completion(nil, nil)
                return
            }
            
            var messages: [Message] = []
            
            for (_, value) in json {
                if var value = value as? [String: Any] {
                    
                    value["totalNumberOfMessages"] = json.count
                    
                    if let identification = value["diag-ID"] as? String {
                        if identification == diagID {
                            
                            if let messagesData = value["messageList"] as? [[String: Any]] {
                                for messageData in messagesData {
                                    let message = Message(data: messageData)
                                    messages.append(message)
                                }
                            }
                        }
                    }
                }
            }
            
            let sortedMessages = messages.sorted { one, two in
                guard let aID = one.identification, let bID = two.identification else {
                    return false
                }
                return aID < bID
            }
            completion(sortedMessages, nil)
        }
    }
}
