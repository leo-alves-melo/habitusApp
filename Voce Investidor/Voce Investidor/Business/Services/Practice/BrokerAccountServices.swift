//
//  BrokerAccountServices.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Retrieve, Create and Updating broker accounts
class BrokerAccountServices {
    
    /// Create a DAO to work with brokerAccount
    private let dao = BrokerAccountDAO()
    
    /// Retrieve all broker accounts from a specific user
    ///
    /// - Parameters:
    ///   - user: The user to receive brokerAccounts
    ///   - completion: the function to be called when retrieving brokerAccounts or error
    func brokerAccounts(from user: User, completion: @escaping ([BrokerAccount]?, Error?) -> Void) {
        self.dao.brokerAccount(from: user, completion: completion)
    }
    
    /// Create a new brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - brokerAccount: A brokerAccount to be created
    ///   - completion: A completion to be called after creation
    func create(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        self.dao.create(brokerAccount: brokerAccount, completion: completion)
    }
    
    /// Update a new brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - brokerAccount: A brokerAccount to be updated
    ///   - completion: A completion to be called after creation
    func update(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        self.dao.update(brokerAccount: brokerAccount, completion: completion)
    }
    
    /// Delete the brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - goal: A brokerAccount to be deleted
    ///   - completion: The completion to be called
    func delete(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        self.dao.delete(brokerAccount: brokerAccount, completion: completion)
    }
}
