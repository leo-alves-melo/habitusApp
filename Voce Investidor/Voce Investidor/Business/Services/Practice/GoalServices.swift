//
//  GoalServices.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

/// Retrieve, Create and Updating goals
class GoalServices {
    
    /// Create a DAO to work with Goals
    private let dao = GoalDAO()
    
    /// Retrieve all goals from a specific user
    ///
    /// - Parameters:
    ///   - user: The user to receive goals
    ///   - completion: the function to be called when retrieving goals or error
    func goals(from user: User, completion: @escaping ([Goal]?, Error?) -> Void) {
        self.dao.goals(from: user, completion: completion)
    }
    
    /// Create a new Goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be created
    ///   - completion: A completion to be called after creation
    func create(goal: Goal, completion: @escaping (Error?) -> Void) {
        self.dao.create(goal: goal, completion: completion)
    }
    
    /// Update a new Goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be updated
    ///   - completion: A completion to be called after creation
    func update(goal: Goal, completion: @escaping (Error?) -> Void) {
        self.dao.update(goal: goal, completion: completion)
    }
    
    /// Delete the goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be deleted
    ///   - completion: The completion to be called
    func delete(goal: Goal, completion: @escaping (Error?) -> Void) {
        self.dao.delete(goal: goal, completion: completion)
    }
}
