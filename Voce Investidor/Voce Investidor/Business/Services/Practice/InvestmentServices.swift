//
//  InvestmentServices.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 29/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class InvestmentServices {
    
    /// Create a DAO to work with Investments
    private let dao = InvestmentDAO()
    
    /// Retrieve all investments from a specific goal
    ///
    /// - Parameters:
    ///   - user: The goal to receive investments
    ///   - completion: the function to be called when retrieving investments or error
    func investments(from goal: Goal, completion: @escaping ([Investment]?, Error?) -> Void) {
        self.dao.investment(from: goal, completion: completion)
    }
    
    /// Create a new Goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be created
    ///   - completion: A completion to be called after creation
    func create(investment: Investment, completion: @escaping (Error?) -> Void) {
        self.dao.create(investment: investment, completion: completion)
    }
    
    /// Update a new investment in the Database
    ///
    /// - Parameters:
    ///   - investment: A investment to be updated
    ///   - completion: A completion to be called after creation
    func update(investment: Investment, completion: @escaping (Error?) -> Void) {
        self.dao.update(investment: investment, completion: completion)
    }
    
    /// Delete the investment in the Database
    ///
    /// - Parameters:
    ///   - goal: A investment to be deleted
    ///   - completion: The completion to be called
    func delete(investment: Investment, completion: @escaping (Error?) -> Void) {
        self.dao.delete(investment: investment, completion: completion)
    }
}
