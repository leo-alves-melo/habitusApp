//
//  InvestimentPaperServices.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 10/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class InvestmentPaperServices {
    
    /// Create a DAO to work with Investments
    private let dao = InvestmentPaperDAO()
    
    /// Retrieve the investmentPaper from a specific goal
    ///
    /// - Parameters:
    ///   - identifier: The identifier of an InvestmentPaper
    ///   - completion: the function to be called when retrieving InvestmentPaper or error
    func investmentPaper(from identifier: String, completion: @escaping (InvestmentPaper?, Error?) -> Void) {
        self.dao.investmentPaper(from: identifier, completion: completion)
    }
    
    /// Retrieve all InvestmentPapers
    ///
    /// - Parameters:
    ///   - user: The goal to receive investments
    ///   - completion: the function to be called when retrieving investments or error
    func investmentPapers(completion: @escaping ([InvestmentPaper]?, Error?) -> Void) {
        self.dao.investmentPapers(completion: completion)
    }
}
