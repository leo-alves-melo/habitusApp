//
//  TopicService.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 04/09/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import UIKit

class CrisisService {
    
    let path = "crisis"
    
    /// Return all crisis to the completion
    ///
    /// - Parameters:
    ///   - completion: The function to be called when levels are got
    func allCrisis(completion: ([Crisis]?, Error?) -> Void) {
        
        let databaseService = DatabaseService()
        
        databaseService.getData(from: self.path) { (json, error) in
            
            guard let json = json, error == nil else {
                print("bad json")
                completion(nil, nil)
                return
            }
            
            var crisisList: [Crisis] = []
            
            for (_, value) in json {
                if let value = value as? [String: Any] {
                    crisisList.append(Crisis(data: value))
                }
            }
            
            completion(crisisList, error)
        }
    }
    
    func getCrisisFinished(for crisis: Crisis, completion: (Bool?, Error?) -> Void) {
        if let identification = crisis.identification {
            let finished = UserDefaults.standard.bool(forKey: identification + "-finished")
            completion(finished, nil)
        } else {
            completion(nil, nil)
        }
    }
    
    func setCrisisFinished(for crisis: Crisis) {
        if let identification = crisis.identification {
            UserDefaults.standard.set(true, forKey: identification + "-finished")
        }
    }
}
