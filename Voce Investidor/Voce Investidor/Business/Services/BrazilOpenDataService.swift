//
//  BrazilOpenDataService.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 27/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import DadosAbertos

class BrazilOpenDataService {
    
    private var serverUrl = "https://api.bcb.guruinvestapp.com.br"
    
    func getSelicData(completion: @escaping ([DailySelicTax]?, ErrorDadosAbertos?) -> Void) {
        SelicTaxService.setServer(self.serverUrl)
        SelicTaxService.allDailySelicTax(timeout: 10, completion: { (dailySelicTax, error) in
                guard error == nil else {
                    print("Error in get Selic on BrazilOpenDataService: \(error!)")
                    completion(nil, error)
                    return
                }
                completion(dailySelicTax, nil)
        })
    }
    
    func getTRTaxMonthly(completion: @escaping ([MonthlyTRTax]?, ErrorDadosAbertos?) -> Void) {
        
        TRTaxService.setServer(self.serverUrl)
        TRTaxService.allMonthlyTRTax(timeout: 10, completion: { (monthlyTRTax, error) in
            guard error == nil else {
                print("Error in get Selic on BrazilOpenDataService: \(error!)")
                completion(nil, error)
                return
            }
            
            completion(monthlyTRTax, nil)
        })
    }
}
