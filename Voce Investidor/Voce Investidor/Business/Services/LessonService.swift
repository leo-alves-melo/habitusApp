//
//  TopicService.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 04/09/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import UIKit

class LessonService {
    
    let path = "lesson"
    
    /// Return all lessons to the completion
    ///
    /// - Parameters:
    ///   - level: The level
    ///   - completion: The function to be called when levels are got
    func allLessons(level: Level, completion: ([Lesson]?, Error?) -> Void) {
        
        let databaseService = DatabaseService()
        
        databaseService.getData(from: self.path) { (json, error) in
            
            guard let json = json, error == nil else {
                print("bad json")
                completion(nil, nil)
                return
            }
            
            var lessons: [Lesson] = []
            
            for (_, value) in json {
                if var value = value as? [String: Any] {
                    
                    value["totalNumberOfLessons"] = json.count
                    
                    var lesson = Lesson(data: value)
                    
                    if lesson.level?.identification == level.identification {
                        
                        lesson.level = level
                        lessons.append(lesson)
                    }
                }
            }

            let sorted = lessons.sorted { one, two in
                guard let lessonNumber1 = one.lessonNumber, let lessonNumber2 = two.lessonNumber else {
                    return false
                }
                return lessonNumber1 < lessonNumber2
            }
            
            completion(sorted, nil)
        }
    }
    
    func getLessonIndex(for lesson: Lesson, completion: (Int?, Error?) -> Void) {
        if let identification = lesson.identification {
            let index = UserDefaults.standard.integer(forKey: identification)
            completion(index, nil)
        } else {
            completion(nil, nil)
        }
    }
    
    func saveLessonIndex(for lesson: Lesson, index: Int, completion: ((Error?) -> Void)?) {
        if let identification = lesson.identification {
            
            self.getLessonIndex(for: lesson) { (oldIndex, error) in
                if let oldIndex = oldIndex {
                    if oldIndex <= index {
                        UserDefaults.standard.set(index, forKey: identification)
                    }
                }
                completion?(error)
            }
        }
    }
    
    func redo(lesson: Lesson) {
        
        if let identification = lesson.identification {
            UserDefaults.standard.set(0, forKey: identification)
            self.setLessonFinished(for: lesson, finished: false)
            
            if let level = lesson.level {
                LevelService().decrementLevelCompletude(for: level, completion: nil)
            }
        }
    }
    
    func getLessonFinished(for lesson: Lesson, completion: (Bool?, Error?) -> Void) {
        if let identification = lesson.identification {
            let finished = UserDefaults.standard.bool(forKey: identification + "-finished")
            completion(finished, nil)
        } else {
            completion(nil, nil)
        }
    }
    
    func setLessonFinished(for lesson: Lesson, finished: Bool) {
        if let identification = lesson.identification {
            UserDefaults.standard.set(finished, forKey: identification + "-finished")
        }
    }
    
}
