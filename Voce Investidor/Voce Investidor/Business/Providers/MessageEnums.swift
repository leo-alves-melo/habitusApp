//
//  MessageEnums.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 12/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum MessageEnums: Int {
    case didLogin = 1001
}
