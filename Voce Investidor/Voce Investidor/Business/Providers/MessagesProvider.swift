//
//  MessagesProvider.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 08/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import ZCAnimatedLabel

protocol MessagesProviderDelegate: class {
    func shouldUpdateMessages(newMessage: Message, index: IndexPath)
    func shouldReceiveInformation(with type: UserInput)
    func shouldShow(story: String)
    func finishMessaging()
}

class MessagesProvider {
    
    private var messagesList: [Message] = []
    var visibleMessagesList: [Message] = []
    private var currentMessage: Message?
    var messageID: String
    var selectedChosenIndex = 0
    var lastMessage = false
    var messagesDictionary: [Int: Message] = [:]
    var shouldShowStory = false
    
    private weak var delegate: MessagesProviderDelegate?
    
    init(for messageID: String, delegate: MessagesProviderDelegate) {
        self.messageID = messageID
        self.delegate = delegate
    }
    
    func getMessages(completion: (Error?) -> Void) {
        
        MessageService().allMessages(for: self.messageID) { (messages, error) in
            guard let messages = messages, error == nil else {
                print("Error when getting messages for ID \(messageID) in MessagesProvider: \(error!.localizedDescription)")
                completion(error)
                return
            }
            
            self.messagesList = messages
            self.addMessagesToDictionary()
            self.currentMessage = self.messagesList.first
            completion(nil)
        }
    }
    
    private func addMessagesToDictionary() {
        for message in self.messagesList {
            if let identification = message.identification {
                self.messagesDictionary[identification] = message
            }
        }
    }
    
    private func finishMessaging() {
        self.delegate?.finishMessaging()
    }
    
    func storyReturn(status: StoryStatus) {
        switch status {
        case .success:
            if let message = self.currentMessage {
                if let nextMessageIndex = message.returnOnSuccess {
                    self.currentMessage = self.messagesDictionary[nextMessageIndex]
                }
            }
            
        case .failed:
            if let message = self.currentMessage {
                if let nextMessageIndex = message.returnOnFailed {
                    self.currentMessage = self.messagesDictionary[nextMessageIndex]
                }
            }
        }
    }
    
    func nextMessage() {
        if self.lastMessage {
            self.finishMessaging()
            return
        }
        
        if self.shouldShowStory {
            self.shouldShowStory = false
            if let message = self.currentMessage {
                if let storyToShow = message.storyToShow {
                    delegate?.shouldShow(story: storyToShow)
                }
            }
            return
        }
        
        if let message = self.currentMessage {
            if message.storyToShow != nil {
                self.shouldShowStory = true
            } else {
                if message.next == [] {
                    self.lastMessage = true
                }
            }
 
            if message.type == .system {
                self.addMessage(message: message)
            } else {
                
                if let userInput = message.userInput {
                    self.delegate?.shouldReceiveInformation(with: userInput)
                }
            }
        }
    }
    
    func userDataReceived() {
        
        if let message = self.currentMessage {
            self.addMessage(message: message)
        }
        
    }
    
    private func changeCurrentMessageIndex() {
        if let message = self.currentMessage {
            if message.next.count > 0 {
                let identification = message.next[self.selectedChosenIndex]
                self.currentMessage = self.messagesDictionary[identification]
                self.selectedChosenIndex = 0
            }
        }
    }
    
    private func addUserMessage(text: String) {
        let message = Message(text, .user)
        self.visibleMessagesList.append(message)
        let index = IndexPath(row: self.visibleMessagesList.count - 1, section: 0)
        self.delegate?.shouldUpdateMessages(newMessage: message, index: index)
    }
    
    private func addMessage(message: Message) {

        let newMessage = Message(message.text, message.type)
        newMessage.text = self.replaceUserInfo(in: message.text)
        self.visibleMessagesList.append(newMessage)
        let index = IndexPath(row: self.visibleMessagesList.count - 1, section: 0)
        self.delegate?.shouldUpdateMessages(newMessage: newMessage, index: index)
        self.changeCurrentMessageIndex()
    }
    
    private func replaceUserInfo(in text: String) -> String {
        
        let replaceDict: [UserData: Any?] = [UserData.name: UserStatic.name,
                                             UserData.userAccept: UserStatic.userAccept,
                                             UserData.investment: UserStatic.investment?[0],
                                             UserData.userAge: UserStatic.userAge,
                                             UserData.userControl: UserStatic.userControl,
                                             UserData.userDebts: UserStatic.userDebts,
                                             UserData.userEmail: UserStatic.userEmail,
                                             UserData.userLastAnswer: UserStatic.userLastAnswer,
                                             UserData.userMonthlyMoney: UserStatic.userMonthlyMoney,
                                             UserData.userSalaryCompromised: UserStatic.userSalaryCompromised,
                                             UserData.userHasCar: UserStatic.userHasCar,
                                             UserData.userHasHealthPlan: UserStatic.userHasHealthPlan,
                                             UserData.userPayRent: UserStatic.userPayRent,
                                             UserData.userAutomotivePayment: UserStatic.userAutomotivePayment,
                                             UserData.userEletronicPayment: UserStatic.userEletronicPayment,
                                             UserData.userCoursePayment: UserStatic.userCoursePayment,
                                             UserData.userChangeCar: UserStatic.userChangeCar,
                                             UserData.userBuyBuilding: UserStatic.userBuyBuilding,
                                             UserData.userRetirement: UserStatic.userRetirement,
                                             UserData.userChildren: UserStatic.userChildren
                                             ]
        
        var newText = text
        for (key, value) in replaceDict {
            let replaceString = String(describing: value ?? "muitos anos. MUITOS. Pensa na idade do universo. Tenho pelo menos o dobro disso em")
            newText = newText.replacingOccurrences(of: key.rawValue, with: replaceString)
        }
        return newText
    }

    func wrongUserDataReceived() {
        self.selectedChosenIndex = 1
        if let message = self.currentMessage {
            self.addMessage(message: message)
        }
    }
}
