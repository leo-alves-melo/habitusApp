//
//  InvestmentPaperEnum.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 11/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum InvestmentPaperEnum: String {
    case SELIC
    case IPCA
}
