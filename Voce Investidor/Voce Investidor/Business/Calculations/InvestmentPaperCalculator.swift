//
//  InvestmentPaperCalculator.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 11/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import DadosAbertos

class InvestmentPaperCalculator {
    
    private var selics: [DailySelicTax]?
    
    func calculate(investment: Investment, completion: @escaping (Double?, InvestmentPaperCalculatorErrors?) -> Void) {
        
        guard let investmentPaperID = investment.investmentPaperID, let openDate = investment.date else {
            completion(nil, InvestmentPaperCalculatorErrors.investmentPaperNotFound)
            return
        }
        
        InvestmentPaperServices().investmentPaper(from: investmentPaperID) { (investmentPaper, error) in
            guard error == nil,
                let investmentPaper = investmentPaper,
                let identifier = investmentPaper.identifier,
                let initialAmount = investment.initialAmount else {
                    
                completion(nil, InvestmentPaperCalculatorErrors.investmentPaperNotFound)
                return
            }
            
            guard let investmentPaperEnum = InvestmentPaperEnum(rawValue:
                identifier.components(separatedBy: "-")[0]) else {
                completion(nil, InvestmentPaperCalculatorErrors.typeOfInvestmentNotFound)
                    return
            }
            
            switch investmentPaperEnum {
            case .SELIC:
                self.calculateSelic(initialAmout: initialAmount, openDate: openDate, completion: completion)
            case .IPCA:
                print()
            }
            
        }
    }
    
    private func calculateSelic(initialAmout: Double, openDate: Date,
                                completion: @escaping (Double?, InvestmentPaperCalculatorErrors?) -> Void) {
        
        guard let selics = self.selics else {
            
            BrazilOpenDataService().getSelicData { (selicsGot, error) in
                guard error == nil, let selicsGot = selicsGot else {
                    completion(nil, InvestmentPaperCalculatorErrors.brazilOpenDataNotConnected)
                    return
                }
                
                self.selics = selicsGot
    
                self.calculateSelic(initialAmout: initialAmout, openDate: openDate,
                                    completion: completion)
                
            }
            
            return
        }
        
        var usedSelics = selics.reduce(0.0, { (result, selic) -> Double in
            if let date = selic.date, let value = selic.value, date > openDate { // TODO: maior ou maior e igual?
                return result + Double(value)
            } else {
                return result
            }
        })

        let currentValue = initialAmout*(1.0 + usedSelics/100.0)
        
        let profit = currentValue - initialAmout
        
        //Distance in months from openDate to current Date
        let timeInMonths = Calendar.current.dateComponents([.month], from: openDate,
                                                           to: Date()).month ?? 0

        // Discounting IR
        var discount = 0.0
        if timeInMonths < 6 {
            discount = 0.225
        } else if timeInMonths < 12 {
            discount = 0.2
        } else if timeInMonths < 24 {
            discount = 0.175
        } else {
            discount = 0.15
        }
        
        let discountedProfit = profit*(1.0 - discount)
        //let descontoIR = profit - profit*(1.0 - discount)
        
        //Distance in days from openDate to current Date
        let timeInDays = Calendar.current.dateComponents([.day], from: openDate,
                                                           to: Date()).day ?? 0

        // Discounting IOF
        var discountIOF = 0.0
        if timeInDays < 30 {
           discountIOF = 1 - (0.033 * Double(timeInDays))
        }
        
        let discountedProfitIOF = discountedProfit*(1.0 - discountIOF)
        //let descontoIOF = profit - profit*(1.0 - discountIOF)
        
        //print("Leo: \(initialAmout + discountedProfitIOF)")
        
        //print("Jéssica: \(currentValue - descontoIR - descontoIOF)")
        //TODO: Verificar como disconta IOF

        let finalValue = initialAmout + discountedProfitIOF
        
        completion(finalValue, nil)
        
    }
}
