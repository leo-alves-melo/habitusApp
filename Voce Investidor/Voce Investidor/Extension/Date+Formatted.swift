//
//  Date+Formatted.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 28/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yy"
        return formatter.string(from: self)
    }
}
