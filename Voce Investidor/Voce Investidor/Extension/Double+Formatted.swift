//
//  String+Formatted.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 29/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

extension Double {
    func formattedReal() -> String {
        return String(format:
            "R$%.02f", self ).replacingOccurrences(of: ".", with: ",")
    }
}

