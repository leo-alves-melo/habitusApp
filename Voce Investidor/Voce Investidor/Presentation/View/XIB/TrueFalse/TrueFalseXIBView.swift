//
//  TrueFalseXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class TrueFalseXIBView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var bigTitle: UILabel!
    
    @IBOutlet weak var fichaView: UIView!
    @IBOutlet weak var fichaLabel: UILabel!
    
    override private init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    convenience init(frame: CGRect, fichaText: String) {
        self.init(frame: frame)
        
        self.fichaLabel.text = fichaText
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TrueFalseXIB", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //self.xContentView = contentView
    }
}
