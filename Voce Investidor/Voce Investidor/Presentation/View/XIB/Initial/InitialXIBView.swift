//
//  InitialXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 04/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// Initial Card from a topic
class InitialXIBView: CardView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var levelNumber: UILabel!
    @IBOutlet weak var bigTitle: UILabel!
    @IBOutlet weak var littleTitle: UILabel!
    @IBOutlet weak var timeToSpend: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("InitialXIB", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.xContentView = contentView
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.bigTitle.font = self.bigTitle.font.withSize(45)
            self.levelNumber.font = self.levelNumber.font.withSize(60)
            self.littleTitle.font = self.littleTitle.font.withSize(40)
            self.timeToSpend.font = self.timeToSpend.font.withSize(40)
        }
    }
}
