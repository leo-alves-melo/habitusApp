//
//  InitialXIBController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 04/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// Controller do XIB inicial
class InitialXIBController: CardController {
    private var initialCardView = InitialXIBView()
    
    init(lesson: Lesson, frame: CGRect) {
        super.init()
        
        if let bigTitle = lesson.title {
           self.initialCardView.bigTitle.text = bigTitle
        }
        if let littleTitle = lesson.description {
            self.initialCardView.littleTitle.text = littleTitle
        }
        if let levelTitle = lesson.level?.title {
            self.initialCardView.levelNumber.text = levelTitle
        }
        if let timeToSpend = lesson.duration {
            self.initialCardView.timeToSpend.text = "\(timeToSpend) min"
        }
        self.updateImage(imagePath: lesson.imagePath)

        self.cardView = self.initialCardView
    }
    
    func updateImage(imagePath: String?) {
        
        initialCardView.imageView.image = UIImage(named: "icons8-image")
        
        if let imagePath = imagePath {
            if let image = UIImage(named: imagePath) {
                initialCardView.imageView.image = image
            } else {
                if let url = URL(string: imagePath) {
                    URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
                        guard let data = data, error == nil else { return }
                        
                        if let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                self.initialCardView.imageView.image = image
                            }
                        }
                        
                    }).resume()
                }
            }
        }
    }
}
