//
//  CardFactory.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

protocol CardFactoryDelegate: class {
    func didFinishLoadingCards(error: Error?)
}

protocol RequireRequisition {}

/// The creator of every cardController
class CardFactory {
    
    weak var delegate: CardFactoryDelegate?
    var numberOfRequisitionCards = 0
    
    init(delegate: CardFactoryDelegate?) {
        self.delegate = delegate
    }
    
    /// Create cards given a list of Contents
    ///
    /// - Parameters:
    ///   - contents: The contents list
    ///   - frame: The size of the cards
    ///   - delegate: The delegate if the content is a Question
    /// - Returns: The card controller list
    func makeCards(contents: [Content], frame: CGRect) -> [CardController?] {
        var cardControllerList: [CardController?] = []
        
        for content in contents {
            cardControllerList.append(makeCard(content: content, frame: frame))
        }
        
        if self.numberOfRequisitionCards == 0 {
            self.delegate?.didFinishLoadingCards(error: nil)
        }
        
        return cardControllerList
    }
    
    /// Create cards given a full topic
    ///
    /// - Parameters:
    ///   - lesson: The lesson to create cards
    ///   - frame: The frame of the container
    /// - Returns: The list of card controllers
    func makeCards(from lesson: Lesson, frame: CGRect) -> [CardController?] {
        var cardControllerList: [CardController?] = []
        
        cardControllerList.append(InitialXIBController(lesson: lesson, frame: frame))
        
        if let contentList = lesson.contentList {
            for content in contentList {
                cardControllerList.append(makeCard(content: content, frame: frame))
            }
        }
        
        if self.numberOfRequisitionCards == 0 {
            self.delegate?.didFinishLoadingCards(error: nil)
        }
        
        return cardControllerList
    }
    
    /// Create a cardController given the content. WARNING: NOT INTERNET SAFE.
    ///
    /// - Parameters:
    ///   - content: The content of the card
    ///   - frame: The size of the card
    ///   - delegate: The delegate if the content is a Question
    /// - Returns: The cardController
    func makeCard(content: Content, frame: CGRect) -> CardController? {
        
        var cardController: CardController?
        
        if let informative = content as? Informative {
            cardController = InformationLectureXIBController(informative: informative, frame: frame)
        }
        if let singleAnswer = content as? SingleAnswer {
            if let delegate = self.delegate as? QuestionCardControllerDelegate {
                cardController = SingleAnswerXIBController(singleAnswer: singleAnswer, frame: frame, delegate: delegate)
            }
        }
        if let crisisSingleAnswer = content as? CrisisSingleAnswer {
            if let delegate = self.delegate as? QuestionCardControllerDelegate {
                cardController = CrisisSingleAnswerXIBController(crisisSingleAnswer: crisisSingleAnswer, frame: frame, delegate: delegate)
            }
        }
        if let answer = content as? Answer {
            cardController = AnswerXIBController(answer: answer, frame: frame)
        }
        if let crisisAnswer = content as? CrisisAnswer {
            cardController = CrisisAnswerXIBController(crisisAnswer: crisisAnswer, frame: frame)
        }
        if let graph = content as? Graph {
            cardController = GraphXIBViewController(graph: graph, frame: frame, delegate: self)
            self.numberOfRequisitionCards += 1
        }
        
        return cardController
    }
}

extension CardFactory: CardControllerAssincronousRequisitionDelegate {
    func didFinishRequisition(error: Error?) {
        
        guard error == nil else {
            print("Error when creating card controller in cardFactory: \(error!.localizedDescription)")
            self.delegate?.didFinishLoadingCards(error: error)
            return
        }
        
        self.numberOfRequisitionCards -= 1
        
        if self.numberOfRequisitionCards == 0 {
            self.delegate?.didFinishLoadingCards(error: nil)
        }
    }
}
