//
//  GraphXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 25/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Charts

class GraphXIBView: CardView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var valueSlider: UISlider!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var valueViewLeading: NSLayoutConstraint!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var receiveGesturesView: UIView!
    @IBOutlet weak var timeViewLeading: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var selicInformationLabel: UILabel!
    @IBOutlet weak var barPoupancaHeight: NSLayoutConstraint!
    @IBOutlet weak var barTesouroHeight: NSLayoutConstraint!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var poupancaValueLabel: UILabel!
    @IBOutlet weak var tesouroValueLabel: UILabel!
    @IBOutlet weak var diferenceValueLabel: UILabel!
    
    var graphDataDelegate: GraphDataDelegate?
    
    let thumbImageWidth = CGFloat(12.0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()

        let recognizer = UIPanGestureRecognizer(target: self, action: nil)
        self.receiveGesturesView.addGestureRecognizer(recognizer)
        self.selicInformationLabel.font = UIFont.italicSystemFont(ofSize: 12.0)
        
    }
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
        self.updateTimeViewPosition()
        self.updateTimeLabel()
        self.updateValueViewPosition()
        self.updateValueLabel()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("GraphXIB", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.xContentView = contentView
    }
    
    private func updateValueLabel() {
        
        let newValue = Int(self.calculateSliderValue())
        DispatchQueue.main.async {
            self.valueLabel.text = "R$\(newValue),00"
        }
        
    }
    
    func calculateSliderValue() -> Float {
        let value = Int(self.valueSlider.value / Float(10))
        let newValue = value*10
        return Float(newValue)
    }
    
    func calculateSliderTime() -> Float {
        let value = Int(self.timeSlider.value)

        return Float(value)
    }
    
    private func updateValueViewPosition() {
        
        let startPosition = -self.valueView.frame.width/2 + thumbImageWidth
        let finalPosition = self.valueSlider.frame.width - self.valueView.frame.width/2 - thumbImageWidth - 5
        
        let difference = finalPosition - startPosition
        
        let percentage = CGFloat((self.valueSlider.value - self.valueSlider.minimumValue)/(self.valueSlider.maximumValue - self.valueSlider.minimumValue))
        
        let newPosition = difference*percentage + startPosition
        
        DispatchQueue.main.async {
            self.valueViewLeading.constant = newPosition
        }
        
    }
    
    private func updateTimeLabel() {
        
        var time = ""

        time = "\(Int(self.timeSlider.value)) meses"

        DispatchQueue.main.async {
            self.timeLabel.text = time
        }
        
    }
    
    private func updateTimeViewPosition() {
        
        let startPosition = -self.timeView.frame.width/2 + thumbImageWidth
        let finalPosition = self.timeSlider.frame.width - self.timeView.frame.width/2 - thumbImageWidth - 5
        
        let difference = finalPosition - startPosition
        
        let percentage =
            CGFloat((self.timeSlider.value - self.timeSlider.minimumValue)/(self.timeSlider.maximumValue - self.timeSlider.minimumValue))
        
        let newPosition = difference*percentage + startPosition
        
        DispatchQueue.main.async {
            self.timeViewLeading.constant = newPosition
        }
        
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        self.graphDataDelegate?.graphDataDidChange(value: self.calculateSliderValue(), time: self.calculateSliderTime())
        self.updateValueViewPosition()
        self.updateValueLabel()
    }
    
    @IBAction func sliderTimeChanged(_ sender: Any) {
        self.graphDataDelegate?.graphDataDidChange(value: self.calculateSliderValue(), time: self.calculateSliderTime())
        self.updateTimeLabel()
        self.updateTimeViewPosition()
    }

}
