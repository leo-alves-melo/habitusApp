//
//  GraphXIBViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 25/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Charts

protocol GraphDataDelegate: class {
    func graphDataDidChange(value: Float, time: Float)
}

class GraphXIBViewController: CardController {
    private var graphCardView = GraphXIBView()
    
    var xLabel: String = "Meses"
    var yLabel: String = "Porcentagem"
    
    var xData: [String] = ["Poupança", "Tesouro Selic"]
    
    var lastSelicValue: Float = 0.0
    var lastTrValue: Float = 0.0
    
    var maximumValue: Double = 0.0
    var minimumValue: Double = 0.0
    
    var barColors: [UIColor] = [UIColor(red: 153.0/255.0,
                                        green: 16.0/255.0,
                                        blue: 208.0/255.0,
                                        alpha: 1),
                                UIColor(red: 245.0/255.0,
                                       green: 166.0/255.0,
                                       blue: 35.0/255.0,
                                       alpha: 1)]
    
    init(graph: Graph, frame: CGRect, delegate: CardControllerAssincronousRequisitionDelegate) {
        super.init()
        
        self.graphCardView.graphDataDelegate = self
        
        guard let maximunAmmountInvested = graph.maximunAmmountInvested,
            let minimunAmmountInvested = graph.minimunAmmountInvested,
            let maximunTime = graph.maximunTime,
            let minimunTime = graph.minimunTime
            else {
                return
        }
        
        self.graphCardView.timeSlider.minimumValue = minimunTime
        self.graphCardView.timeSlider.maximumValue = maximunTime
        self.graphCardView.valueSlider.minimumValue = minimunAmmountInvested
        self.graphCardView.valueSlider.maximumValue = maximunAmmountInvested
        
        let middleAmmountInvested = (maximunAmmountInvested + minimunAmmountInvested)/2
        let middleTime = (maximunTime + minimunTime)/2

        self.graphCardView.frame = frame
        
        self.makeGraph(graph: graph)
        
        self.updateGraph(selic: self.lastSelicValue, trValue: self.lastTrValue,
                         timeInMonths: middleTime, ammountInvested: middleAmmountInvested)
        
        BrazilOpenDataService().getSelicData { (dailySelicTax, error) in
            guard let dailySelicTax = dailySelicTax, error == nil else {
                print("Error in get Selic on BrazilOpenDataService: \(error!)")
                delegate.didFinishRequisition(error: NSError(domain: "", code: 0, userInfo: nil))
                return
            }

            if let lastSelic = dailySelicTax.last {
                if let lastSelicValue = lastSelic.value {
                    
                    self.lastSelicValue = lastSelicValue
                    
                    BrazilOpenDataService().getTRTaxMonthly(completion: { (trTaxes, error) in
                        guard let trTaxes = trTaxes, error == nil else {
                            print("Error in get Selic on BrazilOpenDataService: \(error!)")
                            delegate.didFinishRequisition(error: NSError(domain: "", code: 0, userInfo: nil))
                            return
                        }
                        
                        if let lastTrTax = trTaxes.last {
                            DispatchQueue.main.async {
                                
                                if let trValue = lastTrTax.value {

                                    self.lastTrValue = trValue
                                    
                                    if let date = lastSelic.date {
                                        self.updateSelicLabel(with: date)
                                    }
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.updateGraph(selic: lastSelicValue, trValue: trValue,
                                                         timeInMonths: self.graphCardView.calculateSliderTime(), ammountInvested: self.graphCardView.calculateSliderValue())
                                    }
                                    
                                    delegate.didFinishRequisition(error: nil)
                                }
                                
                            }
                        }
                    })

                }
            }
        }
        
        self.cardView = graphCardView
    }
    
    private func updateSelicLabel(with date: Date) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: date)
        
        self.graphCardView.selicInformationLabel.text = "* Baseado na SELIC de \(dateString) descontando IR"
    }
    
    private func calculateTesouro(selic: Float, timeInMonths: Float, ammountInvested: Float) -> Double {
        
        let monthlySelic = (selic*22.0)/100.0
        
        var currentAmmount = 0.0
        let maintainedTax = 0.0015
        
        for mounth in 0..<Int(timeInMonths) {
            currentAmmount *= Double(1.0+monthlySelic)
            currentAmmount += Double(ammountInvested)
            
            //Maintenance Tax
            if (mounth + 1) % 6 == 0 {
                currentAmmount *= (1-maintainedTax)
            }
            
        }
        
        //Maintainded tax to get the money
        currentAmmount *= (1 - (maintainedTax*(Double(Int(timeInMonths) % 6))/6))

        var discount = 0.0
        
        if timeInMonths < 6 {
            discount = 0.225
        } else if timeInMonths < 12 {
            discount = 0.2
        } else if timeInMonths < 24 {
            discount = 0.175
        } else {
            discount = 0.15
        }
        
        let totalDiscount = (currentAmmount - Double(ammountInvested)*Double(timeInMonths))*(1.0 - discount)
        
        return Double(ammountInvested)*Double(timeInMonths) + totalDiscount
    }
    
    private func calculatePoupanca(selic: Float, trValue: Float,
                                   timeInMonths: Float, ammountInvested: Float) -> Double {
        
        let monthlySelic = (selic*22.0)/100.0
        
        //If anual selic is < 8,5%, we reduce the gain of poupança
        if monthlySelic*12 < 8.5 {
            
            var currentAmmount = 0.0
            
            //let monthlySelic2 = 0.003715
            for _ in 0..<Int(timeInMonths) {
                currentAmmount *= Double(1.0+0.7*monthlySelic)
                currentAmmount += Double(ammountInvested)
                
            }
            
            return currentAmmount
        } else {
            
            let poupanca = 0.005 + trValue/100
            
            var currentAmmount = 0.0
            
            for _ in 0..<Int(timeInMonths) {
                currentAmmount += Double(ammountInvested)
                currentAmmount *= Double(1.0+poupanca)
            }
            
            return currentAmmount
        }
        
    }
    
    private func setPoupancaValueLabel(with value: Double) {
        
        let valueTimes100 = value*100.0
        let intValue = Int(valueTimes100)
        let newValue = Double(intValue)/100.0
        let valueString = String(format: "%.2f", newValue)
        self.graphCardView.poupancaValueLabel.text = "R$\(valueString)".replacingOccurrences(of: ".", with: ",")
    }
    
    private func setTesouroValueLabel(with value: Double) {
        let valueTimes100 = value*100.0
        let intValue = Int(valueTimes100)
        let newValue = Double(intValue)/100.0
        let valueString = String(format: "%.2f", newValue)
        self.graphCardView.tesouroValueLabel.text = "R$\(valueString)".replacingOccurrences(of: ".", with: ",")
    }
    
    private func setDiferenceValueLabel(with value: Double) {
        let valueTimes100 = value*100.0
        let intValue = Int(valueTimes100)
        let newValue = Double(intValue)/100.0
        
        let valueString = String(format: "%.2f", abs(newValue))
        
        if value < 0.0 {
            self.graphCardView.diferenceValueLabel.text = "-R$\(valueString)".replacingOccurrences(of: ".", with: ",")
        } else {
            self.graphCardView.diferenceValueLabel.text = "+R$\(valueString)".replacingOccurrences(of: ".", with: ",")
        }
        
    }
    
    private func updateGraph(selic: Float, trValue: Float, timeInMonths: Float, ammountInvested: Float) {

        let tesouroValue = self.calculateTesouro(selic: selic,
                                                 timeInMonths: timeInMonths, ammountInvested: ammountInvested)
        let poupancaValue = self.calculatePoupanca(selic: selic,
                                                   trValue: trValue,
                                                   timeInMonths: timeInMonths,
                                                   ammountInvested: ammountInvested)
        
        self.setTesouroValueLabel(with: tesouroValue)
        self.setPoupancaValueLabel(with: poupancaValue)
        self.setDiferenceValueLabel(with: tesouroValue - poupancaValue)
        
        self.graphCardView.barTesouroHeight.constant = (self.graphCardView.graphView.frame.height - 60) * CGFloat((tesouroValue - self.minimumValue)/(self.maximumValue - self.minimumValue))
        
        self.graphCardView.barPoupancaHeight.constant = (self.graphCardView.graphView.frame.height - 60) * CGFloat((poupancaValue - self.minimumValue)/(self.maximumValue - self.minimumValue))
        
    }
    
    private func makeGraph(graph: Graph) {

        //self.graphCardView.chartView.delegate = self
        
        self.maximumValue = Double(graph.maximunAmmountInvested!)*Double(graph.maximunTime!)*1.2
        self.minimumValue = 0.0
        
    }
}

extension GraphXIBViewController: ChartViewDelegate {
    
}

extension GraphXIBViewController: GraphDataDelegate {
    func graphDataDidChange(value: Float, time: Float) {
        self.updateGraph(selic: self.lastSelicValue, trValue: self.lastTrValue,
                         timeInMonths: time, ammountInvested: value)
    }

}
