//
//  BarChartXAxisRenderer.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 10/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Charts

// swiftlint:disable all
class BarChartXAxisRenderer: XAxisRenderer {
    
    
    override func drawGridLine(context: CGContext, x: CGFloat, y: CGFloat) {
        super.drawGridLine(context: context, x: x, y: y)
    }
    
    override func drawLabel(context: CGContext, formattedLabel: String,
                            x: CGFloat, y: CGFloat,
                            attributes: [NSAttributedStringKey: Any],
                            constrainedToSize: CGSize,
                            anchor: CGPoint, angleRadians: CGFloat) {
        let path = UIBezierPath(rect: CGRect.init(x: x , y: y-4,
                                                  width: 4,
                                                  height: 3))
        context.addPath(path.cgPath)
        context.setFillColor(UIColor.red.cgColor)
        context.fillPath()
        
        let newLabel = "R$\(formattedLabel)"
        super.drawLabel(context: context, formattedLabel: newLabel,
                        x: x, y: y, attributes: attributes,
                        constrainedToSize: constrainedToSize,
                        anchor: anchor, angleRadians: angleRadians)
    }
    
}
