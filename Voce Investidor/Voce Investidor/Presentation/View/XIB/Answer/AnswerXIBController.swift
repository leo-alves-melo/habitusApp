//
//  AnswerXIBController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// The controller of an Answer cardView
class AnswerXIBController: CardController {
    private var answerCardView = AnswerXIBView()
    
    init(answer: Answer, frame: CGRect) {
        super.init()
        
        if let response = answer.response {
            if response == true {
                self.answerCardView.answerLabel.text = "Correto!"
                self.answerCardView.answerImageView.image = UIImage(named: "Correct")
                self.answerCardView.backgroundImage.image = UIImage(named: "correctAnswerBackground")
            } else {
                self.answerCardView.answerLabel.text = "Errado!"
                self.answerCardView.answerImageView.image = UIImage(named: "Wrong")
                self.answerCardView.backgroundImage.image = UIImage(named: "wrongAnswerBackground")
            }
        }
        
        if let explanation = answer.explanation {
            self.answerCardView.explanationLabel.attributedText = self.replaceMarkup(on: explanation)
        }
        
        self.answerCardView.frame = frame
        
        self.cardView = self.answerCardView
        
    }
}
