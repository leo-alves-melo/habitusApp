//
//  QuestionCardController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 06/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// Protocol to interact viewController with questionCard
protocol QuestionCardControllerDelegate: NSObjectProtocol {
    func didSelected(option: [Int])
    func didDeselected()
    func didAnswer(answer: Answer)
}

/// The controller of a questionCard
class QuestionCardController: CardController {
    weak var delegate: QuestionCardControllerDelegate?
    internal var privateSelectedOption: [Int] = []
    var selectedOption: [Int] {
        get {
            return privateSelectedOption
        }
        set(value) {
            self.delegate?.didSelected(option: value)
            self.privateSelectedOption = value
        }
    }
    func startAction() {}
    func getActionTitle() -> String {return ""}
    func getActionColor() -> UIColor {return UIColor.clear}
    func getActionAlpha() -> CGFloat {return CGFloat(1.0)}
}
