//
//  CardXIB.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import CoreMotion

/// A view of cards
class CardView: SwipeableCardView {
    
    var xContentView: UIView!
    
    private static let kInnerMargin: CGFloat = 0.0
    var shadowView: UIView?
    
    var motionManager = CMMotionManager()
    
    override func layoutSubviews() {
        super.layoutSubviews()

        configureShadow()
    }
    
    func configureShadow() {
        // Shadow View
        self.shadowView?.removeFromSuperview()
        let shadowView = UIView(frame: CGRect(x: CardView.kInnerMargin,
                                              y: CardView.kInnerMargin,
                                              width: self.bounds.width - (2 * CardView.kInnerMargin),
                                              height: self.bounds.height - (2 * CardView.kInnerMargin)))
        self.insertSubview(shadowView, at: 0)
        self.shadowView = shadowView
        
//        // Roll/Pitch Dynamic Shadow
//        if motionManager.isDeviceMotionAvailable {
//            motionManager.deviceMotionUpdateInterval = 0.02
//            motionManager.startDeviceMotionUpdates(to: .main, withHandler: { (motion, _) in
//                if let motion = motion {
//                    let pitch = motion.attitude.pitch * 10 // x-axis
//                    let roll = motion.attitude.roll * 10 // y-axis
//                    self.applyShadow(width: CGFloat(roll), height: CGFloat(pitch))
//                }
//            })
//        }
        self.applyShadow(xOffset: 0.0, yOffset: 4.0)
    }
    
    private func applyShadow(xOffset: CGFloat, yOffset: CGFloat) {
        if let shadowView = shadowView {
            let shadowPath = UIBezierPath(roundedRect: shadowView.bounds, cornerRadius: 14.0)
            shadowView.layer.masksToBounds = false
            shadowView.layer.shadowRadius = 2.0
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: xOffset, height: yOffset)
            shadowView.layer.shadowOpacity = 0.15
            shadowView.layer.shadowPath = shadowPath.cgPath
            shadowView.layer.cornerRadius = 5.0
        }
    }
    
}
