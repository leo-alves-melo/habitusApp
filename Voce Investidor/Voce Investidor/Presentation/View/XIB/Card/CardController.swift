//
//  CardController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

protocol CardControllerAssincronousRequisitionDelegate: class {
    func didFinishRequisition(error: Error?)
}

class CardController {
    
    static let markupRenderer: MarkupRenderer = MarkupRenderer(baseFont: cardsFont)

    var cardView: CardView!
    
    func replaceMarkup(on text: String) -> NSMutableAttributedString {
        let attString = CardController.markupRenderer.render(text: text)
        return NSMutableAttributedString(attributedString: attString)
    }
}
