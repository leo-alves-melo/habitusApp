//
//  SingleAnswerXIBController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// The controller of a Single Answer Controller
class SingleAnswerXIBController: QuestionCardController {
    
    var answerCardController: AnswerXIBController!
    private var singleCardView: SingleAnswerXIBView!
    private var options: [Option]!
    
    init(singleAnswer: SingleAnswer, frame: CGRect, delegate: QuestionCardControllerDelegate) {

        super.init()
        
        self.singleCardView = SingleAnswerXIBView(frame: frame, controller: self)
        self.delegate = delegate
        
        if let question = singleAnswer.question {
            self.singleCardView.questionLabel.attributedText = self.replaceMarkup(on: question)
        }
        if let options = singleAnswer.options {
            self.options = options
            if options.count > 0 && options[0].alternative != nil {
                self.singleCardView.option1Label.attributedText = self.replaceMarkup(on: options[0].alternative!)
            }
            if options.count > 1 && options[1].alternative != nil {
                self.singleCardView.option2Label.attributedText = self.replaceMarkup(on: options[1].alternative!)
            }
            if options.count > 2 && options[2].alternative != nil {
                self.singleCardView.option3Label.attributedText = self.replaceMarkup(on: options[2].alternative!)
            } else {
                self.singleCardView.option3View.isHidden = true
            }
        }
        
        self.cardView = self.singleCardView
        self.singleCardView.frame = frame
    }
    
    /// Create the answer based on the response
    func createAnswerCard() {
        if let options = self.options {
            if let isCorrect = options[self.privateSelectedOption[0]].isCorrect {
                if let explanation = options[self.privateSelectedOption[0]].explanation {
                    let answer = Answer(response: isCorrect, explanation: explanation)
                    if let answerCardController = CardFactory(delegate: nil).makeCard(content: answer,
                                                                       frame: self.cardView.frame) {
                        
                        if let answerCardController = answerCardController as? AnswerXIBController {
                            self.answerCardController = answerCardController
                            
                            self.answerCardController.cardView.xContentView.layer.cornerRadius = self.singleCardView.xContentView.layer.cornerRadius
                            self.answerCardController.cardView.xContentView.layer.masksToBounds = self.singleCardView.xContentView.layer.masksToBounds
                            self.answerCardController.cardView.delegate = self.singleCardView.delegate
                        }
                    }
                }
                
            }
            
        }
    }
    
    /// Start the action of the ViewController Button
    override func startAction() {
        if self.cardView is SingleAnswerXIBView {
            self.createAnswerCard()
            self.cardView = self.answerCardController.cardView
            
            UIView.transition(from: self.singleCardView.xContentView,
                              to: self.answerCardController.cardView.xContentView,
                              duration: 0.5,
                              options: .transitionFlipFromRight, completion: nil)
        } else {
            self.restartQuestion(completion: nil)
        }
    }
    
    /// Restart the Question. User try to answer again
    func restartQuestion(completion: ((Bool)->Void)?) {
        self.cardView = self.singleCardView
        UIView.transition(from: self.answerCardController.cardView.xContentView,
                          to: self.singleCardView.xContentView,
                          duration: 0.5,
                          options: .transitionFlipFromLeft, completion: completion)
    }
    
    override func getActionTitle() -> String {
        if self.cardView is SingleAnswerXIBView {
            return "Confirmar"
        } else {
            return "Refazer"
        }
    }
    
    override func getActionColor() -> UIColor {
        if self.cardView is SingleAnswerXIBView {
            return confirmButtonColor
        } else {
            return redoButtonColor
        }
    }
    
    override func getActionAlpha() -> CGFloat {
        if let singleAnswer = self.cardView as? SingleAnswerXIBView {
            
            if singleAnswer.option1Button.isSelected || singleAnswer.option2Button.isSelected || singleAnswer.option3Button.isSelected {
                return CGFloat(1.0)
            } else {
                return CGFloat(0.5)
            }
        } else {
            return CGFloat(1.0)
        }
    }
}


