//
//  InformationLectureXIBController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class InformationLectureXIBController: CardController {

    private var lectureCardView = InformativeLectureXIBView()
    
    init(informative: Informative, frame: CGRect) {
        super.init()
        
        //Tipo de Estilo
        let paragraphStyle = NSMutableParagraphStyle()
        //Espaçamento entre linhas
        paragraphStyle.lineSpacing = 6.3
        let attrString = self.replaceMarkup(on: informative.text!)
        //Add o estilo paragro com o espaçamento no texto
        attrString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrString.length))
        //Add texto já personalizado na label
        self.lectureCardView.lectureLabel.attributedText = attrString
        self.lectureCardView.lectureLabel.adjustsFontSizeToFitWidth = true
        self.lectureCardView.lectureLabel.lineBreakMode = .byClipping
        
        self.lectureCardView.title.text = informative.title
        self.updateImage(imagePath: informative.imagePath)
        self.cardView = self.lectureCardView
        self.lectureCardView.frame = frame
    }
    
    func updateImage(imagePath: String?) {
        
        lectureCardView.lectureImageView.image = UIImage(named: "icons8-image")
        
        if let imagePath = imagePath {
            if let image = UIImage(named: imagePath) {
                lectureCardView.lectureImageView.image = image
            } else {
                if let url = URL(string: imagePath) { // TODO: Qualquer string passa aqui
                    URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
                        guard let data = data, error == nil else { return }

                        DispatchQueue.main.async {
                            self.lectureCardView.lectureImageView.image = UIImage(data: data)
                        }
                    }).resume()
                }
            }
        }
    }
}
