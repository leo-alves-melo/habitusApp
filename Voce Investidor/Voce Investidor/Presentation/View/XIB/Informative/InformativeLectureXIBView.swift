//
//  ContentLectureXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class InformativeLectureXIBView: CardView {
    
    @IBOutlet weak var lectureImageView: UIImageView!
    @IBOutlet weak var lectureLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
    }
    
    convenience init(frame: CGRect, lectureImage: UIImage, lectureLabelText: String) {
        self.init(frame: frame)
 
        self.lectureImageView.image = lectureImage
        self.lectureLabel.text = lectureLabelText
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("InformativeLectureXIB", owner: self, options: nil)
        
        self.addSubview(contentView)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.xContentView = contentView
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.title.font = self.title.font.withSize(37)
            self.lectureLabel.font = self.lectureLabel.font.withSize(30)
        }
    }
}
