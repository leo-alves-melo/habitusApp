//
//  CrisisSingleAnswerXIBView.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 29/08/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import UIKit

/// View of a Crisis Single Answer Question
class CrisisSingleAnswerXIBView: CardView {
    private var optionSelected: Int?
    weak var controller: CrisisSingleAnswerXIBController?

    @IBOutlet private var contentView: UIView!
    
    @IBOutlet var questionLabel: UILabel!
    
    @IBOutlet var option1Button: UIButton!
    @IBOutlet var option1Image: UIImageView!
    @IBOutlet var option1Label: UILabel!
    
    @IBOutlet var option2Button: UIButton!
    @IBOutlet var option2Image: UIImageView!
    @IBOutlet var option2Label: UILabel!
    
    @IBOutlet var option3Button: UIButton!
    @IBOutlet var option3Image: UIImageView!
    @IBOutlet var option3Label: UILabel!
    @IBOutlet var option3View: UIView!
    
    @IBOutlet weak var option4Label: UILabel!
    
    @IBOutlet weak var option4Image: UIImageView!
    @IBOutlet weak var option4Button: UIButton!
    @IBOutlet weak var option4View: UIView!
    
    init(frame: CGRect, controller: CrisisSingleAnswerXIBController) {
        super.init(frame: frame)
        self.controller = controller
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CrisisSingleAnswerXIB", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.xContentView = contentView
        
        if UIDevice.current.userInterfaceIdiom == .pad {

//            self.subtitleCardLabel.font = self.subtitleCardLabel.font.withSize(30)
            self.questionLabel.font = self.questionLabel.font.withSize(30)
            self.option1Label.font = self.option1Label.font.withSize(30)
            self.option2Label.font = self.option2Label.font.withSize(30)
            self.option3Label.font = self.option3Label.font.withSize(30)
            self.option4Label.font = self.option3Label.font.withSize(30)
        }
    }
    
    private func returnSelectedButtons() {
        var buttons: [Int] = []
        if self.option1Button.isSelected {
            buttons.append(0)
        }
        if self.option2Button.isSelected {
            buttons.append(1)
        }
        if self.option3Button.isSelected {
            buttons.append(2)
        }
        if self.option4Button.isSelected {
            buttons.append(3)
        }
        
        self.controller?.selectedOption = buttons
    }
    
    @IBAction private func option1Pressed(_ sender: Any) {
        optionSelected = 1
        buttonSelect(option: .option1)
        buttonDeselect(option: .option2)
        buttonDeselect(option: .option3)
        buttonDeselect(option: .option4)
        
        returnSelectedButtons()
    }
    
    @IBAction private func option2Pressed(_ sender: Any) {
        optionSelected = 2
        buttonDeselect(option: .option1)
        buttonSelect(option: .option2)
        buttonDeselect(option: .option3)
        buttonDeselect(option: .option4)
        
        returnSelectedButtons()
    }
    
    @IBAction private func option3Pressed(_ sender: Any) {
        optionSelected = 3
        buttonDeselect(option: .option1)
        buttonDeselect(option: .option2)
        buttonSelect(option: .option3)
        buttonDeselect(option: .option4)
        
        returnSelectedButtons()
    }
    
    @IBAction func option4Pressed(_ sender: Any) {
        optionSelected = 4
        buttonDeselect(option: .option1)
        buttonDeselect(option: .option2)
        buttonDeselect(option: .option3)
        buttonSelect(option: .option4)
        
        returnSelectedButtons()
    }
    
    private func buttonSelect(option: OptionEnum) {
        switch option {
        case .option1:
            self.option1Image.image = #imageLiteral(resourceName: "buttonSelected")
            self.option1Button.isSelected = true
        case .option2:
            self.option2Image.image = #imageLiteral(resourceName: "buttonSelected")
            self.option2Button.isSelected = true
        case .option3:
            self.option3Image.image = #imageLiteral(resourceName: "buttonSelected")
            self.option3Button.isSelected = true
        case .option4:
            self.option4Image.image = #imageLiteral(resourceName: "buttonSelected")
            self.option4Button.isSelected = true
        }
    }
    
    private func buttonDeselect(option: OptionEnum) {
        switch option {
        case .option1:
            self.option1Image.image = #imageLiteral(resourceName: "buttonDeselected")
            self.option1Button.isSelected = false
        case .option2:
            self.option2Image.image = #imageLiteral(resourceName: "buttonDeselected")
            self.option2Button.isSelected = false
        case .option3:
            self.option3Image.image = #imageLiteral(resourceName: "buttonDeselected")
            self.option3Button.isSelected = false
        case .option4:
            self.option4Image.image = #imageLiteral(resourceName: "buttonDeselected")
            self.option4Button.isSelected = false
        }
    }
}
