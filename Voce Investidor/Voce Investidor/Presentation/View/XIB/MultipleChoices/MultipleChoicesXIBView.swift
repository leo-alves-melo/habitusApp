//
//  MultipleChoicesXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class MultipleChoicesXIBView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MultipleChoicesXIB", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //self.xContentView = contentView
    }
}
