//
//  AnswerXIBView.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// The card view of an Answer
class CrisisAnswerXIBView: CardView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var answerImageView: UIImageView!
    @IBOutlet weak var answerLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CrisisAnswer", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.xContentView = contentView
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.answerLabel.font = self.answerLabel.font.withSize(30)
        }
    }
}
