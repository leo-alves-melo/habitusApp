//
//  AnswerXIBController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 31/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// The controller of an Answer cardView
class CrisisAnswerXIBController: CardController {
    private var answerCardView = CrisisAnswerXIBView()
    
    init(crisisAnswer: CrisisAnswer, frame: CGRect) {
        super.init()
        
        // crisisAnswer has no response
        if let explanation = crisisAnswer.explanation {
            self.answerCardView.answerLabel.attributedText = self.replaceMarkup(on: explanation)
        }
        
        self.answerCardView.frame = frame
        
        self.cardView = self.answerCardView
    }
}
