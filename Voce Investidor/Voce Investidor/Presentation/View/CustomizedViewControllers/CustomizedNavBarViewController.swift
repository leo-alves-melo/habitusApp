//
//  CustomizedNavBarViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 18/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

/// ViewController with customized tabBar with image
class CustomizedNavBarViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    /// Set style of naviigationBar
    func customizeNavBar() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isTranslucent = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            //Is IPad
            self.navigationController?.navigationBar.barTintColor =  UIColor(patternImage:
                UIImage(named: "navibarBackgroundIpad")!)
        } else {
            //Is IPhone
            self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:
                UIImage(named: "navibarBackground")!)
        }
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
}
