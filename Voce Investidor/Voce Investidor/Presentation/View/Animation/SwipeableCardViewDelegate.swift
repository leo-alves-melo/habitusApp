//
//  TopicControllerDelegate.swift
//  Voce-Investidor
//
//  Created by Gustavo Crivelli on 13/09/18.
//  Copyright © 2018 Gustavo Crivelli. All rights reserved.
//

import Foundation

// Delegate do controlador do tópico. Responsável por comunicar com o SwipeableCardViewContainer.
protocol LessonControllerDelegate: class {
    
    // Ação a se tomar quando um card é selecionado. Provavelmente não será usado, mas tá aí.
    func select(card: SwipeableCardView, atIndex index: Int)
    
    // Ação a se tomar quando o usuário passou a carta atual.
    func dismissCard()
    
    // Ação a se tomar quando o usuário chamou de volta a carta anterior.
    func reinsertCard()
    
    // Retorna o index da carta atual
    func getCurrentCardIndex() -> Int
}
