//
//  SwipeableStackView.swift
//  Swipeable-View-Stack
//
//  Created by Phill Farrugia on 10/21/17.
//  Copyright © 2017 Phill Farrugia. All rights reserved.
//


import UIKit

class SwipeableCardViewContainer: UIView, SwipeableViewDelegate {

    // Offset horizontal de distancia entre cards da stack
    static let horizontalInset: CGFloat = 0.0

    // Offset vertical de distancia entre cards da stack
    static let verticalInset: CGFloat = 10.0

    var dataSource: SwipeableCardViewDataSource? {
        didSet {
            reloadData()
        }
    }
    
    static let leadingDistanceFromScreen: CGFloat = 200.0

    var initialCardIndex: Int!
    
    weak var delegate: LessonControllerDelegate?
    
    private var visibleCardViews: [SwipeableCardView] {
        return subviews as? [SwipeableCardView] ?? []
    }

    fileprivate var remainingCards: Int = 0

    static let numberOfVisibleCards: Int = 3

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }

    /// Reloads the data used to layout card views in the
    /// card stack. Removes all existing card views and
    /// calls the dataSource to layout new card views.
    func reloadData() {
    
        guard let dataSource = dataSource else {
            return
        }

        removeAllCardViews()
        
        self.initialCardIndex = dataSource.getCurrentCardIndex()
        
        let numberOfCards = dataSource.numberOfCards()
        remainingCards = numberOfCards

        for index in 0 ..< numberOfCards {
            let cardView = dataSource.card(forItemAtIndex: index)
            addCardView(cardView: cardView, atIndex: index - initialCardIndex)
            cardView.isHidden = index < initialCardIndex || index >= initialCardIndex + SwipeableCardViewContainer.numberOfVisibleCards
        }

        if let emptyView = dataSource.viewForEmptyCards() {
            addEdgeConstrainedSubView(view: emptyView)
        }

        print(initialCardIndex)
        self.newUpdateCardStack(from: initialCardIndex, to: initialCardIndex)
        
        setNeedsLayout()
    }

    private func addCardView(cardView: SwipeableCardView, atIndex index: Int, reinsert: Bool = false) {
        
        cardView.pop_removeAllAnimations()
        cardView.delegate = self
        cardView.isActive = true
        cardView.alpha = 1.0
        cardView.layer.transform = CATransform3DIdentity

        setFrame(forCardView: cardView, atIndex: index)
        
        if !reinsert {
            insertSubview(cardView, at: 0)
            remainingCards -= 1
        } else {

            self.addSubview(cardView)
            self.bringSubview(toFront: cardView)
        }
    }

    private func removeAllCardViews() {
        for cardView in visibleCardViews {
            cardView.removeFromSuperview()
        }
    }

    /// Sets the frame of a card view provided for a given index. Applies a specific
    /// horizontal and vertical offset relative to the index in order to create an
    /// overlay stack effect on a series of cards.
    ///
    /// - Parameters:
    ///   - cardView: card view to update frame on
    ///   - index: index used to apply horizontal and vertical insets
    private func setFrame(forCardView cardView: SwipeableCardView, atIndex index: Int) {

        var cardViewFrame = bounds
        let horizontalInset = CGFloat(index) * SwipeableCardViewContainer.horizontalInset
        let verticalMultiplier = CGFloat(min(index, SwipeableCardViewContainer.numberOfVisibleCards - 1))
        let verticalInset = verticalMultiplier * SwipeableCardViewContainer.verticalInset
        
        // Se o index for menor que zero, então a carta precede a carta do topo da pilha
        // Ou seja, já foi vista e passada, e deve se mover para a lateral esquerda, fora da tela
        // Caso contrário, a carta está sendo vista ou ainda será vista, e movemos ela para cima na pilha
        if index < 0 {
            cardViewFrame.origin.x = -SwipeableCardViewContainer.leadingDistanceFromScreen - cardViewFrame.width
        } else {
            cardViewFrame.origin.y += verticalInset
        }
        
        cardViewFrame.size.width -= 3 * horizontalInset
        cardViewFrame.origin.x += horizontalInset
        cardView.frame = cardViewFrame
    }
}

// MARK: - SwipeableViewDelegate

extension SwipeableCardViewContainer {

    func didTap(view: SwipeableView) {
        // React to tap?
    }
    
    func didBeginSwipe(onView view: SwipeableView) {
        // React to Swipe Began?
    }

    func didEndSwipe(onView view: SwipeableView) {
        // Wait a bit, then reset view transforms
        //view.timedResetCardBounds(to: CGPoint.zero - CGPoint(x: 2 * view.bounds.width, y: 0.0), withTimeInterval: 0.3)
        
        // Deactivate view for purposes of extra animations/interactions
        view.isActive = false
        
        // Notify delegate that card should be dismissed
        delegate?.dismissCard()
    }
    
    func newUpdateCardStack(from prevTopCardIndex: Int, to topCardIndex: Int) {
        
        guard let dataSource = dataSource else {
            return
        }
        
        if prevTopCardIndex > topCardIndex {
            dataSource.card(forItemAtIndex: topCardIndex).cancelAnimationsAndResetOffscreen()
        }
        
        for index in topCardIndex ..< topCardIndex + SwipeableCardViewContainer.numberOfVisibleCards {
            guard index < dataSource.numberOfCards() else { break }
            let currentCard = dataSource.card(forItemAtIndex: index)
            currentCard.isHidden = false
            currentCard.isActive = true
        }
        
        // Update all existing card's frames based on new indexes, animate frame change
        // to reveal new card from underneath the stack of existing cards.
        // ATTENTION! Swiping to dismiss will mark a card as inactive, to prevent weird animation stuff.
        // What this means is that setFrame is not called for cards that are swiped!
        for (cardIndex, cardView) in visibleCardViews.reversed().enumerated()
        where cardView.isActive {
            if cardIndex < topCardIndex || cardIndex >= topCardIndex + SwipeableCardViewContainer.numberOfVisibleCards {
                
                UIView.animate(withDuration: 0.2, animations: {
                    cardView.center = self.center
                    self.setFrame(forCardView: cardView, atIndex: cardIndex - topCardIndex)
                    self.layoutIfNeeded()
                }, completion: { finished in
                    if finished {
                        cardView.isHidden = true
                    }
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    cardView.center = self.center
                    self.setFrame(forCardView: cardView, atIndex: cardIndex - topCardIndex)
                    self.layoutIfNeeded()
                })
            }
        }
        
        print("cardStack updated")
    }
}
