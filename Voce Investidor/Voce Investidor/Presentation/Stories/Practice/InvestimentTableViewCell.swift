//
//  InvestimentTableViewCell.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 27/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class InvestimentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleInvestimentLabel: UILabel!
    @IBOutlet weak var valueInvestimentLabel: UILabel!
    @IBOutlet weak var dateInvestimentLabel: UILabel!
    @IBOutlet weak var brokerImage: UIImageView!
    
    func prepare(with investiment: Investment) {
        self.titleInvestimentLabel.text = investiment.name
        self.valueInvestimentLabel.text = investiment.currentAmount?.formattedReal() ?? "R$ 0"
        self.dateInvestimentLabel.text = investiment.date?.formatted ?? "Sem data"
        
        if let broker = investiment.broker, let imagePath = broker.miniImagePath, let image = UIImage(named: imagePath) {
            self.brokerImage.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
