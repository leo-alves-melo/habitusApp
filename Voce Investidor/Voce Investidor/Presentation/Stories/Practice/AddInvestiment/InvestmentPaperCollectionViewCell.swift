//
//  InvestmentPaperCollectionViewCell.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 07/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class InvestmentPaperCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var paperName: UILabel!
    @IBOutlet weak var paperIndex: UILabel!
    @IBOutlet weak var paperDate: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    func prepare(with investmentPaper: InvestmentPaper) {
        self.paperName.text = investmentPaper.name
        self.paperDate.text = investmentPaper.due?.formatted
        self.paperIndex.text = investmentPaper.calculationString
    }
    
    func setSelected() {
        self.selectedImage.isHidden = false
        self.mainView.alpha = 1.0
    }
    
    func setDeselected() {
        self.selectedImage.isHidden = true
        self.mainView.alpha = 0.7
    }
}
