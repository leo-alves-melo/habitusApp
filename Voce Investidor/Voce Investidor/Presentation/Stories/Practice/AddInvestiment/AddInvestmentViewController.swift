//
//  AddInvestmentViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 07/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class AddInvestmentViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var fractionTextField: UITextField!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var tesouroCollectionView: UICollectionView!
    @IBOutlet weak var brokerCollectionView: UICollectionView!
    var deadlinePickerView = UIDatePicker()
    
    @IBOutlet weak var deleteValueView: UIView!
    @IBOutlet weak var tapView: UIView!
    var userBrokers: [BrokerAccount] = []
    var papers: [InvestmentPaper] = []
    var selectionIndexPaper = -1
    var selectionIndexBroker = -1
    let toolbarHeight = 50
    
    var goal: Goal!
    
    var receivedBrokers = false
    var receivedPapers = false
    
    var investmentToEdit: Investment?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupKeyboard()
        self.setupPickerView()
        self.setupTextFields()
        self.hideActivityView(hide: false)
        
        BrokerAccountServices().brokerAccounts(from: User.instance, completion: self.requestBrokers(brokers:error:))
        InvestmentPaperServices().investmentPapers(completion: self.requestInvestmentPapers(investmentPapers:error:))
        
        if let investmentToEdit = self.investmentToEdit {
            self.updateData(with: investmentToEdit)
            
        } else {
            self.deleteValueView.isHidden = true
        }
    }
    
    func index(of broker: BrokerAccount) -> Int {
        var index = 0

        while index < self.userBrokers.count {
            if self.userBrokers[index].name == broker.name { //TODO: Avaliar também a data de abertura?
                return index
            }
            
            index += 1
        }

        return index
    }
    
    func index(of investmentPaperID: String) -> Int {
        var index = 0
        
        while index < self.papers.count {
            if self.papers[index].identifier == investmentPaperID {
                return index
            }
            
            index += 1
        }
        
        return index
    }
    
    func updateData(with investment: Investment) {
        self.dateTextField.text = investment.date?.formatted
        self.valueTextField.text = self.currencyInputFormatting(value: "\(investment.initialAmount! * 10.0)")
        self.fractionTextField.text = self.fractionInputFormatting(value: "\(investment.fraction! * 10)")
    }
    
    func requestBrokers(brokers: [BrokerAccount]?, error: Error?) {
        
        self.receivedBrokers = true
        self.didReceiveData()
        
        guard error == nil, let brokers = brokers else {
            self.alertError(message: "Não foi possível conectar.")
            return
        }
        
        self.userBrokers = brokers
        
        if let investment = self.investmentToEdit {
            if let broker = investment.broker {
                self.selectionIndexBroker = self.index(of: broker)
            }
        }

        DispatchQueue.main.async {
            self.brokerCollectionView.reloadData()
        }
    }
    
    func requestInvestmentPapers(investmentPapers: [InvestmentPaper]?, error: Error?) {
        self.receivedPapers = true
        self.didReceiveData()
        
        guard error == nil, let investmentPapers = investmentPapers else {
            self.alertError(message: "Não foi possível conectar.")
            return
        }
        
        self.papers = investmentPapers
        
        if let investment = self.investmentToEdit {
            if let investmentPaper = investment.investmentPaperID {
                self.selectionIndexPaper = self.index(of: investmentPaper)
            }
        }
        
        DispatchQueue.main.async {
            self.tesouroCollectionView.reloadData()
        }
    }
    
    func didReceiveData() {
        if self.receivedPapers && self.receivedBrokers {
            self.hideActivityView(hide: true)
        }
    }
    
    func setupTextFields() {
        self.valueTextField.addTarget(self, action:
            #selector(self.valueTextFieldDidChanged(_:)),
                                      for: .editingChanged)
        
        self.fractionTextField.addTarget(self, action:
            #selector(self.fractionTextFieldDidChanged(_:)),
                                         for: .editingChanged)
    }
    
    @objc func fractionTextFieldDidChanged(_ sender: Any) {
        self.fractionTextField.text = self.fractionInputFormatting(value: self.fractionTextField.text!)
    }
    
    @objc func valueTextFieldDidChanged(_ sender: Any) {
        self.valueTextField.text = self.currencyInputFormatting(value: self.valueTextField.text!)
    }
    
    func setupPickerView() {
        self.deadlinePickerView.datePickerMode = .date
        self.deadlinePickerView.locale = Locale(identifier: "pt_BR")
        self.deadlinePickerView.backgroundColor = UIColor.white
        self.deadlinePickerView.addTarget(self, action: #selector(self.pickerDateDidChange(_:)), for: .valueChanged)
        
        self.dateTextField.inputView = self.deadlinePickerView
        self.dateTextField.inputAccessoryView = self.setupToolbar()
    }
    
    @objc func pickerDateDidChange(_ sender: Any) {
        self.dateTextField.text = self.deadlinePickerView.date.formatted
    }
    
    func setupToolbar() -> UIToolbar {
        let toolbar = UIToolbar(frame:
            CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: CGFloat(self.toolbarHeight)))
        toolbar.barStyle = .default
        
        toolbar.items = [UIBarButtonItem(title: "OK", style:
            .done,
                                         target: self, action: #selector(self.dismissKeyboard))]
        
        return toolbar
    }
    
    @objc func currencyInputFormatting(value: String) -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "R$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        // remove from String: "$", ".", ","
        do {
            let regex = try NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            let newValue = regex.stringByReplacingMatches(in: value, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, value.count), withTemplate: "")
            
            let double = (newValue as NSString).doubleValue
            number = NSNumber(value: (double / 100))
            
            // if first number is 0 or all numbers were deleted
            guard number != 0 as NSNumber else {
                return ""
            }
            
            return formatter.string(from: number)!
        } catch {
            return ""
        }
    }
    
    @objc func fractionInputFormatting(value: String) -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.decimalSeparator = ","
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        
        let divider = 100.0

        // remove from String: "$", ".", ","
        do {
            let regex = try NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            let newValue = regex.stringByReplacingMatches(in: value, options:
                NSRegularExpression.MatchingOptions(rawValue: 0),
                                                          range: NSMakeRange(0, value.count), withTemplate: "")
            
            let double = (newValue as NSString).doubleValue
            number = NSNumber(value: (double / divider))
            
            // if first number is 0 or all numbers were deleted
            guard number != 0 as NSNumber else {
                return ""
            }
            
            return formatter.string(from: number)!
        } catch {
            return ""
        }
    }
    
    func create(investment: Investment) {
        self.hideActivityView(hide: false)
        
        if var investmentToEdit = self.investmentToEdit {
            
            investmentToEdit.broker = investment.broker
            investmentToEdit.date = investment.date
            investmentToEdit.fraction = investment.fraction
            investmentToEdit.initialAmount = investment.initialAmount
            investmentToEdit.investmentPaperID = investment.investmentPaperID
            investmentToEdit.name = investment.name
            
            InvestmentServices().update(investment: investmentToEdit) { (error) in
                self.hideActivityView(hide: true)
                
                guard error == nil else {
                    self.alertError(message: "Não foi possível conectar. Tente novamente mais tarde.")
                    return
                }
                
                self.alertDone(message: "Investimento atualizado com sucesso!", completion: {
                    self.goBackToLastVC()
                })
            }
        } else {
            InvestmentServices().create(investment: investment) { (error) in
                self.hideActivityView(hide: true)
                
                guard error == nil else {
                    self.alertError(message: "Não foi possível conectar. Tente novamente mais tarde.")
                    return
                }
                
                self.alertDone(message: "Investimento criado com sucesso!", completion: {
                    self.goBackToLastVC()
                })
            }
        }
   
    }
    
    @IBAction func finishButtonTapped(_ sender: Any) {
        guard let fractionText = self.fractionTextField.text,
        let valueText = self.valueTextField.text,
            let dateText = self.dateTextField.text else {
                
                self.alertError(message: "Algo deu errado, tente novamente mais tarde.")
                return
        }
        
        guard self.selectionIndexPaper > -1 else {
            self.alertError(message: "Selecione um título.")
            return
        }
        
        guard self.selectionIndexBroker > -1 else {
            self.alertError(message: "Selecione uma corretora.")
            return
        }
        
        if let fraction = Double(fractionText.replacingOccurrences(of:
            "%",
                                                                   with: "").replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with:
                                                                    ".")) {
            if let value = Double(valueText.replacingOccurrences(of: "R$", with: "").replacingOccurrences(of: ".",
                                                                                                          with: "").replacingOccurrences(of: ",", with: ".")) {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yy"
                if let date = dateFormatter.date(from: dateText) {
                    var investment = Investment()
                    
                    investment.broker = self.userBrokers[self.selectionIndexBroker]
                    investment.date = date
                    investment.fraction = fraction
                    investment.initialAmount = value
                    investment.investmentPaperID = self.papers[self.selectionIndexPaper].identifier
                    investment.name = self.papers[self.selectionIndexPaper].name
                    investment.goal = self.goal.identification
                    
                    self.create(investment: investment)
                    
                } else {
                    self.alertError(message: "Escreva  campo de Data")
                }
            } else {
                self.alertError(message: "Escreva o campo de Valor.")
            }
        } else {
            self.alertError(message: "Escreva o campo de Fração.")
        }
    }
    
    func hideActivityView(hide: Bool) {
        DispatchQueue.main.async {
            self.activityView.isHidden = hide
            if hide {
                self.view.sendSubview(toBack: self.activityView)
            } else {
                self.view.bringSubview(toFront: self.activityView)
            }
        }
    }
    
    func goBackToLastVC() {
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    /// Alert an error to user
    ///
    /// - Parameter message: The message to be presented
    func alertError(message: String) {
        
        let alert = UIAlertController(title: "Ops!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /// Alert a done action to user
    ///
    /// - Parameter message: The message to be presented
    func alertDone(message: String, completion: (() -> Void)?) {
        
        let alert = UIAlertController(title: "Sucesso!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (_) in
            completion?()
        })
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setupKeyboard() {
        //Notificate the VC that keyboard will appear
        NotificationCenter.default.addObserver(self, selector:
            #selector(AddInvestmentViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //Notificate the VC that keyboard will disappear
        NotificationCenter.default.addObserver(self, selector:
            #selector(AddInvestmentViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Let the keyboard dismiss when touch on the screen
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(AddInvestmentViewController.dismissKeyboard))
        self.tapView.addGestureRecognizer(tapGesture)
    }
    
    /// Dismiss the keyboard on the VC
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue) {
            let keyboardSize = keyboard.cgRectValue
            
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y:
                                        keyboardSize.height + CGFloat(self.toolbarHeight)), animated: false)
                    }, completion: nil)
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let _ = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue, delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                                    
                    }, completion: nil)
                    
                }
            }
        }
    }
    
    @IBAction func deleteInvestmentTapped(_ sender: Any) {
        
        if let investment = self.investmentToEdit {
            let alert = UIAlertController(title: "Confirma", message: "Deseja retirar esse investimento de sua lista?",
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Retirar", style: .destructive, handler: { (_) in
                
                self.hideActivityView(hide: false)
                
                InvestmentServices().delete(investment: investment, completion: { (error) in
                    self.hideActivityView(hide: true)
                    
                    guard error == nil else {
                        self.alertError(message: "Não foi possível fazer conexão.")
                        return
                    }
                    
                    self.alertDone(message: "Investimento retirado com sucesso!", completion: {
                        self.goBackToLastVC()
                    })
                })
            }))
            
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
}

extension AddInvestmentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.brokerCollectionView:
            return self.userBrokers.count
        case self.tesouroCollectionView:
            return self.papers.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.tesouroCollectionView:
            if let paperCell = collectionView.dequeueReusableCell(withReuseIdentifier:
                "InvestmentPaperCollectionViewCell", for: indexPath) as? InvestmentPaperCollectionViewCell {
                paperCell.prepare(with: self.papers[indexPath.row])
                
                if self.selectionIndexPaper == indexPath.item {
                    paperCell.setSelected()
                } else {
                    paperCell.setDeselected()
                }
                
                return paperCell
            }
        case self.brokerCollectionView:
            if let brokerCell = collectionView.dequeueReusableCell(withReuseIdentifier:
                "SystemBrokerCVCell", for: indexPath) as? SystemBrokerCVCell {
                brokerCell.prepare(with: self.userBrokers[indexPath.row])
                
                if self.selectionIndexBroker == indexPath.item {
                    brokerCell.setSelected()
                } else {
                    brokerCell.setDeselected()
                }
                
                return brokerCell
            }
        default:
            return UICollectionViewCell()
        }

        return UICollectionViewCell()
    }
}

extension AddInvestmentViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        switch collectionView {
        case self.tesouroCollectionView:
            self.selectionIndexPaper = indexPath.item
        case self.brokerCollectionView:
            self.selectionIndexBroker = indexPath.item
        default:
            return
        }
        
        collectionView.reloadData()
    }
}
