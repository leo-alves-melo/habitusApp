//
//  MyBrokersTableViewCell.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 23/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class MyBrokersTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet public weak var collectionBrokers: UICollectionView!
    
    @IBOutlet weak var noBrokersView: UIView!
    
    weak var delegate: BrokerCVDelegate?
    var brokers: [BrokerAccount] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionBrokers.dataSource = self
        self.collectionBrokers.delegate = self
        
        //Add space before
        self.collectionBrokers.contentInset = UIEdgeInsetsMake(0, CGFloat(integerLiteral: 20),
                                                                  0, CGFloat(integerLiteral: 20))
    }
    
    func prepare(with brokers: [BrokerAccount]?, delegate: BrokerCVDelegate) {
        self.brokers = brokers ?? []
        
        if self.brokers.count > 0 {
            self.noBrokersView.isHidden = true
            collectionBrokers.isHidden = false
            self.collectionBrokers.reloadData()
        } else {
            self.noBrokersView.isHidden = false
            collectionBrokers.isHidden = true
        }
        
        self.delegate = delegate
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brokers.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell =  UICollectionViewCell()
        
        if let cellBroker = collectionView.dequeueReusableCell(withReuseIdentifier: "BrokerCollectionViewCell",
                                                            for: indexPath) as? BrokerCollectionViewCell {
            cellBroker.prepare(with: brokers[indexPath.row])
            cell = cellBroker
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.brokerAccountTapped(brokers[indexPath.item])
    }
}

extension MyBrokersTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionBrokers.frame.height * 2,
                      height: self.collectionBrokers.frame.height)
    }
}
