//
//  EmergencyReserveTableViewCell.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 26/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class EmergencyReserveTableViewCell: UITableViewCell {
    
    @IBOutlet weak var currentValueLabel: UILabel!
    @IBOutlet weak var wantageLabel: UILabel!
    @IBOutlet weak var differencePoupancaLabel: UILabel!
    
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressIndicatorOutlet: NSLayoutConstraint!
    
    func prepare(with reserve: Goal) {
        
        let currente = reserve.currentValue
        self.currentValueLabel.text = currente?.formattedReal() ?? "R$ 0"
        
        var wantage = 0.0
        if let finalValue = reserve.finalValue, let currente = currente {
            wantage = finalValue - currente
        }
        
        if wantage < 0.0 {
            self.wantageLabel.text = "Parabéns!"
            self.differencePoupancaLabel.text = "Você alcançou sua meta!"
        } else {
            self.wantageLabel.text = "Faltam " + wantage.formattedReal()
            self.differencePoupancaLabel.text = "Vamos lá! Você consegue!"
        }

        if let value = reserve.finalValue, value > 0.0 {
            
            let progress = CGFloat(currente ?? 1)/CGFloat(value)
            self.progressIndicatorOutlet.constant = progress * self.progressBarView.frame.width
            
        } else {
            self.progressIndicatorOutlet.constant =  self.progressBarView.frame.width
            
        }
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
