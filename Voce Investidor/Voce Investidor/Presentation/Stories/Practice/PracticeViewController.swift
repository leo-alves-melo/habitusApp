//
//  PracticeViewController.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 23/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

enum SectionPractice: String {
    case myBrokers = "Minhas Corretoras"
    case emergencyReserve = "Reserva de Emergência"
}

protocol BrokerCVDelegate: class {
    func brokerAccountTapped(_ brokerAccount: BrokerAccount)
}

class PracticeViewController: CustomizedNavBarViewController {
    
    var tableSectionsPractice: [SectionPractice] = [.myBrokers, .emergencyReserve]
    
    @IBOutlet weak var loginView: UIView!
    var brokers: [BrokerAccount] = []
    var emergencyReserve: Goal!
    
    var goalServices: GoalServices!
    var brokerAccountServices: BrokerAccountServices!
    var investmentServices: InvestmentServices!
    
    var selectedInvestment: Investment!

    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var didReceiveBrokers = false
    var didReceiveInvestments = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.investmentServices = InvestmentServices()
        self.goalServices = GoalServices()
        self.brokerAccountServices = BrokerAccountServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard UserDAO.isiCloudAllowed(), User.instance.hashPassword != nil else {
            return
        }
        self.callingServerData()
        self.tableView.reloadData()
    }
    
    func callingServerData() {
        
        self.didReceiveBrokers = false
        self.didReceiveInvestments = false
        self.hideActivityView(hide: false)
        
        self.goalServices.goals(from: User.instance, completion: self.instanceEmergencyReserve(goals:error:))
        
        self.brokerAccountServices.brokerAccounts(from: User.instance, completion: self.instanceBrokers(brokers:error:))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDAO.isiCloudAllowed() && User.instance.hashPassword != nil {
            self.loginView.isHidden = true
            self.showAddButton()
        } else {
            self.loginView.isHidden = false
            self.view.bringSubview(toFront: self.loginView)
            self.hideAddButton()
        }
    }
    
    func updateTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func alertErrorServerData() {
        
        let alert = UIAlertController(title: "Ops!", message: "Ocorreu um erro ao acessar os dados. Verifique sua conexão com a internet.", preferredStyle: .alert)
        let action = UIAlertAction(title: "Tentar novamentte", style: .default) { (action) in
            self.callingServerData()
        }
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func didReceive() {
        if self.didReceiveBrokers && self.didReceiveInvestments {
            self.hideActivityView(hide: true)
        }
        
        self.updateTableView()
    }
    
    func instanceBrokers(brokers: [BrokerAccount]?, error: Error?) {

        guard error == nil else {
            self.alertErrorServerData()
            return
        }
        
        if brokers?.count == 0 {
            self.brokers = []
            self.didReceiveBrokers = true
            self.didReceive()
            return
        }

        if let brokers = brokers, brokers.count >= 1 {
            self.brokers = brokers
            self.brokers.sort { (first, second) -> Bool in
                if let dateA = first.openDate, let dateB = second.openDate {
                    return dateA.compare(dateB) == .orderedAscending
                }
                return true
            }
            
            self.didReceiveBrokers = true
            self.didReceive()
        }
    }
    
    func hideActivityView(hide: Bool) {
        DispatchQueue.main.async {
            self.activityView.isHidden = hide
            if hide {
                self.view.sendSubview(toBack: self.activityView)
            } else {
                self.view.bringSubview(toFront: self.activityView)
            }
        }
    }
    
    func instanceInvestimentsInReserve(investiments: [Investment]?, error: Error?) {
        guard error == nil, let investiments = investiments else {
            self.alertErrorServerData()
            self.hideActivityView(hide: true)
            return
        }
        
        let orderedInvestments = investiments.sorted { (investment1, investment2) -> Bool in
            if let date1 = investment1.date, let date2 = investment2.date {
                return date1 > date2
            }
            
            return false
        }
        
        self.emergencyReserve.investments = orderedInvestments
        
        if orderedInvestments.count == 0 {
            self.didReceiveInvestments = true
            self.didReceive()
        }

        for (index, investiment) in orderedInvestments.enumerated() {
            InvestmentPaperCalculator().calculate(investment: investiment) { (value, error) in
                guard error == nil, let value = value else {
                    self.alertErrorServerData()
                    self.hideActivityView(hide: true)
                    return
                }

                self.emergencyReserve.investments![index].currentAmount = value

                self.didReceiveInvestments = true
                self.didReceive()
            }
        }
        
    }
    
    func instanceEmergencyReserve(goals: [Goal]?, error: Error?) {
        guard error == nil else {
            self.alertErrorServerData()
            self.hideActivityView(hide: true)
            return
        }
        
        if let goals = goals, goals.count >= 1 {
            for goal in goals where goal.type == GoalType.emergency {
                self.emergencyReserve = goal
                
                //call investiments
                self.investmentServices.investments(from: goal, completion: instanceInvestimentsInReserve)
                
                break
            }
        } else {
            let emergencyGoal = self.createEmergencyGoal()
            GoalServices().create(goal: emergencyGoal) { (error) in
                guard error == nil else {
                    self.alertErrorServerData()
                    self.hideActivityView(hide: true)
                    return
                }
                User.instance.goals = [emergencyGoal]
                self.emergencyReserve = emergencyGoal
                self.updateTableView()
                self.hideActivityView(hide: true)
            }
        }
    }
    
    func createEmergencyGoal() -> Goal {
        var goal = Goal()
        goal.name = "Reserva de Emergência"
        goal.type = GoalType.emergency
        goal.finalValue = Double(User.instance.receipts!*6)
        goal.deadLine = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        
        return goal
    }
        
    func hideAddButton() {
        self.addButton.isEnabled = false
        self.addButton.tintColor = UIColor.clear
    }
    
    func showAddButton() {
        self.addButton.isEnabled = true
        self.addButton.tintColor = UIColor.white
    }
    
    @IBAction func addPressed(_ sender: Any) {

        // create the alert
        let alert = UIAlertController(title: "Adicionar", message: "O que deseja adicionar?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Corretora", style: UIAlertAction.Style.default,
                                      handler:  { action in
                                        self.performSegue(withIdentifier: "segueAddBroker", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Valor na Reserva", style: UIAlertAction.Style.default,
                                      handler:  { action in
                                        
                                        if self.brokers.count > 0 {
                                            self.performSegue(withIdentifier: "segueAddInvestiment", sender: self)
                                        } else {
                                            let alert = UIAlertController(title: "Ops",
                                                                          message: "Adicione uma corretora antes de adicionar um investimento!",
                                                                          preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                                          handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                        
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel,
                                      handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation
    @IBAction func unwindToPractice(segue: UIStoryboardSegue) {}

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueEditReserve"{
            if let next = segue.destination as? EditReserveValuesViewController {
                next.goal = self.emergencyReserve
            }
        }
        
        if segue.identifier == "segueEditBroker", let broker = sender as? BrokerAccount {
            if let next = segue.destination as? AddEditBrokerTableViewController {
                next.brokerAccount = broker
            }
        }
        
        if let destination = segue.destination as? AddInvestmentViewController {
            destination.goal = self.emergencyReserve
            
            if segue.identifier == "editInvestment" {
                destination.investmentToEdit = self.selectedInvestment
            }
        }
    }
}

extension PracticeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSectionsPractice.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableSectionsPractice[section].rawValue
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var result = 0
        switch section {
        case tableSectionsPractice.firstIndex(of: .myBrokers):
            result = 1
        case tableSectionsPractice.firstIndex(of: .emergencyReserve):
            result = 1 //emergencyReserve card
            
            if let emergencyReserve = emergencyReserve, let investments = emergencyReserve.investments, investments.count > 0 {
                result += investments.count
            } else {
                result += 1 // cellNoApplication
            }
            
        default:
            result = 0
        }
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell =  UITableViewCell()
        
        switch indexPath.section {
        case tableSectionsPractice.firstIndex(of: .myBrokers):
            
            if let brokerCell = tableView.dequeueReusableCell(withIdentifier: "MyBrokersTableViewCell",
                                                        for: indexPath) as? MyBrokersTableViewCell {
                brokerCell.prepare(with: self.brokers, delegate: self)
                
                cell = brokerCell
            }
            
        case tableSectionsPractice.firstIndex(of: .emergencyReserve):

            if indexPath.row == 0 {
                
                if let emergencyReserve = emergencyReserve, let reserveCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyReserveTableViewCell",
                                                            for: indexPath) as? EmergencyReserveTableViewCell {
                    reserveCell.prepare(with: emergencyReserve)
                    
                    cell = reserveCell
                }
                
            } else {
                
                if let reserve = emergencyReserve, let investiments = reserve.investments, investiments.count > 0 {
                    
                    if let investimentCell = tableView.dequeueReusableCell(withIdentifier: "InvestimentTableViewCell",
                                                            for: indexPath) as? InvestimentTableViewCell {
                        
                        investimentCell.titleInvestimentLabel.text = "hahahahaha"
                    
                        let indexInvestiment = indexPath.row - 1 //because has the Emergency card
                    
                        investimentCell.prepare(with: investiments[indexInvestiment])
                   
                        cell = investimentCell
                    }
                } else {
                    //cellNoApplication
                    let cellNoApplication = tableView.dequeueReusableCell(withIdentifier: "cellNoApplication", for: indexPath)   
                    cell = cellNoApplication
                    
                }
                
            }
            
        default:
            cell = UITableViewCell()
        }
        
        return cell
    }
}

extension PracticeViewController: UITableViewDelegate {
    
    /// Setup the view for the header in each section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //Create a view for return
        let widthView = self.tableView.frame.width
        let heightView = CGFloat(48)
        let sizeView = CGSize(width: widthView, height: heightView)
        let originView = CGPoint(x: 0, y: 0)
        let rectView = CGRect(origin: originView, size: sizeView)
        let viewReturn = UIView(frame: rectView)
        viewReturn.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9490196078, alpha: 1)
        
        //Create a label for title
        let originLabel = CGPoint(x: 20, y: 0)
        let rectLabel = CGRect(origin: originLabel, size: sizeView)
        
        let titleViewLabel = UILabel(frame: rectLabel)
        titleViewLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
        titleViewLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)

        titleViewLabel.text = tableSectionsPractice[section].rawValue

        viewReturn.addSubview(titleViewLabel)
        
        return viewReturn
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 1 && indexPath.row > 0 {
            self.selectedInvestment = self.emergencyReserve.investments?[indexPath.row - 1]
            self.performSegue(withIdentifier: "editInvestment", sender: nil)
        }
    }
}

extension PracticeViewController: BrokerCVDelegate {
    func brokerAccountTapped(_ brokerAccount: BrokerAccount) {
        self.performSegue(withIdentifier: "segueEditBroker", sender: brokerAccount)
    }
}
