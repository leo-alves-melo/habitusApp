//
//  BrokerCollectionViewCell.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 22/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class BrokerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func prepare(with broker: BrokerAccount) {
        if let imagePath = broker.imagePath, let image = UIImage(named: imagePath) {
            self.imageView.image = image
        }
        self.nameLabel.text = broker.name ?? "Sem nome"
        self.dateLabel.text = broker.openDate?.formatted ?? "Sem data"
    }
}
