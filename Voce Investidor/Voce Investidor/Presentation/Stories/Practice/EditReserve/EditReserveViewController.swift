//
//  EditReserveViewController.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 05/12/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class EditReserveViewController: UIViewController {
    
    var emergencyReserve: Goal!
    var currentValue: Double!
    
    var currentValueLabel: UILabel!
    var wantageLabel: UILabel!

    var progressBarView: UIView!
    var progressIndicatorOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var goalTableView: UITableView!
    var investments: [Investment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let emergencyReserve = self.emergencyReserve else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        self.navigationItem.title = self.emergencyReserve.name
  
        self.currentValue = emergencyReserve.currentValue
        self.goalTableView.tableFooterView = UIView()
        
        let investmentServices = InvestmentServices()
        investmentServices.investments(from: self.emergencyReserve, completion: self.receiveInvestments(investments:error:))
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EditReserveValuesViewController {
            destination.goal = self.emergencyReserve
        }
    }
    
    /// Alert an error to user
    ///
    /// - Parameter message: The message to be presented
    func alertError(message: String) {
        
        let alert = UIAlertController(title: "Ops!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /// Function to receive investments in asyncronous way
    ///
    /// - Parameters:
    ///   - investments: investments receiveds
    ///   - error: possible error
    func receiveInvestments(investments: [Investment]?, error: Error?) {
        guard error == nil else {
            self.alertError(message: "Erro ao conectar. Tente novamente mais tarde.")
            return
        }
        
        if let investments = investments {
            self.investments = investments
            self.updateTableView()
            
        }
    }
    
    func updateTableView() {
        DispatchQueue.main.async {
            self.goalTableView.reloadData()
        }
    }
    
    /// Updates the progressBar of the Goal
    func updateProgress() {
        
        var wantage = 0.0
        if let finalValue = emergencyReserve.finalValue {
            wantage = finalValue - self.currentValue
        }
        
        self.wantageLabel.text = "Faltam " + wantage.formattedReal()
        
        if let value = emergencyReserve.finalValue, value > 0.0 {
            
            let progress = CGFloat(self.currentValue)/CGFloat(value)
            self.progressIndicatorOutlet.constant = progress * self.progressBarView.frame.width
            
        } else {
            self.progressIndicatorOutlet.constant =  self.progressBarView.frame.width
            
        }
    }

}

extension EditReserveViewController: UITableViewDelegate {
    
}

extension EditReserveViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return investments.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let emergencyCell = tableView.dequeueReusableCell(withIdentifier:
                "Emergency",
                                                                 for: indexPath) as? EmergencyReserveTableViewCell {
                emergencyCell.prepare(with: self.emergencyReserve)
                return emergencyCell
            }
        } else if let investmentCell = tableView.dequeueReusableCell(withIdentifier:
            "InvestimentTableViewCell", for:
            indexPath) as? InvestimentTableViewCell {
            
            investmentCell.prepare(with: self.investments[indexPath.row])
            
            return investmentCell
        }
        
        return UITableViewCell()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

}
