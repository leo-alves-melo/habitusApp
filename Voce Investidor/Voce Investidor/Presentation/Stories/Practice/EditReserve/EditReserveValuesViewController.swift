//
//  EditReserveValuesViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 06/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class EditReserveValuesViewController: UIViewController {

    var goal: Goal!
    
    @IBOutlet weak var currentValueLabel: UILabel!
    
    @IBOutlet weak var progressOutlet: NSLayoutConstraint!
    @IBOutlet weak var leftValueLabel: UILabel!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var finalValueTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var deadlineTextField: UITextField!
    
    var deadlinePickerView = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let goal = self.goal else {
            return
        }
        
        self.updateView()
        self.finalValueTextField.text = String(format:
            "R$%.02f", goal.finalValue ?? 0.0).replacingOccurrences(of: ".", with: ",")
        self.setupKeyboard()
        self.setupDueDate()
        
        self.deadlineTextField.inputView = self.deadlinePickerView
        
        self.hideActivityView(hide: true)
        
        self.finalValueTextField.addTarget(self, action:
            #selector(self.valueTextFieldDidChanged(_:)),
                                           for: .editingChanged)
        
        self.finalValueTextField.inputAccessoryView = self.setupToolbar()
        self.deadlineTextField.inputAccessoryView = self.setupToolbar()
        
    }
    
    func setupToolbar() -> UIToolbar {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        
        toolbar.items = [UIBarButtonItem(title: "OK", style:
            .done,
                                         target: self, action: #selector(self.dismissKeyboard))]
        
        return toolbar
    }
    
    func setupDueDate() {
        
        if let deadline = self.goal.deadLine {
            self.deadlinePickerView.date = deadline
            self.deadlineTextField.text = deadline.formatted
        }
        
        self.deadlinePickerView.datePickerMode = .date
        self.deadlinePickerView.locale = Locale.current
        self.deadlinePickerView.backgroundColor = UIColor.white
        self.deadlinePickerView.addTarget(self, action: #selector(self.pickerDateDidChange(_:)), for: .valueChanged)
        
    }
    
    @objc func pickerDateDidChange(_ sender: Any) {
        self.deadlineTextField.text = self.deadlinePickerView.date.formatted
    }
    
    func setupKeyboard() {
        //Notificate the VC that keyboard will appear
        NotificationCenter.default.addObserver(self, selector:
            #selector(EditReserveValuesViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //Notificate the VC that keyboard will disappear
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(EditReserveValuesViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        //Let the keyboard dismiss when touch on the screen
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditReserveValuesViewController.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tapGesture)
    }
    
    /// Dismiss the keyboard on the VC
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

        if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue) {
            let keyboardSize = keyboard.cgRectValue
  
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y:
                                        keyboardSize.height/2), animated: false)
                    }, completion: nil)
                    
                }
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {

        if let _ = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue, delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                                    
                    }, completion: nil)
                    
                }
            }
            
        }
    }
    
    /// Alert an error to user
    ///
    /// - Parameter message: The message to be presented
    func alertError(message: String) {
        
        let alert = UIAlertController(title: "Ops!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /// Alert a done action to user
    ///
    /// - Parameter message: The message to be presented
    func alertDone(message: String, completion: (() -> Void)?) {
        
        let alert = UIAlertController(title: "Sucesso!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (_) in
            completion?()
        })
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    func updateView() {
        
        let current = self.goal.currentValue
        self.currentValueLabel.text = String(format:
            "R$%.02f", current ?? 0).replacingOccurrences(of: ".", with: ",")
        
        var wantage = 0.0
        if let finalValue = self.goal.finalValue, let currente = current {
            wantage = finalValue - currente
        }
        self.leftValueLabel.text = "Faltam " + String(format:
            "R$%.02f", wantage).replacingOccurrences(of: ".", with: ",")
        
        if let value = self.goal.finalValue, value > 0.0 {
            
            let progress = CGFloat(current ?? 1)/CGFloat(value)
            self.progressOutlet.constant = progress * self.progressBarView.frame.width
            
        } else {
            self.progressOutlet.constant =  self.progressBarView.frame.width
        }
    }
    
    func hideActivityView(hide: Bool) {
        DispatchQueue.main.async {
            self.activityView.isHidden = hide
            if hide {
                self.view.sendSubview(toBack: self.activityView)
            } else {
                self.view.bringSubview(toFront: self.activityView)
            }
        }
    }
    
    func goBackToLastVC() {
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func updateGoal(newFinalValue: Double, newDueDate: Date) {
        self.hideActivityView(hide: false)
        
        let goalServices = GoalServices()
        self.goal.finalValue = newFinalValue
        self.goal.deadLine = newDueDate
        goalServices.update(goal: self.goal) { [weak self] (error) in
            
            self?.hideActivityView(hide: true)
            
            guard error == nil else {
                self?.alertError(message: "Não foi possível conectar, tente novamente mais tarde.")
                return
            }
            
            self?.alertDone(message: "Meta atualizada com sucesso!", completion: self?.goBackToLastVC)
        }
    }

    @IBAction func updateGoalPressed(_ sender: Any) {
        self.dismissKeyboard()
        if let finalValueString = self.finalValueTextField.text {
            
            let newString = finalValueString.replacingOccurrences(of:
                "R$", with: "").replacingOccurrences(of:
                    ".", with: "").replacingOccurrences(of:
                        ",", with: ".")
            
            if let finalValue = Double(newString) {
                self.updateGoal(newFinalValue: finalValue, newDueDate: self.deadlinePickerView.date)
            } else {
                self.alertError(message: "Escreva um número válido para o novo valor final!")
            }

        } else {
            self.alertError(message: "Escreva um número válido para o novo valor final!")
        }
    }
    
    @objc func currencyInputFormatting(value: String) -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "R$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        // remove from String: "$", ".", ","
        do {
            let regex = try NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            let newValue = regex.stringByReplacingMatches(in: value, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, value.count), withTemplate: "")
            
            let double = (newValue as NSString).doubleValue
            number = NSNumber(value: (double / 100))
            
            // if first number is 0 or all numbers were deleted
            guard number != 0 as NSNumber else {
                return ""
            }
            
            return formatter.string(from: number)!
        } catch {
            return ""
        }
        
    }
    
    @objc func valueTextFieldDidChanged(_ sender: Any) {
        self.finalValueTextField.text = self.currencyInputFormatting(value: self.finalValueTextField.text!)
    }
    
}

extension EditReserveValuesViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.finalValueTextField {
            self.finalValueTextField.text = "R$0,00"
        }
        return true
    }
}
