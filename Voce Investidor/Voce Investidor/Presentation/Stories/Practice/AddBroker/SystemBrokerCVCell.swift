//
//  BrokerCollectionViewCell.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 22/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class SystemBrokerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    func prepare(with broker: Broker) {
        if let imagePath = broker.imageURL, let image = UIImage(named: imagePath) {
            self.imageView.image = image
            self.selectedImage.isHidden = true
        }
    }
    
    func prepare(with broker: BrokerAccount) {
        if let imagePath = broker.imagePath, let image = UIImage(named: imagePath) {
            self.imageView.image = image
            self.selectedImage.isHidden = true
        }
    }
    
    func setSelected() {
        self.selectedImage.isHidden = false
        self.imageView.alpha = 1.0
    }
    
    func setDeselected() {
        self.selectedImage.isHidden = true
        self.imageView.alpha = 0.7
    }
}
