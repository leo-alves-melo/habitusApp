//
//  AddEditBrokerTableViewController.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 04/12/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

enum BrokerSectionPractice: String {
    case selectBroker = "Selecione uma corretora"
    case openingDate = "Data de abertura"
    case deleteBroker = ""
    case myBroker = "Minha corretora"
}

class AddEditBrokerTableViewController: UITableViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var corretoraTextField: UITextField!
    @IBOutlet weak var dataTextField: UITextField!
    @IBOutlet weak var corretoraCollectionView: UICollectionView!
    @IBOutlet weak var otherCell: UITableViewCell!
    @IBOutlet weak var deleteBrokerCell: UITableViewCell!
    
    // MARK: - Properties
    var brokerAccount: BrokerAccount?
    var editMode: Bool = false
    
    var otherSelected: Bool = false
    var brokers: [Broker] = []
    var brokerAccountEnum: [BrokerEnum] = []
    var selectionIndex: Int = -1
    
    var tableSections: [BrokerSectionPractice] = [.selectBroker, .openingDate, .deleteBroker]
    var backButton: UIBarButtonItem?
    
    // MARK: - Super Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBrokers()
        setupViews()
        
        self.corretoraCollectionView.dataSource = self
        self.corretoraCollectionView.delegate = self
        
        //Add space before
        self.corretoraCollectionView.contentInset = UIEdgeInsetsMake(0, CGFloat(integerLiteral: 20),
                                                                     0, CGFloat(integerLiteral: 20))
        corretoraCollectionView.reloadData()

        //Let the keyboard dismiss when touch on the screen
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        self.tableView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - View Setup
    func setupBrokers() {
        let easynvest = Broker(name: "Easynvest", imagePath: "easynvest", enumValue: .easynvest)
        let rico = Broker(name: "Rico", imagePath: "rico", enumValue: .rico)
        let xpInvest = Broker(name: "XP Investimentos", imagePath: "xp", enumValue: .xpinvestimentos)
        let otherBroker = Broker(name: "Outra Corretora", imagePath: "otherBroker", enumValue: .other)
        
        self.brokers = [easynvest, rico, xpInvest, otherBroker]
    }
    
    func setupViews() {
        otherCell.isHidden = true
        
        dataTextField.placeholder = Date().formatted
        
        // If brokerAccount exists, then user can choose to edit
        if let brokerAccount = brokerAccount {
            dataTextField.text = brokerAccount.openDate?.formatted ?? Date().formatted
            let editButton = UIBarButtonItem(title: "Editar", style: .plain, target: self, action: #selector(editTapped))
            self.backButton = self.navigationItem.leftBarButtonItem
            self.navigationItem.setRightBarButton(editButton, animated: true)
            
            if brokerAccount.broker == .other {
                self.corretoraTextField.text = brokerAccount.name
            }
            
            self.otherCell.isHidden = brokerAccount.broker != .other
            
            self.enableEditing(false)
            
        } else {
            // Otherwise the user is registering a new broker account
            let button = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(finishRegistering))
            self.navigationItem.rightBarButtonItem = button
            self.enableEditing()
        }
    }
    
    func setupToolbar() -> UIToolbar {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        
        toolbar.items = [UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(self.dismissKeyboard))]
        
        return toolbar
    }
    
    // MARK: - IBActions
    @IBAction func textFieldSelected(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.white
        datePicker.locale = Locale(identifier: "pt_BR")
        datePicker.addTarget(self, action: #selector(handleDatePicker(_:)), for: .valueChanged)
        datePicker.addTarget(self, action: #selector(handleDatePicker(_:)), for: .editingDidBegin)
        dataTextField.inputView = datePicker
        dataTextField.inputAccessoryView = setupToolbar()
    }
    
    // Delete current broker account
    @IBAction func deleteBrokerTapped(_ sender: Any) {
        guard let brokerAccount = brokerAccount, editMode else { return }
        
        let alert = UIAlertController(title: "Atenção", message: "Deseja realmente remover o registro desta corretora?", preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        let deleteButton = UIAlertAction(title: "Deletar", style: .destructive)        { [weak self] (action) in
            self?.deleteBrokerAccount()
        }
        alert.addAction(cancelButton)
        alert.addAction(deleteButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - View Handling
    @objc func handleDatePicker(_ sender: UIDatePicker) {
        dataTextField.text = sender.date.formatted
    }
    
    func enableEditing(_ enabled: Bool = true) {
        self.editMode = enabled
        self.corretoraTextField.isEnabled = enabled
        self.dataTextField.isEnabled = enabled
        
        deleteBrokerCell.isHidden = (!enabled) || brokerAccount == nil
        
        if enabled {
            setupBrokers()
            if let brokerAccount = brokerAccount {
                for (index, broker) in brokers.enumerated() {
                    if broker.enumValue == brokerAccount.broker {
                        selectionIndex = index
                    }
                }
                self.tableSections = [.selectBroker, .openingDate, .deleteBroker]
                corretoraTextField.isEnabled = true
                dataTextField.isEnabled = true
                
                corretoraCollectionView.reloadData()
                
            }
        } else {
            if let brokerAccount = brokerAccount {
                for (index, broker) in brokers.enumerated() {
                    if broker.enumValue == brokerAccount.broker {
                        brokers = [broker]
                        corretoraCollectionView.reloadData()
                        selectionIndex = 0
                        break
                    }
                }
                self.tableSections = [.myBroker, .openingDate, .deleteBroker]
            }
        }
    }
    
    /// Dismiss the keyboard on the VC
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        print("dismissed")
    }
    
    @objc func editTapped() {
        self.navigationItem.setLeftBarButton(UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelTapped)), animated: true)
        self.navigationItem.setRightBarButton(UIBarButtonItem(title: "Salvar", style: .done, target: self, action: #selector(saveTapped)), animated: true)
        
        self.enableEditing()
    }
    
    @objc func cancelTapped() {
        self.navigationItem.setLeftBarButton(backButton, animated: true)
        self.navigationItem.setRightBarButton(nil, animated: true)
        self.setupViews()
    }
    
    @objc func saveTapped() {
        self.updateBrokerAccount()
    }
    
    func showErrorAlert(_ message: String) {
        let alert = UIAlertController(title: "Erro", message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Broker Services
    func validateInput() -> Bool {
        guard selectionIndex > -1 else {
            showErrorAlert("Nenhuma corretora selecionada!")
            return false
        }
        
        guard let selectionEnumValue = brokers[selectionIndex].enumValue else {
            showErrorAlert("Corretora inválida.")
            return false
        }
        
        self.brokerAccount = BrokerAccount(brokerAccount: selectionEnumValue)
        
        // If user selected a custom broker, check for a name
        if selectionEnumValue == .other {
            guard let brokerName = corretoraTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), brokerName != "" else { showErrorAlert("Corretora inválida.")
                return false
            }
            brokerAccount?.name = brokerName
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        guard let openDate = dataTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            openDate != "", let date = dateFormatter.date(from: openDate) else {
                showErrorAlert("Data de abertura inválida.")
                return false
        }
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let finalDate = calendar.date(from: components)
        brokerAccount?.openDate = finalDate
        
        return true
    }
    
    /// Alert a done action to user
    ///
    /// - Parameter message: The message to be presented
    func alertDone(message: String, completion: (() -> Void)?) {
        
        let alert = UIAlertController(title: "Sucesso!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (_) in
            completion?()
        })
        
        alert.addAction(action)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Save the broker account
    @objc func finishRegistering() {
        
        if validateInput(), let brokerAccount = brokerAccount {
            let service = BrokerAccountServices()
            service.create(brokerAccount: brokerAccount) { [weak self] (error) in
                if let _ = error {
                    DispatchQueue.main.async {
                        self?.showErrorAlert("Ops! Tivemos um erro em salvar os dados. Por favor, entre em contato com os desenvolvedores.")
                    }
                } else {
                    
                    self?.alertDone(message: "Corretora adicionada com sucesso!", completion: self?.unwindToPractice)

                }
            }
        }
    }
    
    func updateBrokerAccount() {
        if validateInput(), let brokerAccount = brokerAccount {
            let service = BrokerAccountServices()
            service.update(brokerAccount: brokerAccount) { [weak self] (error) in
                if let _ = error {
                    DispatchQueue.main.async {
                        self?.showErrorAlert("Ops! Tivemos um erro em atualizar os dados. Por favor, entre em contato com os desenvolvedores.")
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.unwindToPractice()
                    }
                }
            }
        }
    }
    
    // Delete broker account
    func deleteBrokerAccount() {
        guard let brokerAccount = brokerAccount else { return }
        
        let service = BrokerAccountServices() 
        service.delete(brokerAccount: brokerAccount) { [weak self] (error) in
            if let _ = error {
                DispatchQueue.main.async {
                    self?.showErrorAlert("Ops! Algo deu errado. Por favor, tente novamente mais tarde.")
                }
            } else {
                DispatchQueue.main.async {
                    self?.unwindToPractice()
                }
            }
        }
    }
    
    // MARK: - Navigation
    func unwindToPractice() {
        self.performSegue(withIdentifier: "unwindSegueToPractice", sender: nil)
    }

    // MARK: - Table View
    /// Setup the view for the header in each section
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //Create a view for return
        let widthView = self.tableView.frame.width
        let heightView = CGFloat(48)
        let sizeView = CGSize(width: widthView, height: heightView)
        let originView = CGPoint(x: 0, y: 0)
        let rectView = CGRect(origin: originView, size: sizeView)
        
        let viewReturn = UIView(frame: rectView)
        viewReturn.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9490196078, alpha: 1)
        
        //Create a label for title
        let originLabel = CGPoint(x: 20, y: 0)
        let rectLabel = CGRect(origin: originLabel, size: sizeView)
        
        let titleViewLabel = UILabel(frame: rectLabel)
        titleViewLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
        titleViewLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        
        titleViewLabel.text = tableSections[section].rawValue
        
        viewReturn.addSubview(titleViewLabel)
        
        return viewReturn
    }
}

extension AddEditBrokerTableViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brokers.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell =  UICollectionViewCell()
        
        if let cellBroker = collectionView.dequeueReusableCell(withReuseIdentifier: "SystemBrokerCVCell",
                                                               for: indexPath) as? SystemBrokerCVCell {
            cellBroker.prepare(with: brokers[indexPath.item])
            
            if selectionIndex == indexPath.item {
                cellBroker.setSelected()
            } else {
                cellBroker.setDeselected()
            }
            cell = cellBroker
        }
        
        return cell
    }
}

extension AddEditBrokerTableViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard editMode else { return }
        
        selectionIndex = indexPath.item
        otherCell.isHidden = brokers[selectionIndex].enumValue != .other
        corretoraCollectionView.reloadData()
    }
}

extension AddEditBrokerTableViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.corretoraCollectionView.frame.height * 2,
                      height: self.corretoraCollectionView.frame.height)
    }
}
