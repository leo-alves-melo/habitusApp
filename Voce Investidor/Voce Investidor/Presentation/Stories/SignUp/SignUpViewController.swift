//
//  SignUpViewController.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 08/11/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var password2TextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var showConfirmPasswordButton: UIButton!
    
    weak var delegate: StoryToShowDelegate?
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Notificate the VC that keyboard will appear
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //Notificate the VC that keyboard will disappear
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Let the password be hidden
        self.showPasswordButton.isSelected = false
        self.passwordTextField.isSecureTextEntry = true
        //Let the password be hidden
        self.showConfirmPasswordButton.isSelected = false
        self.password2TextField.isSecureTextEntry = true
        
        //Let the keyboard dismiss when touch on the screen
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tapGesture)
        
        self.activityView.isHidden = true
        
        if self.user == nil {
            self.user = User.instance
        }
        
        if User.instance.email != "" {
            self.emailTextField.text = User.instance.email.lowercased()
        }
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue) {
            let keyboardSize = keyboard.cgRectValue

            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    
                                    self.scrollView.setContentOffset(CGPoint(x: 0,
                                                                             y: keyboardSize.height),
                                                                     animated: false)
                                    
                    }, completion: nil)
                    
                }
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let _ = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue, delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                                    
                    }, completion: nil)
                }
            }
        }
    }
    
    func textFieldsValidate() -> Bool {
        
        //check email format
        var messegeAlert = ""
        
        if let email = self.emailTextField.text {
            if !Validator.validate(email: email) {
                messegeAlert = "Formato de e-mail inválido."
            }
        } else {
            messegeAlert = "Informe o e-mail."
        }
        
        if messegeAlert.isEmpty {
            //compare passwords
            if self.passwordTextField.text != self.password2TextField.text {
                
                messegeAlert = "Senhas incompativeis."
            } else {
                if let count = self.passwordTextField.text?.count {
                    if count < 8 {
                        messegeAlert = "Senha deve conter no mínimo 8 caracteres."
                    }
                } else {
                    messegeAlert = "Informe a senha."
                }
            }
        }

        if !messegeAlert.isEmpty {

            let alert = UIAlertController(title: "Ops!",
                                          message: messegeAlert,
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
            return false
        }
        
        return true
    }
    
    private func changeEye(eye: UIButton, textField: UITextField) {
        if eye.isSelected {
            eye.isSelected = false
            eye.setImage(UIImage(named: "eye_closed"), for: .disabled)
            textField.isSecureTextEntry = true
        } else {
            eye.isSelected = true
            eye.setImage(UIImage(named: "eye_open"), for: .selected)
            textField.isSecureTextEntry = false
        }
    }
    
    private func showActivityView() {
        self.activityView.isHidden = false
    }
    
    @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
        self.changeEye(eye: sender, textField: self.passwordTextField)
    }
    
    @IBAction func showConfirmPasswordButtonTapped(_ sender: UIButton) {
        self.changeEye(eye: sender, textField: self.password2TextField)
    }
    
    func signUp() {
        let userService = UserService()
        
        let iCloudAllowed = UserDAO.isiCloudAllowed()
        UserDAO.allowiCloud()
        
        self.user.email = self.emailTextField.text!
        self.user.hashPassword = Cryptographer().encrypt(sentence: self.passwordTextField.text!) 
        
        self.activityView.isHidden = false
        userService.create(self.user) { [weak self] (userSaved, error) in
            
            DispatchQueue.main.async {
                self?.activityView.isHidden = true
            }
            
            
            if !iCloudAllowed {
                UserDAO.disallowiCloud()
            }
            
            if error == nil {
                
                UserDAO.allowiCloud()
                userService.login(user: userSaved!, completion: { [weak self] (_) in
                    OperationQueue.main.addOperation {
                        UserStatic.userObject = userSaved
                        let delegate = self?.delegate
                        
                        
                        
                        DispatchQueue.main.async {
                            self?.dismiss(animated: true, completion: {
                                delegate?.storyEnded(status: .success)
                            })
                        }
                        
                    }
                })
                return
            } else {
                if let error = error {
                    var message = ""
                    switch error {
                    case .emailAlreadyExists:
                        message = "Já existe uma conta com esse e-mail."
                    case .internetNotConnected:
                        message = "Não foi possível conectar com a internet."
                    case .other:
                        message = "Tente novamente mais tarde."
                    }
                    
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Ops!", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self?.present(alert, animated: true)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        
        self.dismissKeyboard()
        
        if textFieldsValidate() {
            //TODO: Conferir se o e-mail nao está cadastrado
            //TODO: Cadastrar
            //self.showActivityView()
            self.signUp()
        }
        
    }
    
    @IBAction func exitPressed(_ sender: Any) {
        self.dismissKeyboard()
        
        let delegate = self.delegate
        self.dismiss(animated: true, completion: {
            delegate?.storyEnded(status: .failed)
        })
    }

}
