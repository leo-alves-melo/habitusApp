//
//  SettingsViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 11/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import MessageUI

class SettingsViewController: UITableViewController {

    var email = "contato@guruinvestapp.com.br"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavBar()
        self.tableView.tableFooterView = UIView()
        
        //self.showLoadingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    func showLoadingView() {
        let shadowView = UIView(frame: self.view.frame)
        shadowView.backgroundColor = UIColor.gray
        
        self.view.addSubview(shadowView)
    }
    
    /// Set style of naviigationBar
    func customizeNavBar() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isTranslucent = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            //Is IPad
            self.navigationController?.navigationBar.barTintColor =  UIColor(patternImage:
                UIImage(named: "navibarBackgroundIpad")!)
        } else {
            //Is IPhone
            self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:
                UIImage(named: "navibarBackground")!)
        }
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    private func copyEmail(selectedOption: UIAlertAction) {
        UIPasteboard.general.string = self.email
    }
    
    private func sendEmail(selectedOption: UIAlertAction) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            let alert = UIAlertController(title: "Ops!", message: "Não há e-mail configurado em seu device!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([self.email])
        composeVC.setSubject("Comentários sobre o aplicativo")
        composeVC.setMessageBody("", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    private func sendEmail(sender: Any) {
        
        let contact = UIAlertController(title: "Contate-nos!", message: "Envie-nos um e-mail com suas dúvidas ou sugestões!", preferredStyle: .actionSheet)
        if let sender = sender as? UITableViewCell {
            contact.popoverPresentationController?.sourceRect = sender.contentView.layer.contentsRect
            contact.popoverPresentationController?.sourceView = sender.contentView
        }
        
        contact.addAction(UIAlertAction(title: "Copiar e-mail", style: .default, handler: copyEmail))
        contact.addAction(UIAlertAction(title: "Enviar e-mail", style: .default, handler: sendEmail))
        contact.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(contact, animated: true, completion: nil)
        
    }
    
    private func deleteAccount() {


        let alert = UIAlertController(title: "Apagar dados",
                                      message: "Você irá deletar todos as suas informações de nossos servidores. Essa ação é irreversível. Você deseja realmente apagar?",
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Apagar",
                                      style: .default, handler: { (_) in
                                        let dao = UserDAO()
                                        
                                        

                                        dao.cleanMyData { (result) in
                                                UserStatic.userObject = nil
                                            
                                            UserService().exit()
                                            
                                            DispatchQueue.main.async {
                                                let alert = UIAlertController(title: "Dados Apagados",
                                                                              message: "Suas informações foram apagadas dos nossos servidores.",
                                                                              preferredStyle: .alert)
                                                
                                                alert.addAction(UIAlertAction(title: "Ok",
                                                                        style: .default, handler: { action in
                                                                                UserDefaults.standard.set(true, forKey: "notFirstTime")
                                                                                self.performSegue(withIdentifier: "RestartApp", sender: nil)
                                                }))
                                                
                                                self.present(alert, animated: true)

                                            }
                                            
                                        }

        }))
        
        self.present(alert, animated: true)

    }
    
    func exit() {
        
        let alert = UIAlertController(title: "Sair",
                                      message: "Você irá deletar todos as suas informações deste dispositivo, mas manterá as de nossos servidores. Você deseja realmente sair?",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Sair",
                                      style: .default, handler: { (_) in
                                        
                                        UserService().exit()
                                        self.performSegue(withIdentifier: "RestartApp", sender: nil)
        }))
        
        self.present(alert, animated: true)
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            //Send feedback
            if indexPath.row == 0 {
                self.sendEmail(sender: tableView.cellForRow(at: indexPath)!)
            }
        } else if indexPath.section == 1 {
            //Exit my Account
            if indexPath.row == 0 {
                self.exit()
            }
            
        } else {
            
            //Delete my Account
            if indexPath.row == 0 {
                self.deleteAccount()
            }
            
        }
    }
    
    func allowiCloud() {
        UserDAO.allowiCloud()
    }
    
    func disallowiCloud() {
        UserDAO.disallowiCloud()
    }
    
    

    
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        
        if result.rawValue == 2 {
            controller.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: "Obrigado!", message: "Agradecemos pelo seu feedback!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        } else {
            controller.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: "Ops!", message: "Operação cancelada", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
}

