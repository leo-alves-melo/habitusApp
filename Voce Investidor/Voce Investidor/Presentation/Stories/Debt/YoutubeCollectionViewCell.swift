//
//  CollectionViewCell.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 17/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

// Struct holding the content to be displayed at the Debt collection view cells
struct DebtVideo {
    var videoID: String
    var videoImage: UIImage
}

@IBDesignable class YoutubeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    
}
