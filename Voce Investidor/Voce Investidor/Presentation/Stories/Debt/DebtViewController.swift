//
//  FinalTopicViewController.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 05/09/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import StoreKit
class DebtViewController: UIViewController {
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var leftPadding: UIView!
    @IBOutlet weak var rightPadding: UIView!
    
    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    // Collection View Parameters
    let rows = 1
    var dataSource: [DebtVideo] = [DebtVideo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videosCollectionView.contentInset = UIEdgeInsetsMake(0, leftPadding.frame.width,
                                                                  0, rightPadding.frame.width)
        self.setupCollectionViewDataSource()
    }
    
    // Prepare status bar appearances
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        if UIDevice.current.userInterfaceIdiom == .pad {
            // customize label sizes here
        }
    }
    
    // Change status bar appearances back
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    // Load content to collection view data source
    private func setupCollectionViewDataSource() {
        let firstVideo = DebtVideo(videoID: "iwacYbvH-KI", videoImage: UIImage(named: "debt1")!)
        let secondVideo = DebtVideo(videoID: "8zj0GJKTWwE", videoImage: UIImage(named: "debt2")!)
        let thirdVideo = DebtVideo(videoID: "XvRZs-QRUV4", videoImage: UIImage(named: "debt3")!)
        self.dataSource = [firstVideo, secondVideo, thirdVideo]
    }
    
    @IBAction func okAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func playInYoutube(youtubeId: String) {
        if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
            // redirect to app
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
        } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
            // redirect through safari
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
        }
    }
}

extension DebtViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell",
                                                         for: indexPath)
        if let videoCell = cell as? YoutubeCollectionViewCell {
            videoCell.thumbnail.image = self.dataSource[indexPath.item].videoImage
            return videoCell
        }
        return cell
    }
}

extension DebtViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.playInYoutube(youtubeId: dataSource[indexPath.item].videoID)
    }
    
}

extension DebtViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.videosCollectionView.frame.height * 1.67,
                      height: self.videosCollectionView.frame.height)
    }
}
