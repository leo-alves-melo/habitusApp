//
//  AdvantagesViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 24/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class AdvantagesViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipButtonTrailing: NSLayoutConstraint!
    @IBOutlet weak var skipButtonTop: NSLayoutConstraint!
    @IBOutlet weak var advantagesBackgroundImageView: UIImageView!
    @IBOutlet weak var continueButtonHeight: NSLayoutConstraint!
    
    var titlesText: [String] = []
    var descriptionText: [String] = []
    var images: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupBackground()
        self.setupSkipButton()
        self.setupContinueButton()
        self.setupMessages()
        self.setupImages()
        self.setupScrollView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    private func setupBackground() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.advantagesBackgroundImageView.image = UIImage(named: "ipadAdvantagesBack")
        }
    }
    
    private func setupSkipButton() {
        if UIDevice.current.userInterfaceIdiom == .pad {

            self.skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 34, weight: .semibold)
            self.skipButtonTrailing.constant = 38
            self.skipButtonTop.constant = 62
        }
    }
    
    private func lightAttributedString(text: String) -> NSMutableAttributedString {
       
        var font = advantegesTextFontLight
    if UIDevice.current.userInterfaceIdiom == .pad {
        font = advantegesTextFontLightiPad
    }
    let attributes = [NSAttributedStringKey.font: font]

    return NSMutableAttributedString(string: text, attributes: attributes)
    }
    
    private func boldAttributedString(text: String) -> NSMutableAttributedString {
       
        var font = advantegesTextFontHeavy
        if UIDevice.current.userInterfaceIdiom == .pad {
            font = advantegesTextFontHeavyiPad
        }
        let attributes = [NSAttributedStringKey.font: font]
        
        return NSMutableAttributedString(string: text, attributes: attributes)
    }
    
    private func setupContinueButton() {
        self.continueButton.alpha = 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.continueButtonHeight.constant = 72
            self.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        }
    }
    
    private func hideContinueButton() {
        UIView.animate(withDuration: 0.8) {
            self.continueButton.alpha = 0
        }
    }
    
    private func showContinueButton() {
        UIView.animate(withDuration: 0.8) {
            self.continueButton.alpha = 1
        }
        
        UserDefaults.standard.set(true, forKey: "notFirstTime")
    }
    
    private func setupMessages() {
        
        for title in advantagesTextTitle {
            self.titlesText.append(title)
        }
        
        for description in advantagesTextDescription {
            self.descriptionText.append(description)
        }
        
        self.pageControl.numberOfPages = titlesText.count
    }
    
    private func setupImages() {
        self.images.append(UIImage(named: "Vantagens01")!)
        self.images.append(UIImage(named: "Vantagens02")!)
        self.images.append(UIImage(named: "Vantagens03")!)
        self.images.append(UIImage(named: "Vantagens04")!)
    }
    
    private func createTitleLabel(text: String) -> UILabel {
        let label = UILabel(frame: CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2,
                                          width: self.view.frame.width*0.9, height: self.view.frame.height))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            label.center = CGPoint(x: view.center.x, y: view.center.y + view.frame.height/13)
        } else {
            label.center = CGPoint(x: view.center.x, y: view.center.y + view.frame.height/15)
        }
        label.textAlignment = .left
        label.attributedText = self.boldAttributedString(text: text)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.frame = (label.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
        
        return label
    }
    
    private func createDescriptionLabel(text: String) -> UILabel {
        let label = UILabel(frame: CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2,
                                          width: self.view.frame.width*0.9, height: self.view.frame.height))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            label.center = CGPoint(x: view.center.x, y: view.center.y + 9*view.frame.height/40)
        } else {
            label.center = CGPoint(x: view.center.x, y: view.center.y + view.frame.height/5)
        }
        label.textAlignment = .left
        label.attributedText = self.lightAttributedString(text: text)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 4
        label.textColor = UIColor.white
        label.frame = (label.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
        
        return label
    }
    
    private func createImageView(image: UIImage) -> UIImageView {
        
        var imageWidth = self.view.frame.width*0.65
        var imageHeight = self.view.frame.width*0.5
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            imageWidth = self.view.frame.width*0.51
            imageHeight = self.view.frame.height*0.32
        }
        
        let imageView = UIImageView(frame: CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2,
                                                  width: imageWidth, height: imageHeight))
        imageView.image = image
        
        imageView.contentMode = .scaleAspectFit
        
        imageView.center = CGPoint(x: view.center.x, y: view.center.y - view.frame.height/5)
        imageView.frame = (imageView.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))

        return imageView
    }
    
    private func setupScrollView() {
        // Add all pages in scrollView
        for index in 0..<self.titlesText.count {
            let titleLabel = self.createTitleLabel(text: self.titlesText[index])
            let descriptionLabel = self.createDescriptionLabel(text: self.descriptionText[index])
            let imageView = self.createImageView(image: self.images[index])
            
            scrollView.addSubview(titleLabel)
            scrollView.addSubview(descriptionLabel)
            scrollView.addSubview(imageView)
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width + self.view.frame.width,
                                            height: 0)
        }
    }
    
    private func hideSkipButton() {
        UIView.animate(withDuration: 0.25, animations: {
            self.skipButton.alpha = 0

        })
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "notFirstTime")
        self.performSegue(withIdentifier: "AdvantagesDidEnd", sender: nil)
    }
}

extension AdvantagesViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = floor(scrollView.contentOffset.x / self.view.frame.width)
        
        self.pageControl.currentPage = Int(page)
        
        // esconde botao de pular
        if Int(page) == pageControl.numberOfPages - 1 {
            self.hideSkipButton()
            
            // exibe botao de continuar
            self.showContinueButton()
        }
    }
}
