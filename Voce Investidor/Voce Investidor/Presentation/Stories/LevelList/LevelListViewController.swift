//
//  LevelListViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 18/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class LevelListViewController: CustomizedNavBarViewController {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var levelsCollectionView: UICollectionView!
    
    var levelsList: [Level] = []
    var selectedLevel: Level!
    
    private var callMigrationChat = false
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        LevelService().allLevels { (levels, error) in
            guard let levels = levels, error == nil else {
                print("Can't get levels")
                return
            }
            
            self.levelsList = levels
            
            self.levelsCollectionView.reloadData()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.view.bringSubview(toFront: self.activityView)

        // - Inicio -- Bloco de teste
//        var user = User.instance
//        user.email = "leo@gmail.com"
//        UserStatic.userObject = user
//        UserDefaults.standard.set(true, forKey: "AllowiCloud")
//
        // - FIM -- Bloco de teste

        if UserStatic.userObject == nil {

            let userService = UserService()
            
            userService.getLogged { (user, error) in
                OperationQueue.main.addOperation {
                    self.activityView.isHidden = true
                }

                if let error = error {
                    UserStatic.userObject = user
                    
                    switch error {
                    case .userDoesNotHavePassword:
                        print("precisa criar senha") //CRIAR SENHA
                        
                        if !UserDefaults.standard.bool(forKey: "didOfferPasswordCreation") {
                            self.performSegue(withIdentifier: "showMigrationSegue", sender: nil)
                        }
                        
                    case .userDoesNotExist:
                        self.performSegue(withIdentifier: "showDiagnosticSegue", sender: nil)
                    case .internetNotConnected:
                        OperationQueue.main.addOperation {
                            self.activityView.isHidden = true
                        }
                    default:
                        print("Error")
                    }

                    return
                }
                
//                var broker = BrokerAccount(brokerAccount: .easynvest)
//
//                broker.openDate = Date()
//                
//                //BrokerAccountServices().create(brokerAccount: broker, completion: { (error) in
//                    BrokerAccountServices().brokerAccounts(from: User.instance, completion: { (brokers, error) in
//                        print(brokers)
//                        
//                    })
//                //})
            }
        } else if let email = UserStatic.userObject?.email {
            // Esse trecho serve para migrar os usuário da versão que não pedia cadastro, para a que pede cadastro. No caso, o usuário que possui dados cadastrados no icloud, mas não possui senha.
            
            let userService = UserService()
            
            userService.haveRegisteredHashPassword(email: email, completion: { (hasPassword, error) in
                
                if error != nil, let hasPassword = hasPassword, !hasPassword {
                    self.performSegue(withIdentifier: "showDiagnosticSegue", sender: nil)
                    self.callMigrationChat = true
                    return
                }
                
                DispatchQueue.main.async {
                    self.activityView.isHidden = true
                }
                
            })
        } else {
            
            self.activityView.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "showDiagnosticSegue" {
            if let destination = segue.destination as? UINavigationController {
                if let chatVC = destination.childViewControllers.first as? ChatViewController {
                    chatVC.presentedModal = true
                    chatVC.parentVC = self
                    if callMigrationChat {
                        chatVC.messageID = "diag_migration_1"
                    }
                }
            }
        } else if segue.identifier == "showMigrationSegue" {
            if let destination = segue.destination as? UINavigationController {
                if let chatVC = destination.childViewControllers.first as? ChatViewController {
                    chatVC.presentedModal = true
                    chatVC.parentVC = self
                    chatVC.messageID = "migration"
                }
            }
        } else {

            if let destination = segue.destination as? LessonListViewController {
                destination.level = self.selectedLevel
            }
        }
    }
}

extension LevelListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

       return self.levelsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LevelListCollectionViewCell",
                                                            for: indexPath) as? LevelListCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.topTitleConstraint.constant = cell.frame.size.height * 0.20
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.leadingConstraint.constant = cell.frame.size.height * 0.1
            cell.traillingConstraint.constant = cell.frame.size.height * 0.1
        }
        
        let level = levelsList[indexPath.row]
        
        cell.descriptionLabel.text = level.subtitle

        cell.titleLabel.text = level.title
        
        LevelService().getLevelCompletude(for: level) { (completude, error) in
            
            guard let completude = completude, error == nil else {
                return
            }
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            let progress: CGFloat = min(1.0, CGFloat(completude) / CGFloat(level.lessonCount ?? 1))
            
            cell.widthConstraint.constant = cell.progressBarView.frame.width * CGFloat(progress)
        }

        cell.layer.shadowColor = UIColor(red: 111.0/255.0,
                                         green: 136.0/255.0,
                                         blue: 242.0/255.0,
                                         alpha: 0.15).cgColor
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedLevel = self.levelsList[indexPath.row]
        self.performSegue(withIdentifier: "LevelSelected", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeCard = self.calcSizeCard()
        
        return CGSize(width: sizeCard, height: sizeCard)
    }
    
    
    func calcSizeCard() -> CGFloat {
        var cellForRow: CGFloat = 2
        if UIDevice.current.userInterfaceIdiom == .pad {
            cellForRow = 3
        }
        let numberOfSpaces: CGFloat = cellForRow + 1
        let spaceBetweenCell: CGFloat  = 18
        let sumOfSpaces = spaceBetweenCell * numberOfSpaces
        
        return (self.view.frame.width - sumOfSpaces) / cellForRow
    }
}
