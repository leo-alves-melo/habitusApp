//
//  LevelListCollectionViewCell.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 19/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class LevelListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var topTitleConstraint: NSLayoutConstraint!
    @IBOutlet weak var traillingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.titleLabel.font = self.titleLabel.font.withSize(35)
            self.descriptionLabel.font = self.descriptionLabel.font.withSize(25)
        }
    }
}
