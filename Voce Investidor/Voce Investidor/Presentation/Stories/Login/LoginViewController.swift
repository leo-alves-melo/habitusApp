//
//  LoginViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 08/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var activityView: UIView!
//    var completionLogin: (()->Void)?
    
    weak var delegate: StoryToShowDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Notificate the VC that keyboard will appear
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //Notificate the VC that keyboard will disappear
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Let the password be hidden
        self.showPasswordButton.isSelected = false
        self.passwordTextField.isSecureTextEntry = true
        
        //Let the keyboard dismiss when touch on the screen
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tapGesture)
        
        self.activityView.isHidden = true
    }
    
    /// Dismiss the keyboard on the VC
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

        if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue) {
            let keyboardSize = keyboard.cgRectValue
            
            
                
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    
                    UIView.animate(withDuration: duration.doubleValue,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y: keyboardSize.height), animated: false)
                                    
                    }, completion: nil)
                    
                }
                
            }
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {

        if let _ = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
           
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {


                    UIView.animate(withDuration: duration.doubleValue, delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                   animations: {
                                    self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

                    }, completion: nil)

                }
            }
            
        }
    }
    
    @IBAction func showPasswordButtonDidTapped(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            sender.setImage(UIImage(named: "eye_closed"), for: .disabled)
            self.passwordTextField.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            sender.setImage(UIImage(named: "eye_open"), for: .selected)
            self.passwordTextField.isSecureTextEntry = false
        }
    }
    
    func hideActivityView(hide: Bool) {
        DispatchQueue.main.async {
            self.activityView.isHidden = hide
        }
    }
    
    func finishLogin() {
        
        self.hideActivityView(hide: false)
        
        let iCloudAllowed = UserDAO.isiCloudAllowed()
        UserDAO.allowiCloud()
        
        UserService().getLogin(email: self.emailTextField.text!,
                               hashPassword: Cryptographer().encrypt(sentence: self.passwordTextField.text!)) { [weak self] (user, error) in
                                
                                self?.hideActivityView(hide: true)
                                
                                if !iCloudAllowed {
                                    UserDAO.disallowiCloud()
                                }
                                
                                guard error == nil else {
                                    
                                    var messageError = ""
                                    switch error! {
                                    case .emailNotFound:
                                        messageError = "E-mail não cadastrado."
                                    case .iCloudNotConnected:
                                        messageError = "O iCloud não está conectado."
                                    case .internetNotConnected:
                                        messageError = "Não foi possível conectar a internet."
                                    case .userDoesNotHavePassword:
                                        messageError = "Você ainda não criou sua senha. Faça o cadastro."
                                    case .wrongPassword:
                                        messageError = "Senha incorreta."
                                    default:
                                        messageError = "Tente novamente mais tarde."
                                    }
                                    
                                    let alert = UIAlertController(title: "Ops!",
                                                                  message: messageError,
                                                                  preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                    self?.present(alert, animated: true, completion: nil)
                                    
                                    return
                                }
                                
                                if let _ = user {
                                    let delegate = self?.delegate
                                    
                                    UserDAO.allowiCloud()
                                    
                                    DispatchQueue.main.async {
                                        self?.dismiss(animated: true, completion: {
                                            delegate?.storyEnded(status: .success)
                                        })
                                    }
                                    
                                } else {
                                    let alert = UIAlertController(title: "Ops!",
                                                                  message: "E-mail ou senha inválidos!",
                                                                  preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                    self?.present(alert, animated: true, completion: nil)
                                }
        }
    }

    @IBAction func loginDidTapped(_ sender: Any) {
        
        self.dismissKeyboard()
        
        if let email = self.emailTextField.text {
            if let password = self.passwordTextField.text {
                if Validator.validate(email: email) {
                    
                    self.finishLogin()
                    
                } else {
                    let alert = UIAlertController(title: "Ops!", message: "Coloque um e-mail válido!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let alert = UIAlertController(title: "Ops!", message: "Coloque uma senha!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Ops!", message: "Coloque um e-mail!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        //self.activityView.isHidden = false
        
        
    }
    
    @IBAction func exitButtonDidTapped(_ sender: Any) {
        let delegate = self.delegate
        self.dismissKeyboard()
        self.dismiss(animated: true, completion: {
            delegate?.storyEnded(status: .failed)
        })
    }
    
    @IBAction func forgotMyPasswordDidTapped(_ sender: Any) {
        self.dismissKeyboard()
        if let url = URL(string: "https://guruinvestapp.com.br/recuperarsenha") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
