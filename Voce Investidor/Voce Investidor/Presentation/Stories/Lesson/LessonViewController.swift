//
//  ViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 15/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class LessonViewController: UIViewController {

    // Outlets dos elementos de interface
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cardViewContainer: SwipeableCardViewContainer!
    
    @IBOutlet weak var progressBarFillConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityView: UIView!
    
    // Cards do topico. Serve como fonte para o protocolo de CardViewDataSource.
    // ATENÇÃO! Não é uma lista de views, e sim de controllers. Para acessar a view, faça cards[0].cardView
    var cards: [CardController] = []
    var lesson: Lesson!
    weak var delegate: NextLessonDelegate?

    // Index da carta atualmente no topo da pilha
    var currentCardIndex = 0
    var finished = false
    var firstSetup = true
    
    var initialTime = Date()
    
    // As Subviews têm tamanhos dinâmicos, dependentes de elementos do ViewController
    // O setup das subviews então só deveria ser feito depois do primeiro autolayout
    override func viewDidLayoutSubviews() {
        if firstSetup {
            firstSetup = false

            self.setupCards()
            self.setupProgressbar()
            self.updateCountLabel()
            self.setupConfirmButton()
            self.view.bringSubview(toFront: self.activityView)
        }
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()

        guard self.lesson != nil else {
            print("No Lesson was passed")
            return
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !finished {
            let timeInLesson = self.timeInLesson()
            let oldTime = self.oldTimeSpended()
            
            self.setTimeSpended(time: timeInLesson + oldTime)
        }
        
    }
    
    //TODO: Criar funcoes confirmeButtonVisible e confirmeButtonInvisible ou algo do tipo
    func setupConfirmButton() {
        self.confirmButton.alpha = 0.0
    }
    
    /// Instanciar card controllers e container
    func setupCards() {
    
        // Pegar o currentCardIndex de persistencia de dados
        if let lastSeenIndex = self.lesson.lastSeenIndex {
            self.currentCardIndex = lastSeenIndex
            print(lastSeenIndex)
        }
        
        // Frame que define o tamanho de todos os cards
        let frame = CGRect(origin: CGPoint.zero, size: cardViewContainer.frame.size)
        
        // Preenche lista de controllers de cards. Para cada um, cria uma borda no cardView que ele contém
        // e adiciona o controller na lista de cardControllers
        let cardControllers = CardFactory(delegate: self).makeCards(from: lesson,
                                                    frame: frame)
        for cardController in cardControllers {
            if let cardController = cardController {
                self.createBorder(card: cardController.cardView)
                self.cards.append(cardController)
            }
        }
        
        // Inicializa o container de cardViews.
        self.cardViewContainer.delegate = self
        self.cardViewContainer.dataSource = self
        self.view.bringSubview(toFront: cardViewContainer)
    }
    
    /// Inicializa a barra de progresso.
    func setupProgressbar() {
        self.progressBar.backgroundColor = progressBarBackgroundColor
        
        self.progressBarFillConstraint.constant = calcWidthProgressBar()
    }
    
    // Atualiza o texto do label de progresso de acordo com a contagem atual de cards
    func updateCountLabel() {
        if self.currentCardIndex >= self.cards.count {
            self.counterLabel.text = "Tópico concluído!"
        } else {
            self.counterLabel.text = "\(self.currentCardIndex+1) de \(self.cards.count)"
        }
    }
    
    func calcWidthProgressBar() -> CGFloat {
        // A barra nunca deve ficar vazia. Ao invés disso, há um mínimo do preenchimento
        let minProgressWidth: CGFloat = 3.0

        print("self.progressBar.frame.width \(self.progressBar.frame.width)")
        
        return max(minProgressWidth,
                   CGFloat(self.currentCardIndex) * self.progressBar.frame.width /
                    CGFloat(self.cards.count - 1))
    }
    
    // Atualiza o preenchimento da barra de progresso de acordo com a contagem atual de cards
    func updateProgressBar() {
        var newWidth = calcWidthProgressBar()
        newWidth = min(newWidth, self.progressBar.frame.width)
       
        self.progressBarFillConstraint.constant = newWidth
        UIView.animate(withDuration: 0.25, animations: {
            self.progressBar.layoutSubviews()
        })
    }
    
    /// Create the border of the Card
    ///
    /// - Parameter card: The card to be applied the border
    func createBorder(card: CardView) {
        card.xContentView.layer.masksToBounds = true
        card.xContentView.layer.cornerRadius = 9
    }
    
    // Mostrar ou esconder botão de confirmação de resposta, se necessário
    func updateConfirmButton() {
        if let questionCard = self.cards[self.currentCardIndex] as? QuestionCardController {
            self.confirmButton.isHidden = false
            self.confirmButton.setTitle(questionCard.getActionTitle(), for: .normal)
            self.confirmButton.backgroundColor = questionCard.getActionColor()
            self.confirmButton.alpha = questionCard.getActionAlpha()
        } else {
            self.confirmButton.isHidden = true
        }
    }
    
    // Preparações necessárias para realização de segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueFinal"{
            if let next = segue.destination as? FinalLessonViewController {
                next.lesson = self.lesson
                next.delegate = self.delegate
            }
        }
    }
    
    // Ação do botão "next", deve mostrar carta seguinte se houver ou realizar segue final caso contrário
    @IBAction func nextButtonTapped(_ sender: Any) {
        guard self.currentCardIndex < self.cards.count else { return }
        self.dismissCard()
    }
    
    // Ação do botão "back", deve mostrar carta anterior se houver
    @IBAction func backButtonTapped(_ sender: Any) {
        guard self.currentCardIndex > 0 else { return }
        self.reinsertCard()
    }
    
    // Ação do botão de confirmação de resposta
    @IBAction func confirmButtonDidTapped(_ sender: Any) {
        if self.confirmButton.alpha == 1 {
            if let questionCard = self.cards[self.currentCardIndex] as? QuestionCardController {
                questionCard.startAction()
                self.updateConfirmButton()
            }
        }
    }
    
    @IBAction func exitButtonDidTapped(_ sender: Any) {
        
        // Mostrar alert apenas se o usuário alcançou dois terços da lição
        if self.currentCardIndex <= 2 * self.cards.count / 3 {
            AnalyticsService().send(indexStopped: self.currentCardIndex, for: self.lesson)
            self.dismiss(animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Você está quase terminando!",
                                          message: "Tem certeza que deseja sair antes de completar a lição?",
                                          preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Sair",
                                          style: .default,
                                          handler: { [weak self] _ in
                                            if let index = self?.currentCardIndex, let lesson = self?.lesson {
                                                AnalyticsService().send(indexStopped: index, for: lesson)
                                                self?.dismiss(animated: true, completion: nil)
                                            }
                                          }))
            
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func timeInLesson() -> Int {
        let currentTime = Date()
        
        let timeSpended = DateInterval(start: self.initialTime, end: currentTime)
        
        return Int(timeSpended.duration)
    }
    
    func oldTimeSpended() -> Int {
        return UserDefaults.standard.integer(forKey: self.lesson.identification! + "-" + "oldTimeSpended")
    }
    
    func setTimeSpended(time: Int) {
        UserDefaults.standard.set(time, forKey: self.lesson.identification! + "-" + "oldTimeSpended")
    }
    
    func sendTimeToAnalytics() {
        
        let oldTimeSpended = self.oldTimeSpended()
        let timeInLesson = self.timeInLesson()

        AnalyticsService().send(time: timeInLesson + oldTimeSpended, for: self.lesson)
        
        self.setTimeSpended(time: 0)
    }
    
    func finishLesson() {
        
        self.finished = true
        
        self.sendTimeToAnalytics()

        let lessonService = LessonService()
        lessonService.getLessonFinished(for: self.lesson, completion: { (finished, error) in
            if finished == false {
                lessonService.setLessonFinished(for: self.lesson, finished: true)
                if let currentLevel = self.lesson.level {
                    LevelService().incrementLevelCompletude(for: currentLevel, completion: nil)
                }
            }
        })
        
        LessonService().saveLessonIndex(for: self.lesson,
                                        index: self.cards.count - 1,
                                        completion: { (error) in
                                            if let error = error {
                                                print(error.localizedDescription)
                                            }
                                            self.performSegue(withIdentifier: "SegueFinal", sender: nil)
        })
    }
    
    // Screenshot of card for sharing
    func takeCardScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.cardViewContainer.bounds.size, false, UIScreen.main.scale)
        self.cardViewContainer.drawHierarchy(in: self.cardViewContainer.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    // Calls sharing controller
    @IBAction func shareAction(_ sender: Any) {
        
        // image to share
        let image = takeCardScreenshot()
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension LessonViewController: QuestionCardControllerDelegate {
    func didAnswer(answer: Answer) {
        print(answer)
    }
    
    func didSelected(option: [Int]) {
        self.confirmButton.alpha = 1
    }
    
    func didDeselected() {
        self.confirmButton.alpha = 0.5
    }
}

extension LessonViewController: SwipeableCardViewDataSource {
    func numberOfCards() -> Int {
        return cards.count
    }
    
    func card(forItemAtIndex index: Int) -> SwipeableCardView {
        return cards[index].cardView
    }
    
    func viewForEmptyCards() -> UIView? {
        return nil
    }
}

extension LessonViewController: LessonControllerDelegate {
    
    func dismissCard() {
        if self.currentCardIndex == self.cards.count - 1 {
            self.finishLesson()
            return
        }
        
        if let singleAnswer = self.cards[self.currentCardIndex] as? SingleAnswerXIBController {
            if singleAnswer.cardView is AnswerXIBView {
                singleAnswer.restartQuestion { [weak self] (_) in
                    if let strongSelf = self {
                        strongSelf.updateProperties(newCardIndex: max(strongSelf.currentCardIndex + 1, 0))
                    }
                }
                return
            }
        }
        self.updateProperties(newCardIndex: max(self.currentCardIndex + 1, 0))
    }
    
    func reinsertCard() {
        guard self.currentCardIndex > 0 else { return }
        
        if let singleAnswer = self.cards[self.currentCardIndex] as? SingleAnswerXIBController {
            if singleAnswer.cardView is AnswerXIBView {
                singleAnswer.restartQuestion { [weak self] (_) in
                    if let strongSelf = self {
                        strongSelf.updateProperties(newCardIndex: max(strongSelf.currentCardIndex - 1, 0))
                    }
                }
                return
            }
        }
        self.updateProperties(newCardIndex: max(self.currentCardIndex - 1, 0))
    }
    
    private func updateProperties(newCardIndex: Int) {
        if newCardIndex > currentCardIndex {
            LessonService().saveLessonIndex(for: self.lesson,
                                            index: newCardIndex,
                                            completion: nil)
        }
        self.cardViewContainer.newUpdateCardStack(from: currentCardIndex, to: newCardIndex)
        self.currentCardIndex = newCardIndex
        self.updateCountLabel()
        self.updateProgressBar()
        self.updateConfirmButton()
    }
    
    func select(card: SwipeableCardView, atIndex index: Int) {
        self.dismissCard()
    }
    
    func getCurrentCardIndex() -> Int {
        return self.currentCardIndex
    }
}

extension LessonViewController: CardFactoryDelegate {
    func didFinishLoadingCards(error: Error?) {
        guard error == nil else {
            print("Error when finishing loading cards: \(error!.localizedDescription)")
            
            let alert = UIAlertController(title: "Ops!", message: "Ocorreu um erro ao acessar os dados do Banco Central. Verifique sua conexão com a internet.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        self.activityView.isHidden = true
    }
}
