//
//  FinalTopicViewController.swift
//  Voce Investidor
//
//  Created by Jessica Batista de Barros Cherque on 05/09/2018.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import StoreKit
class FinalLessonViewController: UIViewController {
    
    var lesson: Lesson!
    weak var delegate: NextLessonDelegate?
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var phraseLabel: UILabel!
    @IBOutlet weak var imageDistanceFromTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phraseLabel.text = (lesson.completionEffectPhrase)
        self.imageDistanceFromTop.constant = self.view.frame.height*CGFloat(0.12)
        
        var fontAtrAry = self.phraseLabel.font.fontDescriptor.symbolicTraits
        fontAtrAry.insert([.traitItalic])
        let fontAtrDetails = self.phraseLabel.font.fontDescriptor.withSymbolicTraits(fontAtrAry)
        self.phraseLabel.font = UIFont(descriptor: fontAtrDetails!, size: self.phraseLabel.font.pointSize)

        if lesson.lessonNumber == 1 && lesson.level?.levelNumber == 1 ||
           lesson.lessonNumber == 1 && lesson.level?.levelNumber == 2 ||
           lesson.lessonNumber == 1 && lesson.level?.levelNumber == 4 {

            let twoSecondsFromNow = DispatchTime.now() + 2.0
            DispatchQueue.main.asyncAfter(deadline: twoSecondsFromNow) { [navigationController] in
                SKStoreReviewController.requestReview()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.titleLabel.font = self.titleLabel.font.withSize(60)
            self.subtitleLabel.font = self.subtitleLabel.font.withSize(40)
            self.phraseLabel.font = self.phraseLabel.font.withSize(38)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func continuePressed(_ sender: Any) {
        guard let presentingViewController = self.presentingViewController,
            let delegate = self.delegate,
            let nextLessonNumber = self.lesson.lessonNumber
        else {
            return
        }

        self.dismiss(animated: true, completion: {
            presentingViewController.dismiss(animated: true, completion: {
                delegate.startLesson(number: nextLessonNumber)
                
            })
            
        })
    }
    
    @IBAction func exitButtonDidTapped(_ sender: Any) {
        let presentingViewController = self.presentingViewController
        self.dismiss(animated: true, completion: {
            presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
}
