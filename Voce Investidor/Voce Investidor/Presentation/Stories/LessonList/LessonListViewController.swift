//
//  LessonListViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 14/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

protocol NextLessonDelegate: NSObjectProtocol {
    func startLesson(number: Int)
}

class LessonListViewController: CustomizedNavBarViewController {

    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var lessonTableView: UITableView!
    
    var lessonList: [Lesson] = []
    var level: Level?
    var selectedLesson: Int?
    var user: User!

    private var firstSetup = true
    
    override func viewDidLayoutSubviews() {
        if firstSetup {
            firstSetup = false
            lessonTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lessonTableView.tableFooterView = UIView()
        self.navigationItem.title = ""
        
        if let level = self.level {
            if let levelTitle = level.title {
                self.navigationItem.title = "\(levelTitle) - Lições"
                
                LessonService().allLessons(level: level) { (lessonListReceived, error) in
                    guard let lessonList = lessonListReceived, error == nil else {
                        print("Error in LessonListViewController: \(error.debugDescription)")
                        return
                    }
                    
                    self.lessonList = lessonList
                }
            }
        }

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lessonTableView.reloadData()

        self.view.bringSubview(toFront: self.activityView)

        let userService = UserService()

        if UserStatic.userObject == nil {
            userService.getLogged { (user, error) in
                OperationQueue.main.addOperation {
                    self.activityView.isHidden = true
                }

                if error == nil {

                    UserStatic.userObject = user
                    self.user = user

                    if let level = self.level {
                        if (user?.payCarFinancing == nil && level.levelNumber == 2) || (user?.wantToBuyCar == nil && level.levelNumber == 3){
                            OperationQueue.main.addOperation {
                                self.performSegue(withIdentifier: "showDiagnosticSegue", sender: nil)
                            }
                        } else {
                            
                            //Checar se precisa responder a crise
                            OperationQueue.main.addOperation {
                                self.showCrisis()
                            }
                        }
                    }
                    return
                }
            }
        } else {

            self.user = UserStatic.userObject

            OperationQueue.main.addOperation {
                self.activityView.isHidden = true
            }

            if (self.user.payCarFinancing == nil && self.level?.levelNumber == 2) || (self.user.wantToBuyCar == nil && self.level?.levelNumber == 3){
                OperationQueue.main.addOperation {
                    self.performSegue(withIdentifier: "showDiagnosticSegue", sender: nil)

                }
                return
            }

        }

        self.user = UserStatic.userObject
        showCrisis()
        
    }

    func showCrisis() {
        if self.user == nil {
            return
        }

        if level?.levelNumber == 1 && self.user.crisisCarBreak == nil {
            self.performSegue(withIdentifier: "showCrisisSegue", sender: nil)
        }
        //TODO: Não eh esse, to usando porque não tinha opção de demição
        if level?.levelNumber == 2 && self.user.crisisHouseRent == nil {
            self.performSegue(withIdentifier: "showCrisisSegue", sender: nil)
        }

        if level?.levelNumber == 3 && self.user.crisisRetire == nil {
            self.performSegue(withIdentifier: "showCrisisSegue", sender: nil)
        }

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? LessonViewController {
            if let lesson = self.selectedLesson {
                destination.lesson = lessonList[lesson]
                destination.delegate = self
            }
        }

        if segue.identifier == "showCrisisSegue" {

            if let destination = segue.destination as? UINavigationController {

                if let crisisController = destination.childViewControllers[0] as? CrisisViewController {
                    if let level = level {
                        if let levelNumber = level.levelNumber {
                            crisisController.user = self.user
                            crisisController.activeCrisis = "C-\(levelNumber)"
                        }
                    }
                }
            }
            return
        }

        if let destination = segue.destination as? UINavigationController
        
        {
            if let destinationChat = destination.childViewControllers[0] as? ChatViewController {
                if let level = level {
                    if let levelNumber = level.levelNumber {
                        destinationChat.messageID = "diag_\(levelNumber)"
                        destinationChat.presentedModal = true
                    }
                    
                }
            }
        }
    }
    
    func updateImage(for cell: LessonListTableViewCell, imagePath: String?) {

        cell.lessonImageView.image = UIImage(named: "icons8-image")
        
        if let imagePath = imagePath {
            if let image = UIImage(named: imagePath) {
                cell.lessonImageView.image = image
            } else {
                if let url = URL(string: imagePath) {
                    URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
                        guard let data = data, error == nil else { return }
                        
                        if let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                cell.lessonImageView.image = image
                            }
                        }
                        
                    }).resume()
                }
            }
        }
    }
}

extension LessonListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lessonList.count
    }
    
    // TODO: Add comments
    // TODO: Put Cell initialization methods in the Cell class
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            "LessonListTableViewCell", for: indexPath) as? LessonListTableViewCell {
            let lesson = self.lessonList[indexPath.row]
            
            if let duration = lesson.duration {
                cell.lessonDuration.text = "\(duration) min"
            }

            cell.lessonTitle.text = lesson.title
            
            LessonService().getLessonIndex(for: lesson) { (index, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                guard let index = index else {
                    print("No index")
                    return
                }
                
                self.lessonList[indexPath.row].lastSeenIndex = index
                
                if let contentListCount = lesson.contentList?.count {
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    self.lessonList[indexPath.row].percentageCompleted = (100*index)/contentListCount
                    cell.progressBarWidth.constant = CGFloat(index)/CGFloat(contentListCount)*cell.progressBarView.frame.width
                }
            }
            
            self.updateImage(for: cell, imagePath: lesson.imagePathCell)

            // Round the corners and setup cell shadow
            cell.cellView.layer.cornerRadius = 9
            cell.cellView.layer.masksToBounds = true
            cell.shadowView.layer.cornerRadius = 9
            cell.shadowView.layer.masksToBounds = false
            
            return cell
            
        } else {
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(103)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedLessonIndex = indexPath.row
        
        //If the lesson list is completed, asks user if he wants to redo
        if let percentageCompleted = self.lessonList[selectedLessonIndex].percentageCompleted {
            if percentageCompleted == 100 {
                let alert = UIAlertController(title: "Refazer?",
                                              message: "Deseja fazer a lição \"\(self.lessonList[selectedLessonIndex].title!)\"do início? Seu progresso nela será perdido.",
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { [weak self] (_) in
                    
                    guard self != nil else {
                        return
                    }
                    
                    self!.lessonList[selectedLessonIndex].lastSeenIndex = 0
                    self!.lessonList[selectedLessonIndex].percentageCompleted = 0

                    LessonService().redo(lesson: self!.lessonList[selectedLessonIndex])

                    self!.selectedLesson = selectedLessonIndex
                    self!.performSegue(withIdentifier: "didSelectedLesson", sender: nil)

                }))
                
                alert.addAction(UIAlertAction(title: "Não", style: .default, handler: { [weak self] (_) in
                    
                    guard self != nil else {
                        return
                    }
                    
                    self!.selectedLesson = selectedLessonIndex
                    self!.performSegue(withIdentifier: "didSelectedLesson", sender: nil)
                }))
                
                alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
                
            } else {
                self.selectedLesson = selectedLessonIndex
                self.performSegue(withIdentifier: "didSelectedLesson", sender: nil)
            }
            
        }
        
        
    }
}

extension LessonListViewController: NextLessonDelegate {
    func startLesson(number: Int) {
        guard let level = self.level else { return }
        
        LevelService().getLevelCompletude(for: level) {
            [weak self] (completude, error) in
            guard let completude = completude, error == nil else { return }
            
            let progress: CGFloat = min(1.0, CGFloat(completude) / CGFloat(level.lessonCount ?? 1))
            if progress == 1.0 {
                let alert = UIAlertController(title: "Parabéns!",
                                              message: "Você terminou todas as lições desse nível!",
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK",
                                              style: .default, handler: nil))
                
                self?.present(alert, animated: true)
            } else if let count = self?.lessonList.count, number < count {
                self?.selectedLesson = number
                self?.performSegue(withIdentifier: "didSelectedLesson", sender: nil)
            }
        }
    }
}
