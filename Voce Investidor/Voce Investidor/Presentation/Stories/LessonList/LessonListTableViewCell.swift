//
//  LessonListTableViewCell.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 14/09/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class LessonListTableViewCell: UITableViewCell {

    @IBOutlet weak var lessonImageView: UIImageView!
    @IBOutlet weak var lessonTitle: UILabel!
    @IBOutlet weak var lessonDuration: UILabel!
    @IBOutlet weak var progressBarWidth: NSLayoutConstraint!
    
    @IBOutlet weak var shadowView: ShadowView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var progressBarView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        self.layer.shadowOffset = CGSize(width: 4, height: 7)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 1.0
        self.layer.shadowColor = UIColor(red: 111.0/255.0,
                                                    green: 136.0/255.0,
                                                    blue: 242.0/255.0,
                                                    alpha: 0.15).cgColor
        
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
