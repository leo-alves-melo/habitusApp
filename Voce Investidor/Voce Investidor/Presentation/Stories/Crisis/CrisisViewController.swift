//
//  ViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 15/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class CrisisViewController: CustomizedNavBarViewController {
    
    // Outlets dos elementos de interface
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cardViewContainer: SwipeableCardViewContainer!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var skipButton: UIBarButtonItem!
    
    var activeCrisis: String?
    var emptyView: UIView? = nil
    var option = -1
    var user: User!
    
    var responded = false
    
    // Cards do topico. Serve como fonte para o protocolo de CardViewDataSource.
    // ATENÇÃO! Não é uma lista de views, e sim de controllers. Para acessar a view, faça cards[0].cardView
    var cards: [CardController] = []
    var crisis: Crisis!
    weak var delegate: NextLessonDelegate?
    
    // Index da carta atualmente no topo da pilha
    var currentCardIndex = 0
    var finished = false
    var firstSetup = true
    
    var initialTime = Date()
    var skippable: Bool = false
    
    // As Subviews têm tamanhos dinâmicos, dependentes de elementos do ViewController
    // O setup das subviews então só deveria ser feito depois do primeiro autolayout
    override func viewDidLayoutSubviews() {
        if firstSetup {
            firstSetup = false
            
            self.setupEmptyView()
            
            self.setupConfirmButton()
 
            self.view.bringSubview(toFront: self.activityView)
            
            if self.crisis != nil {
                self.setupCards()
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { [weak self] (timer) in
            self?.displaySkipButton(animated: true)
            timer.invalidate()
        })
        
        guard self.crisis != nil else {
            //print("No Crisis was passed")
            CrisisService().allCrisis(completion: { [weak self] (crisisList, error) in
                if let list = crisisList {
                    for item in list {
                        if item.identification == self?.activeCrisis {
                            self?.crisis = item
                            
                        }
                    }
                }
                if self?.crisis == nil {
                    DispatchQueue.main.async {
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            })
            return
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setupEmptyView() {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let view = UIView(frame: frame)
        let label = UILabel(frame: frame)
        view.addSubview(label)
        
        self.emptyView = view
    }
    
    func setupConfirmButton() {
        self.confirmButton.alpha = 0.0
    }
    
    /// Instanciar card controllers e container
    func setupCards() {
        
//        // Pegar o currentCardIndex de persistencia de dados
//        if let lastSeenIndex = self.lesson.lastSeenIndex {
//            self.currentCardIndex = lastSeenIndex
//            print(lastSeenIndex)
//        }
        
        // Frame que define o tamanho de todos os cards
        let frame = CGRect(origin: CGPoint.zero, size: cardViewContainer.frame.size)
        
        // Preenche lista de controllers de cards. Para cada um, cria uma borda no cardView que ele contém
        // e adiciona o controller na lista de cardControllers
        if let content = self.crisis.content {
            let cardControllerList = CardFactory(delegate: self).makeCards(contents: [content], frame: frame)
            for cardController in cardControllerList {
                if let cardController = cardController {
                    cardController.cardView.isSwipeable = false
                    self.createBorder(card: cardController.cardView)
                    self.cards.append(cardController)
                }
            }
        }
        
        // Inicializa o container de cardViews.
        self.cardViewContainer.dataSource = self
        self.view.bringSubview(toFront: cardViewContainer)
    }
    
    /// Create the border of the Card
    ///
    /// - Parameter card: The card to be applied the border
    func createBorder(card: CardView) {
        card.xContentView.layer.masksToBounds = true
        card.xContentView.layer.cornerRadius = 9
    }
    
    // Mostrar ou esconder botão de confirmação de resposta, se necessário
    func updateConfirmButton() {
        if let questionCard = self.cards[self.currentCardIndex] as? QuestionCardController {
            self.confirmButton.isHidden = false
            self.confirmButton.setTitle(questionCard.getActionTitle(), for: .normal)
            self.confirmButton.backgroundColor = questionCard.getActionColor()
            self.confirmButton.alpha = questionCard.getActionAlpha()
        } else {
            self.confirmButton.isHidden = true
        }
    }
    
    // Preparações necessárias para realização de segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueFinal"{
            if let next = segue.destination as? FinalLessonViewController {
                //TODO
                //next.lesson = self.lesson
                next.delegate = self.delegate
            }
        }
    }
    
    // Faz o botão de pular aparecer
    func displaySkipButton(animated: Bool) {
        self.skippable = true
        if animated {
            UIView.animate(withDuration: 1.0, animations: { [weak self] in
                self?.skipButton.tintColor = UIColor.white
                })
        }
        else {
            self.skipButton.tintColor = UIColor.white
        }
    }
    
    private func attributeToUser() {
        
        let userService = UserService()
        
        if self.activeCrisis == "C-1" {

            switch self.option {
            case 0:
                self.user.crisisCarBreak = CrisisResponse.friends
            case 1:
                self.user.crisisCarBreak = CrisisResponse.loan
            case 2:
                self.user.crisisCarBreak = CrisisResponse.dontPay
            case 3:
                self.user.crisisCarBreak = CrisisResponse.savings
            default:
                self.user.crisisCarBreak = CrisisResponse.dontPay
            }
        } else if self.activeCrisis == "C-2" {
            switch self.option {
            case 0:
                self.user.crisisHouseRent = CrisisResponse.friends
            case 1:
                self.user.crisisHouseRent = CrisisResponse.loan
            case 2:
                self.user.crisisHouseRent = CrisisResponse.dontPay
            case 3:
                self.user.crisisHouseRent = CrisisResponse.savings
            default:
                self.user.crisisHouseRent = CrisisResponse.dontPay
            }
        } else if self.activeCrisis == "C-3" {
            switch self.option {
            case 0:
                self.user.crisisRetire = CrisisRetire.govern
            case 1:
                self.user.crisisRetire = CrisisRetire.working
            case 2:
                self.user.crisisRetire = CrisisRetire.privatePension
            default:
                self.user.crisisRetire = CrisisRetire.savings
            }
        }
        
        userService.update(self.user, completion: { _,_ in 
            
        })
    }
    
    private func finishCrisis() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Ação do botão de pular
    @IBAction func skipButtonTapped(_ sender: Any) {
        self.attributeToUser()
        self.sendSkippedTimeToAnalytics()
        self.finishCrisis()
    }
    
    // Ação do botão de confirmação de resposta
    @IBAction func confirmButtonDidTapped(_ sender: Any) {
        if self.confirmButton.alpha == 1 {
            if let questionCard = self.cards[self.currentCardIndex] as? QuestionCardController {
                
                if !self.responded {
                    questionCard.startAction()
                    self.updateConfirmButton()
                    self.responded = true
                } else {
                    self.attributeToUser()
                    self.finishCrisis()
                }
            }
        }
    }
    
    func timeInCrisis() -> Int {
        let currentTime = Date()
        
        let timeSpended = DateInterval(start: self.initialTime, end: currentTime)
        
        return Int(timeSpended.duration)
    }
    
    func oldTimeSpended() -> Int {
        return UserDefaults.standard.integer(forKey: self.crisis.identification! + "-" + "oldTimeSpended")
    }
    
    func setTimeSpended(time: Int) {
        UserDefaults.standard.set(time, forKey: self.crisis.identification! + "-" + "oldTimeSpended")
    }
    
    func sendTimeToAnalytics() {
        
        let oldTimeSpended = self.oldTimeSpended()
        let timeInCrisis = self.timeInCrisis()
        
        AnalyticsService().send(time: timeInCrisis + oldTimeSpended, for: self.crisis)
        
        self.setTimeSpended(time: 0)
    }
    
    func sendSkippedTimeToAnalytics() {
        
        let oldTimeSpended = self.oldTimeSpended()
        let timeInCrisis = self.timeInCrisis()
        
        AnalyticsService().send(time: timeInCrisis + oldTimeSpended, forSkipped: self.crisis)
        
        self.setTimeSpended(time: 0)
    }
    
//    func finishCrisis() {
//
//        self.finished = true
//
//        self.sendTimeToAnalytics()
//
//        let crisisService = CrisisService()
//        crisisService.getCrisisFinished(for: self.crisis, completion: { (finished, error) in
//            if finished == false {
//                crisisService.setCrisisFinished(for: self.crisis)
//            }
//        })
//    }
}

extension CrisisViewController: QuestionCardControllerDelegate {
    func didAnswer(answer: Answer) {
        print(answer)
    }
    
    func didSelected(option: [Int]) {
        self.confirmButton.alpha = 1
        self.option = option[0]
    }
    
    func didDeselected() {
        self.confirmButton.alpha = 0.5
    }
}

extension CrisisViewController: SwipeableCardViewDataSource {
    func getCurrentCardIndex() -> Int {
        return self.currentCardIndex
    }
    
    func numberOfCards() -> Int {
        return cards.count
    }
    
    func card(forItemAtIndex index: Int) -> SwipeableCardView {
        return cards[index].cardView
    }
    
    func viewForEmptyCards() -> UIView? {
        return nil
        // return emptyView
    }
}

extension CrisisViewController: CardFactoryDelegate {
    func didFinishLoadingCards(error: Error?) {
        guard error == nil else {
            print("Error when finishing loading cards: \(error!.localizedDescription)")
            
            let alert = UIAlertController(title: "Ops!", message: "Ocorreu um erro ao acessar os dados do Banco Central. Verifique sua conexão com a internet.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        self.activityView.isHidden = true
    }
}
