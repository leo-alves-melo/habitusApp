//
//  ChatTableViewCell.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 03/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import ZCAnimatedLabel

protocol ChatTableViewCellDelegate: class {
    func finishedAnimating()
}

class ChatTableViewCell: UITableViewCell, ZCAnimatedLabelDelegate {
    
    @IBOutlet weak var messageLabel: ZCRevealLabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var nonAnimatedLabel: UILabel!

    @IBOutlet weak var debugText: UILabel!
    
    weak var delegate: ChatTableViewCellDelegate?

    var timer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if messageLabel != nil {
            messageLabel.onlyDrawDirtyArea = true
            messageLabel.layerBased = false
            messageLabel.animationDuration = 0.05
            messageLabel.animationDelay = 0.03
            messageLabel.delegate = self
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        
    }
    
    func startAnimatingText() {
        if messageLabel != nil {
            messageLabel.setNeedsDisplay()
            messageLabel.startAppearAnimation()
        }
    }
    
    func finishedAnimating() {
        self.delegate?.finishedAnimating()
    }
    
    func startTimer() {
        
        timer?.invalidate()
        
        var time = Double(0)
        
        if self.nonAnimatedLabel != nil {
            time = Double(self.nonAnimatedLabel.text?.count ?? 0)
        } else {
            time = Double(self.debugText.text?.count ?? 0)
        }

        time *= 0.033
        timer = Timer.scheduledTimer(withTimeInterval: time, repeats: false, block: { [weak self] timer in
                self?.finishedAnimating()
            })
    }
}
