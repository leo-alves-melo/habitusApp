//
//  SingleOptionViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 05/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class SingleOptionViewController: UIViewController {

    @IBOutlet weak var buttonUp: UIButton!
    @IBOutlet weak var buttonMedium: UIButton!
    @IBOutlet weak var buttonDown: UIButton!
    
    weak var delegate: ParentViewControllerDelegate?
    
    var privateOptions: [String] = []
    var optionsData: [String] {
        get {
            return privateOptions
        }
        set(value) {
            
            self.privateOptions = value
            
            self.buttonUp.isHidden = true
            self.buttonMedium.isHidden = true
            self.buttonDown.isHidden = true
            
            if self.privateOptions.count > 0 {
                self.buttonUp.isHidden = false
                self.buttonUp.setTitle(self.privateOptions[0], for: .normal)
            }
            if self.privateOptions.count > 1 {
                self.buttonMedium.isHidden = false
                self.buttonMedium.setTitle(self.privateOptions[1], for: .normal)
            }
            if self.privateOptions.count > 2 {
                self.buttonDown.isHidden = false
                self.buttonDown.setTitle(self.privateOptions[2], for: .normal)
            }
   
        }
    }

    func hideButtons() {
        self.buttonUp.isHidden = true
        self.buttonMedium.isHidden = true
        self.buttonDown.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func buttonLeftUpTapped(_ sender: Any) {
        self.delegate?.selectedString(string: self.buttonUp.title(for: .normal)!, index: 0)
    }
    
    @IBAction func buttonRightUpTapped(_ sender: Any) {
        self.delegate?.selectedString(string: self.buttonMedium.title(for: .normal)!, index: 1)
    }
    
    @IBAction func buttonLeftDownTapped(_ sender: Any) {
        self.delegate?.selectedString(string: self.buttonDown.title(for: .normal)!, index: 2)
    }
    
}
