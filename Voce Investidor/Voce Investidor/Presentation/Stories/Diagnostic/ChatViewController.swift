//
//  ChatViewController.swift
//  Voce Investidor
//
//  Created by Gustavo De Mello Crivelli on 03/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

//swiftlint:disable all

import UIKit
import ZCAnimatedLabel

enum StoryStatus {
    case success
    case failed
}

protocol ParentViewControllerDelegate: class {
    func selectedString(string: String, index: Int)
}

protocol StoryToShowDelegate: class {
    func storyEnded(status: StoryStatus)
}

class ChatViewController: CustomizedNavBarViewController {
    
    @IBOutlet var activityView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var finishViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var finishView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var loginButton: UIBarButtonItem!
    
    var callbackStoryToShow: ((_ status: StoryStatus)-> Void)!
    
    var parentVC: UIViewController?
    
    var keyboardIsOpen: Bool = false
    var messageID: String = "diag_1"
    var presentedModal = false
    
    var messagesProvider: MessagesProvider?
    
    var shouldReceiveUserInformation = false
    var userInput: UserInput?

    var user: User!
    var messages: [Message] = []
    
    var pendingOperations = PendingOperations()
    
    override func viewDidLoad() {

        super.viewDidLoad()
        let userService = UserService()

        userService.getLogged { (userLogged, error) in

            if let _ = error {
                self.user = User.instance
                return
            }

            self.user = userLogged
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        self.textViewBottomConstraint.constant = 0

        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // If you need to skip the initial chat, execute the line below
        // so there is some user info and the app doesn't crash
        //self.setupDebugProfile()
        
        self.loadMessages()
        self.hideSendButton()
        
        if self.messageID != "diag_1" {
             self.hideLoginButton()
        }
    }
    
    func hideLoginButton() {
        self.loginButton.isEnabled = false
        self.loginButton.tintColor = UIColor.clear
    }
    
    func showLoginButton() {
        self.loginButton.isEnabled = true
        self.loginButton.tintColor = UIColor.blue
    }
    
    func hideSendButton() {
        self.sendButton.alpha = 0.5
        self.sendButton.isEnabled = false
    }
    
    func showSendButton() {
        self.sendButton.alpha = 1
        self.sendButton.isEnabled = true
    }
    
    func loadMessages() {
        
        self.messagesProvider = MessagesProvider(for: self.messageID, delegate: self)
        
        self.messagesProvider?.getMessages(completion: { (error) in
            
            guard error == nil else {
                print("Error when calling messageProvider in Diagnostic: \(error!.localizedDescription)")
                return
            }
            
            self.messagesProvider?.nextMessage()
        })
    }
    
    func setupDebugProfile() {
        UserStatic.name = "DEBUG"
        UserStatic.userEmail = "DEBUG"
        UserStatic.investment = ["DEBUG"]
        UserStatic.userAge = 33
        UserStatic.userControl = "DEBUG"
        UserStatic.userDebts = "Sempre tem alguma conta atrasada"
        UserStatic.userMonthlyMoney = "DEBUG"
        UserStatic.userSalaryCompromised = "DEBUG"
        UserStatic.userHasCar = "DEBUG"
        UserStatic.userHasHealthPlan = "DEBUG"
    }
    
    func passUserStaticToUser() {
        self.user.name = UserStatic.name!
        //self.user.email = UserStatic.userEmail!
        
        switch UserStatic.investment![0] {
        case "Poupança":
            user.hasPreviousInvestments = InvestmentsType.poupanca
            
        case "Fundos de investimentos":
            user.hasPreviousInvestments = InvestmentsType.funds
            
        default:
            user.hasPreviousInvestments = InvestmentsType.nothing
        }
        
        let years = UserStatic.userAge
        var birthDateComponents = DateComponents()
        
        birthDateComponents.year = Calendar.current.component(.year, from: Date()) - years!
        birthDateComponents.month = 1
        birthDateComponents.day = 1
        
        user.birthDate = Calendar.current.date(from: birthDateComponents)
        
        switch UserStatic.userControl {
        case "Do meu orçamento, nada escapa":
            user.financialControl = FinancialControl.best
            
        case "Registro meus gastos quando lembro":
            user.financialControl = FinancialControl.minimum
            
        default:
            user.financialControl = FinancialControl.nothing
        }
        
        switch UserStatic.userDebts {
        case "Sempre tem alguma conta atrasada":
            user.howPayBills = BillsPayment.overdue
            
        case "Pago em dia, com muito esforço":
            user.howPayBills = BillsPayment.allOk
            
        default:
            user.howPayBills = BillsPayment.okButHard
        }
        
        switch UserStatic.userMonthlyMoney {
        case "Até R$1.000,00 por mês":
            user.receipts = 1000
            
        case "De R$1.000,00 a R$6.000,00 por mês":
            user.receipts = 6000
            
        case "Acima de R$6.000,00 por mês":
            user.receipts = 6001
            
        default:
            user.receipts = 0
        }  
        
        switch UserStatic.userSalaryCompromised {
        case "Até 10% do meu salário":
            user.fixedExpensesMonthly = FixedExpenses.lessThan10
            
        case "De 10 até 50% do meu salário":
            user.fixedExpensesMonthly = FixedExpenses.lessThan50
            
        case "Mais de 50% do meu salário":
            user.fixedExpensesMonthly = FixedExpenses.greaterThan50
            
        default:
            user.fixedExpensesMonthly = FixedExpenses.unknow
        }
        
        user.hasCar = (UserStatic.userHasCar == "Sim")
        user.hasHealthInsurance = (UserStatic.userHasHealthPlan == "Sim")
    }

    @IBAction func startButtonTapped(_ sender: Any) {

        if let startButton = sender as? UIButton {
            startButton.isEnabled = false
        }
        
        self.view.bringSubview(toFront: self.activityView)

        let userService = UserService()

        let diagnostic = messageID
        
        if diagnostic == "migration" {
            if self.user.hashPassword == nil {
                UserDefaults.standard.set(true, forKey: "didOfferPasswordCreation")
                UserDAO.disallowiCloud()
            }
        }
        
        if diagnostic == "diag_1" {
            
            if self.user.hashPassword == nil {
                UserDAO.disallowiCloud()
            } else {
                UserDAO.allowiCloud()
            }
            
            if !self.user.updated {
                self.passUserStaticToUser()
            }
        }
        
        if diagnostic == "diag_2" {
            
            self.user.payHouseRent = UserStatic.userPayRent == "Sim"
            self.user.payCarFinancing = UserStatic.userAutomotivePayment == "Sim"
            self.user.payEquipament = UserStatic.userEletronicPayment == "Sim"
            self.user.payUniversity = UserStatic.userCoursePayment == "Sim"
        }
        
        if diagnostic == "diag_3" {
            
            switch UserStatic.userChangeCar {
            case "Sim, em até 1 ano":
                user.wantToBuyCar = PeriodShort.lessThanOneYear
                
            case "Sim, em até 2 anos ou mais":
                user.wantToBuyCar = PeriodShort.lessThanTwoYears
                
            default:
                user.wantToBuyCar = PeriodShort.dont
            }
            
            switch UserStatic.userBuyBuilding {
            case "Sim, em até 1 ano":
                user.wantToBuyHouse = PeriodMedium.lessThanOneYear
                
            case "Sim, em até 5 anos ou mais":
                user.wantToBuyHouse = PeriodMedium.lessThanFiveYears
                
            default:
                user.wantToBuyHouse = PeriodMedium.dont
            }
            
            switch UserStatic.userRetirement {
            case "Menos de 5 anos":
                self.user.wantToRetire = PeriodLong.lessThanFiveYears
                
            case "De 5 a 15 anos":
                self.user.wantToRetire = PeriodLong.lessThanFifteenYears
                
            case "Mais de 15 anos":
                self.user.wantToRetire = PeriodLong.moreThanFifteenYears
                
            default:
                self.user.wantToRetire = PeriodLong.dont
            }
        }

        userService.update(self.user) { [weak self] (userSaved, error) in
            if error == nil {
                userService.login(user: userSaved!, completion: { [weak self] (_) in
                    OperationQueue.main.addOperation {
                        UserStatic.userObject = userSaved
                        self?.activityView.isHidden = true
                        if self?.messageID == "diag_1" && userSaved?.howPayBills == BillsPayment.overdue {
                            self?.dismiss(animated: false, completion: {
                                let storyboard = UIStoryboard(name: "Debt", bundle: nil)
                                let debtVC = storyboard.instantiateViewController(withIdentifier: "DebtViewController")
                                self?.parentVC?.present(debtVC, animated: true, completion: nil)
                                //self?.performSegue(withIdentifier: "showDebtSegue", sender: nil)
                            })
                        } else {
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }
                })
                return
            }
            
            if self?.messageID == "diag_1" {
                UserDAO.disallowiCloud()
            }
            
            OperationQueue.main.addOperation {
                UserStatic.userObject = userSaved
                self?.activityView.isHidden = true
                if self?.messageID == "diag_1" && userSaved?.howPayBills == BillsPayment.overdue {
                    self?.dismiss(animated: false, completion: {
                        let storyboard = UIStoryboard(name: "Debt", bundle: nil)
                        let debtVC = storyboard.instantiateViewController(withIdentifier: "DebtViewController")
                        self?.parentVC?.present(debtVC, animated: true, completion: nil)
                        //self?.performSegue(withIdentifier: "showDebtSegue", sender: nil)
                    })
                } else {
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        self.keyboardIsOpen = true

        if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue) {
            let keyboardSize = keyboard.cgRectValue

            if self.textViewBottomConstraint.constant == CGFloat(0) {
                
                if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                    if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                        
                        self.textViewBottomConstraint.constant = keyboardSize.height
                        
                        UIView.animate(withDuration: duration.doubleValue,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                       animations: {
                            self.view.layoutIfNeeded()
                                        
                                        if self.tableView.contentSize.height > self.tableView.bounds.size.height {
                                            let bottomOffset = CGPoint(x: 0,
                                                                       y: self.tableView.contentSize.height - self.tableView.bounds.size.height)
                                            self.tableView.setContentOffset(bottomOffset, animated: false)
                                        }
                            
                        }, completion: nil)

                    }
                    
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        self.keyboardIsOpen = false
        
        if let _ = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.textViewBottomConstraint.constant > CGFloat(0) {
                if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                    if let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                        
                        self.textViewBottomConstraint.constant = CGFloat(0)
                        
                        UIView.animate(withDuration: duration.doubleValue, delay: 0.0,
                                       options: UIViewAnimationOptions(rawValue: UInt(curve.intValue)),
                                       animations: {
                            self.view.layoutIfNeeded()
                            
                        }, completion: nil)

                    }
                }
            }
        }
    }
    
    private func attributeToUser(field: UserInputAttributeTo, value: String) {
        switch field {
        case .userName:
            UserStatic.name = value.trimmingCharacters(in: .whitespacesAndNewlines)
        case .userInvestment:
            if UserStatic.investment == nil {
                UserStatic.investment = [value]
            } else {
                UserStatic.investment?.append(value)
            }
        case .userAge:
            UserStatic.userAge = Int(value)

        case .userControl:
            UserStatic.userControl = value

        case .userDebts:
            UserStatic.userDebts = value

        case .userEmail:
            UserStatic.userEmail = value
        case .userHasCar:
            UserStatic.userHasCar = value
        case .userHasHealthPlan:
            UserStatic.userHasHealthPlan = value
        case .userMonthlyMoney:
            UserStatic.userMonthlyMoney = value
        case .userLastAnswer:
            UserStatic.userLastAnswer = value
        case .userSalaryCompromised:
            UserStatic.userSalaryCompromised = value
        case .userPayRent:
            UserStatic.userPayRent = value
        case .userAutomotivePayment:
            UserStatic.userAutomotivePayment = value
        case .userEletronicPayment:
            UserStatic.userEletronicPayment = value
        case .userCoursePayment:
            UserStatic.userCoursePayment = value
        case .userChangeCar:
            UserStatic.userChangeCar = value
        case .userBuyBuilding:
            UserStatic.userBuyBuilding = value
        case .userRetirement:
            UserStatic.userRetirement = value
        case .userChildren:
            UserStatic.userChildren = value
        case .userAccept:
            UserStatic.userAccept = value
        }
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        guard let text = self.textField.text, text != "" else {
            return
        }
        
        self.hideSendButton()

        if self.keyboardIsOpen {
            self.view.endEditing(true)
        } else {
            self.hideCustomKeyboard()
        }
        
        self.textField.text = ""
        
        self.shouldReceiveUserInformation = false
        
        // User input validation
        if let userInput = self.userInput {
            if let attributeTo = userInput.attributeTo {
                
                // Validate email
                if attributeTo == .userEmail {
                    if !Validator.validate(email: text) {
                        self.attributeToUser(field: attributeTo, value: text)
                        self.messagesProvider?.wrongUserDataReceived()
                        return
                    }
                }
                
                // Validate age
                if attributeTo == .userAge {
                    if !Validator.validate(age: text) {
                        self.attributeToUser(field: attributeTo, value: text)
                        self.messagesProvider?.wrongUserDataReceived()
                        return
                    }
                }
                
                // Validation is fine, save info
                self.attributeToUser(field: attributeTo, value: text)
            }
        }
        
        self.messagesProvider?.userDataReceived()
    }
    
    func hideCustomKeyboard() {
        self.textViewBottomConstraint.constant = CGFloat(0)

        if let cont = self.childViewControllers[0] as? SingleOptionViewController {
            cont.hideButtons();
        }
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       options: UIViewAnimationOptions(rawValue: UInt(7)),
                       animations: {
                        self.view.layoutIfNeeded()
                        
        })
    }
    
    func defineCustomKeyboardSize() {
        
        if self.childViewControllers[0] is SingleOptionViewController {
            
            var height = 67
            
            if let optionsData = self.userInput?.optionsData {
                if optionsData.count > 1 {
                    height += 57
                }
                if optionsData.count > 2 {
                    height += 57
                }
            }
            
            self.textViewBottomConstraint.constant = CGFloat(height)
            
        } else {
            self.textViewBottomConstraint.constant = self.view.frame.height/3
        }
    }
    
    func showCustomKeyboard() {
        self.defineCustomKeyboardSize()
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       options: UIViewAnimationOptions(rawValue: UInt(7)),
                       animations: {
                        self.view.layoutIfNeeded()
                        let bottomOffset = CGPoint(x: 0,
                                                   y: self.tableView.contentSize.height - self.tableView.bounds.size.height)
                        
                        if self.tableView.contentSize.height > self.tableView.bounds.size.height {
                            self.tableView.setContentOffset(bottomOffset, animated: false)
                        }
        })
    }
    
    private lazy var dateViewController: DateViewController? = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Diagnostic", bundle: Bundle.main)
        
        // Instantiate View Controller
        if var viewController = storyboard.instantiateViewController(
            withIdentifier: "DateViewController") as? DateViewController {
            // Add View Controller as Child View Controller
            self.add(inContainerView: viewController)
            
            viewController.delegate = self
            
            return viewController
        }
        
        return nil

    }()
    
    private lazy var singleOptionViewController: SingleOptionViewController? = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Diagnostic", bundle: Bundle.main)
        
        // Instantiate View Controller
        if var viewController = storyboard.instantiateViewController(
            withIdentifier: "SingleOptionViewController") as? SingleOptionViewController {
            // Add View Controller as Child View Controller
            self.add(inContainerView: viewController)
            
            viewController.delegate = self
            
            return viewController
        }
        
        return nil
        
    }()
    
    private func add(inContainerView viewController: UIViewController) {
        // Add Child Vizew Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        self.containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
        
        self.showCustomKeyboard()
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
    private func showStartButton() {
        self.finishViewHeightConstraint.constant = self.textView.frame.height
        
        UIView.animate(withDuration: 0.3, delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        self.view.layoutIfNeeded()
                        
        }, completion: nil)
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let login = segue.destination as? LoginViewController {
            if segue.identifier == "login" {
                self.callbackStoryToShow = callbackStoryEndedDefalt
                login.delegate = self
            } else if segue.identifier == "SegueLoginButton"{
                self.callbackStoryToShow = callbackStoryEndedFromLoginButton
                login.delegate = self
            }
        }
        if let signUp = segue.destination as? SignUpViewController {
            self.callbackStoryToShow = callbackStoryEndedDefalt
            
            if self.messageID != "migration" {
                self.passUserStaticToUser()
            }
            
            signUp.user = self.user
            signUp.delegate = self
        }
    }
}

extension ChatViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count //self.messagesProvider?.visibleMessagesList.count ?? 0
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let messagesProvider = self.messagesProvider else {
            
            return UITableViewCell()
        }
        
        let message = messages[indexPath.row] //messagesProvider.visibleMessagesList[indexPath.row]

        let cellName = message.type.asString()

        let cell = tableView.dequeueReusableCell(withIdentifier: cellName,
                                                 for: indexPath) as! ChatTableViewCell
        let textToShow = message.text

        cell.delegate = self
        cell.isUserInteractionEnabled = false
        
        if message.type == .system {
            cell.debugText.text = textToShow
        } else {
            cell.nonAnimatedLabel.text = textToShow

            if let name = UserStatic.name {
                let substring = name.prefix(1)
                cell.userLabel.text = String(substring)
            } else if self.user.name != "" {
                let substring = self.user.name.prefix(1)
                cell.userLabel.text = String(substring)
            }
        }
        
        if !message.displayed {
            message.displayed = true
            cell.startTimer()
        }
        
        return cell
    }
}

extension ChatViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // TODO: Cancel all timers and animations
    }
}

extension ChatViewController: ParentViewControllerDelegate {
    func selectedString(string: String, index: Int) {
        self.textField.text = string
        self.messagesProvider?.selectedChosenIndex = index
    }
 
}

extension ChatViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        var shouldBeginEditing = false
        
        self.showSendButton()

        if self.shouldReceiveUserInformation {
            if let typeOfInformation = self.userInput?.type {
                
                switch typeOfInformation {
                case .buttonsSingleOption:
                    if let optionsData = self.userInput?.optionsData {
                        self.singleOptionViewController?.optionsData = optionsData
                        self.add(inContainerView: self.singleOptionViewController!)
                    }
                case .datePicker:
                    self.add(inContainerView: self.dateViewController!)
                case .keyboardName:
                    shouldBeginEditing = true
                    textField.keyboardType = .namePhonePad
                    textField.autocapitalizationType = .words
                case .keyboardNumber:
                    shouldBeginEditing = true
                    textField.keyboardType = .numberPad
                    textField.autocorrectionType = .no
                case .keyboardEmail:
                    shouldBeginEditing = true
                    textField.keyboardType = .emailAddress
                }
            }
        }
        
        return shouldBeginEditing
    }
}

// Delegate for message animations.
extension ChatViewController: ChatTableViewCellDelegate {
    
    // Called whenever a message finishes animating. Useful for displaying new messages.
    func finishedAnimating() {

        self.messagesProvider?.nextMessage()
    }
}

extension ChatViewController: MessagesProviderDelegate {
    
    func shouldShow(story: String) {
        self.performSegue(withIdentifier: story, sender: nil)
    }
    
    func finishMessaging() {
        self.showStartButton()
    }
    
    func shouldReceiveInformation(with userInput: UserInput) {
        self.shouldReceiveUserInformation = true
        self.userInput = userInput
        self.textField.becomeFirstResponder()
    }
    
    func shouldUpdateMessages(newMessage: Message, index: IndexPath) {
        
        self.messages.append(newMessage)
        
        // Insert new messages, updating only new ones
        self.tableView.beginUpdates()

        self.tableView.insertRows(at: [index], with: .automatic)
        self.tableView.endUpdates()

        if self.tableView.contentSize.height > self.tableView.bounds.size.height {
            self.tableView.scrollToRow(at: index, at: .bottom, animated: true)
        }
    }
}

extension ChatViewController: StoryToShowDelegate {
    
    func storyEnded(status: StoryStatus) {
        
        if let callback = self.callbackStoryToShow {
            callback(status)
        }
    }
    
    func callbackStoryEndedDefalt(status: StoryStatus) {
        
        self.messagesProvider?.storyReturn(status: status)
        self.messagesProvider?.nextMessage()
    }
    
    func callbackStoryEndedFromLoginButton(status: StoryStatus) {
        if status == StoryStatus.success {
            self.performSegue(withIdentifier: "FinishChat", sender: nil)
        }
    }
}
