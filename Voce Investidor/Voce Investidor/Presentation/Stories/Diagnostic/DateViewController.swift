//
//  DateViewController.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 04/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class DateViewController: UIViewController {

    @IBOutlet weak var pickerView: UIPickerView!
    weak var delegate: ParentViewControllerDelegate?
    let numberOfMonths = 60
    
    var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
                  "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    
    var pickerData: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for index in 0..<self.numberOfMonths {
            self.pickerData.append("\(months[11 - index % 12]) \(2018 - Int(index/12))")
        }
        
    }

    @IBAction func selectDate(_ sender: Any) {
        self.delegate?.selectedString(string: self.pickerData[self.pickerView.selectedRow(inComponent: 0)], index: 0)
    }
    
    @IBAction func backPicker(_ sender: Any) {
        self.pickerView.selectRow(self.pickerView.selectedRow(inComponent: 0) - 1, inComponent: 0, animated: true)
    }
    
    @IBAction func nextPicker(_ sender: Any) {
        self.pickerView.selectRow(self.pickerView.selectedRow(inComponent: 0) + 1, inComponent: 0, animated: true)
    }
    
}

extension DateViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 120
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
    
    
}
