//
//  AuthenticationErrors.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 14/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum AuthenticationError: Error {
    case emailNotFound
    case iCloudNotConnected
    case wrongPassword
    case internetNotConnected
    case userDoesNotHavePassword
    case userDoesNotExist
}
