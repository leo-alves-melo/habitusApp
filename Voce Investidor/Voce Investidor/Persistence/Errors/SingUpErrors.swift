//
//  SingUpErrors.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 21/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum SignUpErrors: Error {
    case emailAlreadyExists
    case internetNotConnected
    case other
}
