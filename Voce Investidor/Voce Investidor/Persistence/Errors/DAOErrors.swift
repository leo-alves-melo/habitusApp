//
//  DAOErrors.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 29/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

enum DAOErrors: Error {
    case needIdentifier
    case notFound
}
