//
//  InvestmentDAO.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 29/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import CloudKit

/// DAO to Retrieve, Create and Update Investments
class InvestmentDAO {
    
    /// Table to store the user details in Public database
    private let type = "Investment"
    
    /// Retrieve all investments from a specific user
    ///
    /// - Parameters:
    ///   - goal: The goal to receive investments
    ///   - completion: the function to be called when retrieving investments or error
    func investment(from goal: Goal, completion: @escaping ([Investment]?, Error?) -> Void) {

        let goalRecord = CKRecord(recordType: "Goal", recordID: .init(recordName: goal.identification!))
        let goalReference = CKReference(record: goalRecord, action: .none)
        let predicate = NSPredicate(format: "goal = %@", goalReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)

        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in

            guard let records = records, error == nil else {
                completion(nil, error)
                return
            }

            var investments: [Investment] = []

            for record in records {
                investments.append(self.investment(from: record))
            }

            completion(investments, nil)
        }
    }
    
    /// Create a new investment in the Database
    ///
    /// - Parameters:
    ///   - investment: A investment to be created
    ///   - completion: The completion to be called
    func create(investment: Investment, completion: @escaping (Error?) -> Void) {
        
        let record = self.record(from: investment)
        
        Database.getDatabase().save(record) { (_, error) in
            completion(error)
        }
        
    }
    
    /// Update the investment in the Database
    ///
    /// - Parameters:
    ///   - investment: A investment to be updated
    ///   - completion: The completion to be called
    func update(investment: Investment, completion: @escaping (Error?) -> Void) {
        
        guard let investmentID = investment.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let investmentRecord = CKRecord(recordType: self.type, recordID: .init(recordName: investmentID))
        let investmentReference = CKReference(record: investmentRecord, action: .none)
        let predicate = NSPredicate(format: "%K == %@", "recordID", investmentReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, let investmentRecord = records.first, error == nil else {
                completion(error)
                return
            }
            
            let newRecord = self.recordUpdated(investment: investment, record: investmentRecord)
            
            Database.getDatabase().save(newRecord) { (_, error) in
                completion(error)
            }
        }
    }
    
    /// Delete the investment in the Database
    ///
    /// - Parameters:
    ///   - goal: A investment to be deleted
    ///   - completion: The completion to be called
    func delete(investment: Investment, completion: @escaping (Error?) -> Void) {
        
        guard let investmentID = investment.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let recordID = CKRecordID(recordName: investmentID)
        
        Database.getDatabase().delete(withRecordID: recordID) { (_, error) in
            completion(error)
        }
    }
    
    /// Create a record given a investment with recordID to update an existing element in Database
    ///
    /// - Parameter investment: Investment to be reference to record
    /// - Returns: The record created
    private func recordUpdated(investment: Investment, record: CKRecord) -> CKRecord {
        
        if let goal = investment.goal {
            let goalRecord = CKRecord(recordType: "Goal", recordID: .init(recordName: goal))
            let goalReference = CKReference(record: goalRecord, action: .none)
            
            record["goal"] = goalReference
        }
        
        record["name"] = investment.name
        record["date"] = investment.date
        record["initialAmount"] = investment.initialAmount
        record["note"] = investment.note

        if let broker = investment.broker {
            if broker.broker == .other {
                record["broker"] = investment.broker?.name
            } else {
                
                record["broker"] = investment.broker?.broker.rawValue
            }
        }

        record["investmentPaper"] = investment.investmentPaperID
        
        record["fraction"] = investment.fraction

        return record
    }

    /// Create a investment given a record
    ///
    /// - Parameter record: Record got from database
    /// - Returns: The investment created from the record
    private func investment(from record: CKRecord) -> Investment {
        var investment = Investment()

        if let name = record["name"] as? String {
            investment.name = name
        }
        if let date = record["date"] as? Date {
            investment.date = date
        }
        if let initialAmount = record["initialAmount"] as? Double {
            investment.initialAmount = initialAmount
        }
        if let note = record["note"] as? String {

            investment.note = note
        }
        if let goal = record["goal"] as? CKReference {
            investment.goal = goal.recordID.recordName
        }
        if let broker = record["broker"] as? String {
            let brokerEnum = BrokerEnum(value: broker)
            investment.broker = BrokerAccount(brokerAccount: brokerEnum)
            if brokerEnum == .other {
                investment.broker?.name = broker
            }
            
        }
        
        investment.fraction = record["fraction"] as? Double
        
        investment.investmentPaperID = record["investmentPaper"] as? String
        
        investment.identification = record.recordID.recordName

        return investment
    }
    
    /// Create a record given a investment
    ///
    /// - Parameter investment: investment to be reference to record
    /// - Returns: The record created
    private func record(from investment: Investment) -> CKRecord {
        
        let record = CKRecord(recordType: self.type)
        
        if let goal = investment.goal {
            let goalRecord = CKRecord(recordType: "Goal", recordID: .init(recordName: goal))
            let goalReference = CKReference(record: goalRecord, action: .none)
            
            record["goal"] = goalReference
        }

        record["name"] = investment.name
        record["date"] = investment.date
        record["initialAmount"] = investment.initialAmount
        record["note"] = investment.note
        
        if let broker = investment.broker {
            if broker.broker == .other {
                record["broker"] = investment.broker?.name
                
            } else {
                record["broker"] = investment.broker?.broker.rawValue
            }
        }
        
        record["investmentPaper"] = investment.investmentPaperID
        
        record["fraction"] = investment.fraction
    
        return record
    }
}
