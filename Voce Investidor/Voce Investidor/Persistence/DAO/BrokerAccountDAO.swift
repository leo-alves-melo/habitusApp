//
//  BrokerAccountDAO.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import CloudKit

/// DAO to Retrieve, Create and Update BrokersAccounts
class BrokerAccountDAO {
    
    /// Table to store the user details in Public database
    private let type = "BrokerAccount"
    
    /// Retrieve all brokersAccount from a specific user
    ///
    /// - Parameters:
    ///   - user: The user to receive Brokers
    ///   - completion: the function to be called when retrieving brokerAccounts or error
    func brokerAccount(from user: User, completion: @escaping ([BrokerAccount]?, Error?) -> Void) {
        
        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: user.email))
        let userReference = CKReference(record: userRecord, action: .none)
        let predicate = NSPredicate(format: "user = %@", userReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, error == nil else {
                completion(nil, DAOErrors.notFound)
                return
            }
            
            var brokerAccounts: [BrokerAccount] = []
            
            for record in records {
                brokerAccounts.append(self.brokerAccount(from: record))
            }
            
            completion(brokerAccounts, nil)
        }
    }
    
    /// Create a new brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - brokerAccount: A brokerAccount to be created
    ///   - completion: The completion to be called
    func create(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        
        let record = self.record(from: brokerAccount)
        
        Database.getDatabase().save(record) { (_, error) in
            completion(error)
        }
        
    }
    
    /// Update the brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - brokerAccount: A brokerAccount to be updated
    ///   - completion: The completion to be called
    func update(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        
        guard let brokerAccountID = brokerAccount.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let brokerAccountRecord = CKRecord(recordType: self.type, recordID: .init(recordName: brokerAccountID))
        let brokerAccountReference = CKReference(record: brokerAccountRecord, action: .none)
        let predicate = NSPredicate(format: "%K == %@", "recordID", brokerAccountReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, let brokerAccountRecord = records.first, error == nil else {
                completion(error)
                return
            }
            
            let newRecord = self.recordUpdated(brokerAccount: brokerAccount, record: brokerAccountRecord)
            
            Database.getDatabase().save(newRecord) { (_, error) in
                completion(error)
            }
        }
    }
    
    /// Delete the brokerAccount in the Database
    ///
    /// - Parameters:
    ///   - brokerAccount: A brokerAccount to be deleted
    ///   - completion: The completion to be called
    func delete(brokerAccount: BrokerAccount, completion: @escaping (Error?) -> Void) {
        
        guard let brokerAccountID = brokerAccount.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let recordID = CKRecordID(recordName: brokerAccountID)
        
        Database.getDatabase().delete(withRecordID: recordID) { (_, error) in
            completion(error)
        }
    }
    
    /// Create a record given a brokerAccount with recordID to update an existing element in Database
    ///
    /// - Parameter brokerAccount: BrokerAccount to be reference to record
    /// - Returns: The record created
    private func recordUpdated(brokerAccount: BrokerAccount, record: CKRecord) -> CKRecord {
        
        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: User.instance.email))
        let userReference = CKReference(record: userRecord, action: .none)
        
        record["user"] = userReference
        
        if brokerAccount.broker != .other {
            record["broker"] = brokerAccount.broker.rawValue
        } else {
            record["broker"] = brokerAccount.name
        }
        
        record["openDate"] = brokerAccount.openDate
        
        return record
    }
    
    /// Create a record given a brokerAccount
    ///
    /// - Parameter brokerAccount: BrokerAccount to be reference to record
    /// - Returns: The record created
    private func record(from brokerAccount: BrokerAccount) -> CKRecord {
        
        let record = CKRecord(recordType: self.type)
        
        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: User.instance.email))
        let userReference = CKReference(record: userRecord, action: .none)
        
        record["user"] = userReference
        
        if brokerAccount.broker != .other {
            record["broker"] = brokerAccount.broker.rawValue
        } else {
            record["broker"] = brokerAccount.name
        }
        
        record["openDate"] = brokerAccount.openDate
        
        return record
    }
    
    /// Create a broker account given a record
    ///
    /// - Parameter record: Record got from database
    /// - Returns: The broker account created from the record
    private func brokerAccount(from record: CKRecord) -> BrokerAccount {
        
        var brokerAccount = BrokerAccount(brokerAccount: BrokerEnum(value: record["broker"] as? String))
        
        if brokerAccount.broker == .other {
            brokerAccount.name = record["broker"] as? String
        }
        
        brokerAccount.identification = record.recordID.recordName
        
        if let openDate = record["openDate"] as? Date {
            brokerAccount.openDate = openDate
        }
        
        return brokerAccount
    }
}
