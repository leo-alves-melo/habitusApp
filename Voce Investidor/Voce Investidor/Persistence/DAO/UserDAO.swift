//
//  UserEntity.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 09/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import CloudKit

// Database layer
//swiftlint:disable type_body_length
class UserDAO {

    /// Table to store the user details in Public database
    let type = "Profile"
    
    public static func allowiCloud() {
        UserDefaults.standard.set(true, forKey: "AllowiCloud")
    }
    
    public static func disallowiCloud() {
        UserDefaults.standard.set(false, forKey: "AllowiCloud")
    }
    
    public static func isiCloudAllowed() -> Bool {
        return UserDefaults.standard.bool(forKey: "AllowiCloud")
    }

    public func update(user: User, completion: @escaping (_ user: User?, _ error: Error?) -> Void) {
        
        if UserDAO.isiCloudAllowed() {
            //CHECK IF exists
            self.fetchRecordByEmail(email: user.email) { (recordFetched, errorOnFetch) in
                
                guard errorOnFetch == nil else {
                    
                    UserStatic.userObject = user
                    
                    let record = self.userToRecord(user: user, recordIn: recordFetched)
                    
                    let defaults = UserDefaults.standard
                    let recordDefaults = record
                    recordDefaults["email"] = user.email.lowercased() as NSString
                    defaults.set(recordDefaults.dictionaryWithValues(forKeys: recordDefaults.allKeys()), forKey: "Profile")
                    
                    //doesn't exist.
                    completion(nil, errorOnFetch)
                    return
                    
                }
                
                var record: CKRecord!
                
                //exists. Fill record to update
                record = self.userToRecord(user: user, recordIn: recordFetched)
                
                let defaults = UserDefaults.standard
                let recordDefaults = record!
                recordDefaults["email"] = user.email.lowercased() as NSString
                defaults.set(recordDefaults.dictionaryWithValues(forKeys: recordDefaults.allKeys()), forKey: "Profile")
                
                if UserDAO.isiCloudAllowed() {
                    //update or create
                    Database.getDatabase().save(record) { (savedRecord, error) in
                        
                        if let error = error {
                            UserStatic.userObject = user
                            
                            completion(nil, error)
                            return
                            
                        }
                        let user = self.recordToUser(record: savedRecord!)
                        UserStatic.userObject = user
                        
                        completion(user, nil)
                    }
                } else {
                    UserStatic.userObject = user
                    completion(user, nil)
                }
            }
        } else {
            let record = self.userToRecord(user: user, recordIn: CKRecord(recordType: "Profile"))
            
            let defaults = UserDefaults.standard
            let recordDefaults = record
            recordDefaults["email"] = user.email.lowercased() as NSString
            defaults.set(recordDefaults.dictionaryWithValues(forKeys: recordDefaults.allKeys()), forKey: "Profile")
            
            //doesn't exist.
            completion(user, nil)
        }

    }
    
    /// Create the user
    ///
    /// - Parameters:
    ///   - user: User object to save
    ///   - completion: Result callback
    public func create(user: User, completion: @escaping (_ user: User?, _ error: SignUpErrors?) -> Void) {

        if UserDAO.isiCloudAllowed() {
            //CHECK IF exists
            self.fetchRecordByEmail(email: user.email) { (recordFetched, errorOnFetch) in
                
                var record: CKRecord!
                
                //This user must not exist
                if errorOnFetch == nil {
                    
                    //does exist
                    if let recordFetched = recordFetched {
                        if recordFetched["hashPassword"] != nil {
                            return completion(nil, SignUpErrors.emailAlreadyExists)
                        }
                    }
                }
                
                //Not exists. Fill record to update
                record = self.userToRecord(user: user, recordIn: recordFetched)
                
                let defaults = UserDefaults.standard
                let recordDefaults = record!
                recordDefaults["email"] = user.email.lowercased() as NSString
                defaults.set(recordDefaults.dictionaryWithValues(forKeys: recordDefaults.allKeys()), forKey: "Profile")
                
                if UserDAO.isiCloudAllowed() {
                    //update or create
                    Database.getDatabase().save(record) { (savedRecord, error) in
                        
                        if let _ = error {
                            UserStatic.userObject = user
                            
                            completion(nil, SignUpErrors.internetNotConnected)
                            return
                        }
                        
                        let user = self.recordToUser(record: savedRecord!)
                        UserStatic.userObject = user
                        
                        completion(user, nil)
                    }
                } else {
                    UserStatic.userObject = user
                    completion(user, nil)
                }
            }
        } else {
            let record = self.userToRecord(user: user, recordIn: CKRecord(recordType: "Profile"))
            
            let defaults = UserDefaults.standard
            let recordDefaults = record
            recordDefaults["email"] = user.email.lowercased() as NSString
            defaults.set(recordDefaults.dictionaryWithValues(forKeys: recordDefaults.allKeys()), forKey: "Profile")
            
            //doesn't exist.
            completion(user, nil)
        }
        
    }

    /// Fetch a User from database
    ///
    /// - Parameters:
    ///   - email: ID for the record
    ///   - completion: Result callback
    public func fetchUserByEmail(email: String, _ completion: @escaping (_ user: User?,_ error: AuthenticationError?) -> Void) {
        
        let email = email.lowercased()
        
        if UserDAO.isiCloudAllowed() {
            let recordId = CKRecordID(recordName: email)
            
            Database.getDatabase().fetch(withRecordID: recordId) { (record, error) in
                
                if error != nil {
                    
                    let defaults = UserDefaults.standard
                    
                    let recordReaded = defaults.dictionary(forKey: "Profile")
                    
                    if recordReaded != nil {
                        
                        let userReaded = self.dictToUser(record: recordReaded!)
                        completion(userReaded, AuthenticationError.internetNotConnected)
                        return
                    }
                    
                    completion(nil, AuthenticationError.emailNotFound)
                    return
                }
                
                // If do not found the record
                if let record = record {
                    let user = self.recordToUser(record: record)
                    
                    completion(user, nil)
                } else {
                    completion(nil, AuthenticationError.emailNotFound)
                }
            }
        } else {
            let defaults = UserDefaults.standard
            
            let recordReaded = defaults.dictionary(forKey: "Profile")
            
            if recordReaded != nil {
                
                let userReaded = self.dictToUser(record: recordReaded!)
                completion(userReaded, nil)
                return
            }
            
            completion(nil, AuthenticationError.iCloudNotConnected)
        }
        
    }

    /// Fetch a CKRecord from Profile
    ///
    /// - Parameters:
    ///   - email: ID for the record
    ///   - completion: Result callback
    public func fetchRecordByEmail(email: String, completion: @escaping (_ record: CKRecord?,_ error: Error?) -> Void) {
        
        let email = email.lowercased()
        
        if UserDAO.isiCloudAllowed() {
            let recordId = CKRecordID(recordName: email)
            
            Database.getDatabase().fetch(withRecordID: recordId) { (record, error) in
                if let error = error {
                    
                    
                    
                    completion(nil, error)
                    return
                }
                
                completion(record, nil)
                
            }

        } else {
            let defaults = UserDefaults.standard
            
            let recordReaded = defaults.dictionary(forKey: "Profile")
            
            if recordReaded != nil {
                
                let userReaded = self.dictToUser(record: recordReaded!)
                let recordConverted = self.userToRecord(user: userReaded, recordIn: nil)
                completion(recordConverted, nil)
                return
            }
            
            completion(nil, NSError(domain: "", code: 0, userInfo: nil))
            return
        }
        
    }

    /// Transform an User object to a CKRecord to store on the cloud
    ///
    /// - Parameters:
    ///   - user: User to be converted
    ///   - recordIn: A record to be updated. Otherwise, a new record will be returned
    /// - Returns: A record to saved in the cloud
    // swiftlint:disable:next cyclomatic_complexity
    func userToRecord(user: User, recordIn: CKRecord?) -> CKRecord {

        var record: CKRecord!

        if recordIn == nil {
            let recordId = CKRecordID(recordName: user.email.lowercased())

            record = CKRecord(recordType: type, recordID: recordId)
        } else {
            record = recordIn
        }

        record["name"] = user.name as NSString
        if let birthDate = user.birthDate {
            record["birthDate"] = birthDate as NSDate
        }
        
        if let hashPassword = user.hashPassword {
            record["hashPassword"] = hashPassword as NSString
        }

        if let financialControl = user.financialControl {
            record["financialControl"] = financialControl.rawValue as NSString
        }

        if let hasPreviousInvestments = user.hasPreviousInvestments {
            record["hasPreviousInvestments"] = hasPreviousInvestments.rawValue as NSString
        }

        if let howPayBills = user.howPayBills {
            record["howPayBills"] = howPayBills.rawValue as NSString
        }

        if let receipts = user.receipts {
            record["receipts"] = receipts as NSNumber
        }

        if let fixedExpensesMonthly = user.fixedExpensesMonthly {
            record["fixedExpensesMonthly"] = fixedExpensesMonthly.rawValue
        }

        if let hasCar = user.hasCar {
            record["hasCar"] = hasCar as NSNumber
        }

        if let hasHealthInsurance = user.hasHealthInsurance {
            record["hasHealthInsurance"] = hasHealthInsurance as NSNumber
        }

        if let crisisCarBreak = user.crisisCarBreak {
            record["crisisCarBreak"] = crisisCarBreak.rawValue as NSString
        }

        if let crisisHealthInsurance = user.crisisHealthInsurance {
            record["crisisHealthInsurance"] = crisisHealthInsurance.rawValue as NSString
        }

        if let payHouseRent = user.payHouseRent {
            record["payHouseRent"] = payHouseRent as NSNumber
        }

        if let payEquipament = user.payEquipament {
            record["payEquipament"] = payEquipament as NSNumber
        }

        if let payCarFinancing = user.payCarFinancing {
            record["payCarFinancing"] = payCarFinancing as NSNumber
        }

        if let payUniversity = user.payUniversity {
            record["payUniversity"] = payUniversity as NSNumber
        }

        if let crisisHouseRent = user.crisisHouseRent {
            record["crisisHouseRent"] = crisisHouseRent.rawValue as NSString
        }

        if let crisisCarFinancing = user.crisisCarFinancing {
            record["crisisCarFinancing"] = crisisCarFinancing.rawValue as NSString
        }

        if let crisisEquipment = user.crisisEquipment {
            record["crisisEquipment"] = crisisEquipment.rawValue as NSString
        }

        if let crisisUniversity = user.crisisUniversity {
            record["crisisUniversity"] = crisisUniversity.rawValue as NSString
        }

        if let wantToBuyCar = user.wantToBuyCar {
            record["wantToBuyCar"] = wantToBuyCar.rawValue as NSString
        }

        if let wantToBuyHouse = user.wantToBuyHouse {
            record["wantToBuyHouse"] = wantToBuyHouse.rawValue as NSString
        }

        if let wantToRetire = user.wantToRetire {
            record["wantToBuyRetire"] = wantToRetire.rawValue as NSString
        }

        if let crisisBuyCar = user.crisisBuyCar {
            record["crisisBuyCar"] = crisisBuyCar.rawValue as NSString
        }

        if let crisisBuyHouse = user.crisisBuyHouse {
            record["crisisBuyHouse"] = crisisBuyHouse.rawValue as NSString
        }

        if let crisisRetire = user.crisisRetire {
            record["crisisRetire"] = crisisRetire.rawValue as NSString
        }

        return record
    }

    /// Transform a record to User object
    ///
    /// - Parameter record: CKRecord fetched from database
    /// - Returns: Am User object
    // swiftlint:disable:next cyclomatic_complexity
    func recordToUser(record: CKRecord) -> User {

        let user = User.instance

        if let name = record["name"] as? String {
            user.name = name
        }
        
        user.updated = true

        user.email = record.recordID.recordName.lowercased()

        user.birthDate = record["birthDate"] as? Date

        if let email = record["email"] as? String {
            user.email = email
        }
        
        if let goals = record["goals"] as? [[String: Any]] {
            var currentGoals: [Goal] = []
            
            for goalData in goals {
                currentGoals.append(Goal())
            }
            
            user.goals = currentGoals
        }
        
        if let brokers = record["brokers"] as? [[String: Any]] {
            var currentBrokers: [BrokerAccount] = []
            
            for brokerData in brokers {
                //currentBrokers.append(BrokerAccount(data: brokerData))
            }
            
            user.brokers = currentBrokers
        }
        
        if let hashPassword = record["hashPassword"] as? String {
            user.hashPassword = hashPassword
        }

        if let financialControl = record["financialControl"] as? String {
            user.financialControl = FinancialControl(rawValue: financialControl)
        }

        if let hasPreviousInvestments = record["hasPreviousInvestments"] as? String {
            user.hasPreviousInvestments = InvestmentsType(rawValue: hasPreviousInvestments)
        }

        if let howPayBills = record["howPayBills"] as? String {
            user.howPayBills = BillsPayment(rawValue: howPayBills)
        }

        if let receipts = record["receipts"] as? Int {
            user.receipts = receipts
        }

        if let fixedExpensesMonthly = record["fixedExpensesMonthly"] as? String {
            user.fixedExpensesMonthly = FixedExpenses(rawValue: fixedExpensesMonthly)
        }

        if let hasCar = record["hasCar"] as? Bool {
            user.hasCar = hasCar
        }

        if let hasHealthInsurance = record["hasHealthInsurance"] as? Bool {
            user.hasHealthInsurance = hasHealthInsurance
        }

        if let crisisCarBreak = record["crisisCarBreak"] as? String {
            user.crisisCarBreak = CrisisResponse(rawValue: crisisCarBreak)
        }

        if let crisisHealthInsurance = record["crisisHealthInsurance"] as? String {
            user.crisisHealthInsurance = CrisisResponse(rawValue: crisisHealthInsurance)
        }

        if let payHouseRent = record["payHouseRent"] as? Bool {
            user.payHouseRent = payHouseRent
        }

        if let payEquipament = record["payEquipament"] as? Bool {
            user.payEquipament = payEquipament
        }

        if let payCarFinancing = record["payCarFinancing"] as? Bool {
            user.payCarFinancing = payCarFinancing
        }

        if let payUniversity = record["payUniversity"] as? Bool {
            user.payUniversity = payUniversity
        }

        if let crisisHouseRent = record["crisisHouseRent"] as? String {
            user.crisisHouseRent = CrisisResponse(rawValue: crisisHouseRent)
        }

        if let crisisCarFinancing = record["crisisCarFinancing"] as? String {
            user.crisisCarFinancing = CrisisResponse(rawValue: crisisCarFinancing)
        }

        if let crisisEquipment = record["crisisEquipment"] as? String {
            user.crisisEquipment = CrisisResponse(rawValue: crisisEquipment)
        }

        if let crisisUniversity = record["crisisUniversity"] as? String {
            user.crisisUniversity = CrisisResponse(rawValue: crisisUniversity)
        }

        if let wantToBuyCar = record["wantToBuyCar"] as? String {
            user.wantToBuyCar = PeriodShort(rawValue: wantToBuyCar)
        }

        if let wantToBuyHouse = record["wantToBuyHouse"] as? String {
            user.wantToBuyHouse = PeriodMedium(rawValue: wantToBuyHouse)
        }

        if let wantToRetire = record["wantToBuyRetire"] as? String {
            user.wantToRetire = PeriodLong(rawValue: wantToRetire)
        }

        if let crisisBuyCar = record["crisisBuyCar"] as? String {
            user.crisisBuyCar = CrisisHowToGet(rawValue: crisisBuyCar)
        }

        if let crisisBuyHouse = record["crisisBuyHouse"] as? String {
            user.crisisBuyHouse = CrisisHowToGet(rawValue: crisisBuyHouse)
        }

        if let crisisRetire = record["crisisRetire"] as? String {
            user.crisisRetire = CrisisRetire(rawValue: crisisRetire)
        }

        return user

    }

    // swiftlint:disable:next cyclomatic_complexity
    func dictToUser(record: [String: Any]) -> User {

        let user = User.instance

        if let name = record["name"] as? String {
            user.name = name
        }

        if let email = record["email"] as? String {
            user.email = email
        }

        user.birthDate = record["birthDate"] as? Date

        if let financialControl = record["financialControl"] as? String {
            user.financialControl = FinancialControl(rawValue: financialControl)
        }

        if let hasPreviousInvestments = record["hasPreviousInvestments"] as? String {
            user.hasPreviousInvestments = InvestmentsType(rawValue: hasPreviousInvestments)
        }

        if let howPayBills = record["howPayBills"] as? String {
            user.howPayBills = BillsPayment(rawValue: howPayBills)
        }

        if let receipts = record["receipts"] as? Int {
            user.receipts = receipts
        }

        if let fixedExpensesMonthly = record["fixedExpensesMonthly"] as? String {
            user.fixedExpensesMonthly = FixedExpenses(rawValue: fixedExpensesMonthly)
        }

        if let hasCar = record["hasCar"] as? Bool {
            user.hasCar = hasCar
        }

        if let hasHealthInsurance = record["hasHealthInsurance"] as? Bool {
            user.hasHealthInsurance = hasHealthInsurance
        }

        if let crisisCarBreak = record["crisisCarBreak"] as? String {
            user.crisisCarBreak = CrisisResponse(rawValue: crisisCarBreak)
        }

        if let crisisHealthInsurance = record["crisisHealthInsurance"] as? String {
            user.crisisHealthInsurance = CrisisResponse(rawValue: crisisHealthInsurance)
        }

        if let payHouseRent = record["payHouseRent"] as? Bool {
            user.payHouseRent = payHouseRent
        }

        if let payEquipament = record["payEquipament"] as? Bool {
            user.payEquipament = payEquipament
        }

        if let payCarFinancing = record["payCarFinancing"] as? Bool {
            user.payCarFinancing = payCarFinancing
        }

        if let payUniversity = record["payUniversity"] as? Bool {
            user.payUniversity = payUniversity
        }

        if let crisisHouseRent = record["crisisHouseRent"] as? String {
            user.crisisHouseRent = CrisisResponse(rawValue: crisisHouseRent)
        }

        if let crisisCarFinancing = record["crisisCarFinancing"] as? String {
            user.crisisCarFinancing = CrisisResponse(rawValue: crisisCarFinancing)
        }

        if let crisisEquipment = record["crisisEquipment"] as? String {
            user.crisisEquipment = CrisisResponse(rawValue: crisisEquipment)
        }

        if let crisisUniversity = record["crisisUniversity"] as? String {
            user.crisisUniversity = CrisisResponse(rawValue: crisisUniversity)
        }

        if let wantToBuyCar = record["wantToBuyCar"] as? String {
            user.wantToBuyCar = PeriodShort(rawValue: wantToBuyCar)
        }

        if let wantToBuyHouse = record["wantToBuyHouse"] as? String {
            user.wantToBuyHouse = PeriodMedium(rawValue: wantToBuyHouse)
        }

        if let wantToRetire = record["wantToBuyRetire"] as? String {
            user.wantToRetire = PeriodLong(rawValue: wantToRetire)
        }

        if let crisisBuyCar = record["crisisBuyCar"] as? String {
            user.crisisBuyCar = CrisisHowToGet(rawValue: crisisBuyCar)
        }

        if let crisisBuyHouse = record["crisisBuyHouse"] as? String {
            user.crisisBuyHouse = CrisisHowToGet(rawValue: crisisBuyHouse)
        }

        if let crisisRetire = record["crisisRetire"] as? String {
            user.crisisRetire = CrisisRetire(rawValue: crisisRetire)
        }

        return user

    }

    /// Return the user logged.
    ///
    /// - Parameter completion: Result callback
    func getLogged(completion: @escaping (_ user: User?, _ error: AuthenticationError?) -> Void) {
        
        let defaults = UserDefaults.standard
        
        let recordReaded = defaults.dictionary(forKey: "Profile")
        
        if let recordReaded = recordReaded {
            
            let userReaded = self.dictToUser(record: recordReaded)
            
            if UserDAO.isiCloudAllowed() {
                
                self.fetchUserByEmail(email: User.instance.email, { (user, error) in
                    
                    if let error = error {
                        
                        if error == AuthenticationError.internetNotConnected {
                            completion(user, error)
                        } else {
                            completion(nil, AuthenticationError.emailNotFound)
                        }
                        
                        return
                    }
                    
                    if user?.hashPassword == nil {
                        return completion(user, AuthenticationError.userDoesNotHavePassword)
                    } else {
                        return completion(user, nil)
                    }
   
                })
            } else {
                if userReaded.hashPassword == nil {
                    return completion(userReaded, AuthenticationError.userDoesNotHavePassword)
                } else {
                    return completion(userReaded, nil)
                }
            }
        } else {
            completion(nil, AuthenticationError.userDoesNotExist)
        }
    }
    
    func createUserOnDefaults(user: User) {
        
        let record = self.userToRecord(user: user, recordIn: nil)

        let defaults = UserDefaults.standard
        
        record["email"] = user.email.lowercased() as NSString
        defaults.set(record.dictionaryWithValues(forKeys: record.allKeys()), forKey: "Profile")

    }
    
    func getLogin(email: String, hashPassword: String, completion: @escaping (User?, AuthenticationError?)-> Void) {

        self.fetchUserByEmail(email: email) { (user, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            
            if let user = user {
                if let hashPasswordUser = user.hashPassword {
                    // If the password is equal to the one from database
                    if hashPasswordUser == hashPassword {
                        
                        self.createUserOnDefaults(user: user)

                        completion(user, nil)
                    } else {
                        completion(nil, AuthenticationError.wrongPassword)
                    }
                } else {
                    completion(nil, AuthenticationError.emailNotFound)
                }
            }
        }
        
        
    }

    /// Save the user login on the private database
    func saveLogin(user: User, completion: @escaping (_ user: User?, _ error: Error?) -> Void) {

        if UserDAO.isiCloudAllowed() {
            let recordId = CKRecordID(recordName: user.email)
            
            let record = CKRecord(recordType:  "Authenticated", recordID: recordId)
            
            record["email"] = user.email.lowercased() as NSString
            
            Database.getPrivateDatabase().save(record) { (savedRecord, error) in
                if let error = error {
                    
                    completion(nil, error)
                    return
                }
                
                
                
                let user = self.recordToUser(record: savedRecord!)
                
                completion(user, nil)
                
            }
        } else {
            completion(user, nil)
        }
        

    }

    func cleanMyData(completion: @escaping (_ cleaned: Bool) -> Void)
    {
        
        if UserDAO.isiCloudAllowed() {
            self.fetchRecordByEmail(email: User.instance.email, completion: { (user, error) in
                
                guard error == nil else {
                    completion(false)
                    return
                }
                
                if let user = user {
                    Database.getDatabase().delete(withRecordID: user.recordID, completionHandler: { (recordDeleted, error) in
                        let defaults = UserDefaults.standard
                        defaults.removeObject(forKey: "Profile")
                        defaults.removeObject(forKey: "Authenticated")
                        
                        completion(true)
                    })
                }
            })
        } else {
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "Profile")
            defaults.removeObject(forKey: "Authenticated")
            
            completion(true)
        }
    }
}
