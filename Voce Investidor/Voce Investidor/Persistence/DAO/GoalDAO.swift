//
//  GoalDAO.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 28/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import CloudKit

/// DAO to Retrieve, Create and Update Goals
class GoalDAO {
    
    /// Table to store the user details in Public database
    private let type = "Goal"
    
    /// Retrieve all goals from a specific user
    ///
    /// - Parameters:
    ///   - user: The user to receive goals
    ///   - completion: the function to be called when retrieving goals or error
    func goals(from user: User, completion: @escaping ([Goal]?, Error?) -> Void) {

        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: user.email))
        let userReference = CKReference(record: userRecord, action: .none)
        let predicate = NSPredicate(format: "user = %@", userReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)

        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, error == nil else {
                completion(nil, error)
                return
            }
            
            var goals: [Goal] = []
            
            for record in records {
                goals.append(self.goal(from: record))
            }
            
            completion(goals, nil)
        }
    }
    
    /// Create a new goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be created
    ///   - completion: The completion to be called
    func create(goal: Goal, completion: @escaping (Error?) -> Void) {
        
        let record = self.record(from: goal)
        
        Database.getDatabase().save(record) { (_, error) in
            completion(error)
        }
        
    }
    
    /// Update the goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be updated
    ///   - completion: The completion to be called
    func update(goal: Goal, completion: @escaping (Error?) -> Void) {
        
        guard let goalID = goal.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let goalRecord = CKRecord(recordType: self.type, recordID: .init(recordName: goalID))
        let goalReference = CKReference(record: goalRecord, action: .none)
        let predicate = NSPredicate(format: "%K == %@", "recordID", goalReference)
        let query = CKQuery(recordType: self.type, predicate: predicate)
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, let goalRecord = records.first, error == nil else {
                completion(error)
                return
            }
            
            let newRecord = self.recordUpdated(goal: goal, record: goalRecord)

            Database.getDatabase().save(newRecord) { (_, error) in
                completion(error)
            }
        }
    }
    
    /// Delete the goal in the Database
    ///
    /// - Parameters:
    ///   - goal: A goal to be deleted
    ///   - completion: The completion to be called
    func delete(goal: Goal, completion: @escaping (Error?) -> Void) {
        
        guard let goalID = goal.identification else {
            return completion(DAOErrors.needIdentifier)
        }
        
        let recordID = CKRecordID(recordName: goalID)
        
        Database.getDatabase().delete(withRecordID: recordID) { (_, error) in
            completion(error)
        }
    }
    
    /// Create a record given a goal with recordID to update an existing element in Database
    ///
    /// - Parameter goal: Goal to be reference to record
    /// - Returns: The record created
    private func recordUpdated(goal: Goal, record: CKRecord) -> CKRecord {
        
        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: User.instance.email))
        let userReference = CKReference(record: userRecord, action: .none)
        
        record["user"] = userReference
        
        record["name"] = goal.name
        record["type"] = goal.type?.rawValue
        record["objectiveImagePath"] = goal.objectiveImagePath
        record["objectiveName"] = goal.objectiveName
        record["deadLine"] = goal.deadLine
        record["finalValue"] = goal.finalValue
        
        return record
    }
    
    /// Create a record given a goal
    ///
    /// - Parameter goal: Goal to be reference to record
    /// - Returns: The record created
    private func record(from goal: Goal) -> CKRecord {

        let record = CKRecord(recordType: self.type)
        
        let userRecord = CKRecord(recordType: "Profile", recordID: .init(recordName: User.instance.email))
        let userReference = CKReference(record: userRecord, action: .none)
        
        record["user"] = userReference
        
        record["name"] = goal.name
        record["type"] = goal.type?.rawValue
        record["objectiveImagePath"] = goal.objectiveImagePath
        record["objectiveName"] = goal.objectiveName
        record["deadLine"] = goal.deadLine
        record["finalValue"] = goal.finalValue

        return record
    }
    
    /// Create a goal given a record
    ///
    /// - Parameter record: Record got from database
    /// - Returns: The goal created from the record
    private func goal(from record: CKRecord) -> Goal {
        var goal = Goal()
        
        goal.identification = record.recordID.recordName
        
        if let name = record["name"] as? String {
            goal.name = name
        }
        if let objectiveImagePath = record["objectiveImagePath"] as? String {
            goal.objectiveImagePath = objectiveImagePath
        }
        if let objectiveName = record["objectiveName"] as? String {
            goal.objectiveName = objectiveName
        }
        if let type = record["type"] as? String {
            goal.type = GoalType(value: type)
        }
        if let deadLine = record["deadLine"] as? Date {
            
            goal.deadLine = deadLine
        }
        if let finalValue = record["finalValue"] as? Double {
            goal.finalValue = finalValue
        }
        
        return goal
    }
}
