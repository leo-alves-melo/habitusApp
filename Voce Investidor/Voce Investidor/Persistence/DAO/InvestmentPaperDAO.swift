//
//  InvestmentPaperDAO.swift
//  Voce Investidor
//
//  Created by Leonardo Alves de Melo on 30/11/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import CloudKit

/// DAO to Retrieve, Create and Update InvestmentPapers
class InvestmentPaperDAO {
    
    /// Table to store the user details in Public database
    private let type = "InvestmentPaper"
    
    /// Retrieve a specific investmentPaper
    ///
    /// - Parameters:
    ///   - identification: The identification of the investmentPaper
    ///   - completion: the function to be called when retrieving goals or error
    func investmentPaper(from identification: String, completion: @escaping (InvestmentPaper?, Error?) -> Void) {
        
        let predicate = NSPredicate(format: "identifier == %@", identification)
        let query = CKQuery(recordType: self.type, predicate: predicate)
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, let investmentPaperRecord = records.first, error == nil else {
                completion(nil, DAOErrors.notFound)
                return
            }
            
            let investmentPaper = self.investmentPaper(from: investmentPaperRecord)

            completion(investmentPaper, nil)
        }
    }
    
    /// Retrieve all investmentPapers
    ///
    /// - Parameters:
    ///   - completion: the function to be called when retrieving goals or error
    func investmentPapers(completion: @escaping ([InvestmentPaper]?, Error?) -> Void) {
        
        
        let query = CKQuery(recordType: self.type, predicate: NSPredicate(value: true))
        
        Database.getDatabase().perform(query, inZoneWith: nil) { (records, error) in
            
            guard let records = records, error == nil else {
                completion(nil, DAOErrors.notFound)
                return
            }
            
            var investmentPapers: [InvestmentPaper] = []
            
            for investmentPaperRecord in records {
                let investmentPaper = self.investmentPaper(from: investmentPaperRecord)
                investmentPapers.append(investmentPaper)

            }

            completion(investmentPapers, nil)
        }
    }
    
    /// Create a investmentPapr given a record
    ///
    /// - Parameter record: Record got from database
    /// - Returns: The investmentPaper created from the record
    private func investmentPaper(from record: CKRecord) -> InvestmentPaper {
        var investmentPaper = InvestmentPaper()
        
        if let name = record["name"] as? String {
            investmentPaper.name = name
        }
        if let due = record["due"] as? Date {
            investmentPaper.due = due
        }
        if let minimumAmount = record["minimumAmount"] as? Double {
            investmentPaper.minimumAmount = minimumAmount
        }
        if let calculation = record["calculation"] as? String {
            
            investmentPaper.calculationString = calculation
        }

        investmentPaper.identifier = record["identifier"] as? String

        return investmentPaper
    }
}
