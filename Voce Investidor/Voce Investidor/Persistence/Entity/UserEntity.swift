//
//  UserEntity.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 09/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class UserEntity {
    private(set) var name: NSString = ""
    private(set) var email: NSString = ""
    private(set) var birthDate: NSDate?
    private(set) var financialControl: FinancialControl?
    private(set) var hasPreviousInvestments: InvestmentsType?
    private(set) var howPayBills: BillsPayment?
    private(set) var receipts: Int?
    private(set) var fixedExpensesMonthly: FixedExpenses?
    private(set) var hasCar: Bool?
    private(set) var healthInsurance: Bool?
    private(set) var crisisCarBreak: CrisisResponse?
    private(set) var crisisHealthInsurance: CrisisResponse?
    private(set) var payHouseRent: Bool?
    private(set) var payCarFinancing: Bool?
    private(set) var payEquipament: Bool?
    private(set) var payUniversity: Bool?
    private(set) var crisisHouseRent: CrisisResponse?
    private(set) var crisisCarFinancing: CrisisResponse?
    private(set) var crisisEquipment: CrisisResponse?
    private(set) var crisisUniversity: CrisisResponse?
    private(set) var wantToBuyCar: PeriodShort?
    private(set) var wantToBuyHouse: PeriodMedium?
    private(set) var wantToBuyRetire: PeriodLong?
    private(set) var crisisBuyCar: CrisisHowToGet?
    private(set) var crisisBuyHouse: CrisisHowToGet?
    private(set) var crisisRetire: CrisisRetire?
}
