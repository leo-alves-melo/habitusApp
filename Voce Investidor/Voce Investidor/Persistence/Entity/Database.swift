//
//  File.swift
//  Voce Investidor
//
//  Created by Edgar Silva on 09/10/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import CloudKit

class Database {

    static func getDatabase() -> CKDatabase {

        let container = CKContainer.default()

        return container.publicCloudDatabase

    }

    static func getPrivateDatabase() -> CKDatabase {

        let container = CKContainer.default()

        return container.privateCloudDatabase

    }


}
